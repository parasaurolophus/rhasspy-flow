Copyright &copy; Kirk Rader 2019-2020. All rights reserved.

# (Nearly) Cloud-Free Home Automation

A [Node-RED][] project repository that provides presence detection, voice control and lighting automation for a small apartment.

See <https:/gitlab.com//parasaurolophus/rhasspy-flow/>

```mermaid
graph TD

  subgraph "The 'Cloud'"
    location[Mobile OS<br/>location<br/>services]
    certbot[Let's Encrypt]
    duckdns[Duck DNS]
  end

  subgraph Mobile Device
    owntracks[Owntracks<br/>app]
    browser1["Web Browser<br/>(dashboard)"]
  end

  subgraph Laptop / Desktop
    browser2["Web Browser<br/>(dashboard,<br/>editor)"]
  end

  subgraph "Home Network"

    router["Router<br/>(port<br/>forwarding)"]

    subgraph Raspberry Pi
      nodered[Node-RED<br/>server]
      broker["Mosquitto<br/>(MQTT broker)"]
      infrastructure["cron jobs<br/>(periodically<br/>renew certificates,<br/>update IP address)"]
      controller[Aeotec<br/>Z-Stick]
    end

    bridge[Hue Bridge]
    hue[Multiple<br/>Hue devices]
    zwave[Multiple<br/>Z-Wave devices]
    client["Rhasspy<br/>voice<br/>assistant /<br/>dashboard<br/>client<br/>(one per<br/>room)"]

  end

  browser2 --- |HTTPS| router
  certbot --- |CA<br/>service| infrastructure
  broker --- nodered
  location --- owntracks
  owntracks --- |MQTT /<br/>TLS| router
  browser1 -- HTTPS --- router
  router --- broker
  router --- nodered
  router --- client
  router --- |Hue<br/>API| bridge
  bridge --- |Zigbee| hue
  nodered --- |OpenZWave| controller
  controller --- |Z-Wave| zwave
  duckdns --- |Dynamic<br/>DNS<br/>service| infrastructure
```

Lighting automation, driven by [node-red-contrib-suncalc][], uses a number of sub-flows for modularity. It also depends on a global context variable, `global.nighttime`, to determine the transition from `evening` to `night`.

```mermaid
stateDiagram-v2

  day --> evening: sunset /<br/>interior evening,<br/>exterior evening
  evening --> night: global.nighttime /<br/>interior night,<br/>exterior off
  night --> day: sunrise /<br/>interior off,<br/>exterior off
```

Presence detection uses [MQTT][] messages sent by the [Owntracks][] app on each occupant's mobile device. It uses link nodes as entry points to the same sub-flows that drive time-of-day based ligthing automation.

```mermaid
stateDiagram-v2

  home --> away: last occupant leaves /<br/>resume automation
  away --> home: first occupant arrives /<br/>interior evening
```

_[Rationale: the `interior evening` sub-flow is invoked when the first occupant arrives so that the home will be lit no matter how dark or gloomy the day or night. The normal automation for the current time of day is resumed when the last person leaves as a simple means of presence simulation for security purposes.]_

In both cases, the "evening" sub-flows take the current date into account in choosing which of a number of lighting "themes" to use.

| Theme      | Date Range          | Description             |
|------------|---------------------|-------------------------|
| `tribal`   | July 1 - 4          | US Independence Day     |
| `spooky`   | Any day in October  | Halloween               |
| `jolly`    | Any day in December | Winter Solstice         |
| `standard` | Any other day       | Standard lighting theme |

Similarly, the `interior night` sub-flow is used to provide a very dim night-light functionality in the public spaces of the home.

## Dependencies and Assumptions

These flows depend explicitly on the following:

- A machine running an up-to-date version of [Node-RED][] installed as per the instructions at <https://nodered.org/docs/getting-started/>

  > Note that you should follow the instructions for upgrading to the latest release even if [Node-RED][] is pre-installed, e.g. as by the "Recommended Software" installer for Raspberry Pi OS since the version installed by OS package managers is typically very out of date.

  - [node-red-contrib-suncalc][]

  - [node-red-contrib-moment][]

  - [node-red-contrib-huemagic][]

  - <https://github.com/parasaurolophus/node-red-contrib-openzwave>
        (fork of [node-red-contrib-openzwave][] that supports `complete` and has some as-yet-umpublished bug fixes)

  - [node-red-node-rbe][]

  - [node-red-dashboard][]

  - [node-red-node-ui-led][]

  - [node-red-contrib-semaphore][]

  - [node-red-contrib-fan][]

- [OpenZWave][] is installed on the [Node-RED][] server, configured to communicate with a [Z-Wave][] controller, e.g. [Aeotec Z-Stick][z-stick]

- If using a controller like the [Aeotec Z-Stick][z-stick] that appears as a serial device in Linux (e.g. _/dev/ttyACM0_), add the user account that launches [Node-RED][] to the `dialout` group; for example

      sudo adduser jdoe dialout

- A [MQTT][] broker securely accessible both to [Node-RED][] and other services like [Rhasspy][] inside the home and to mobile devices outside the home

- [Owntracks][] running on household members' mobile devices

- A machine running [Rhasspy][] in each room with appropriate hardware and software configuration to act as a voice assistant ([see below](#voice-capture))

They also assume:

- The [Node-RED][] instance has been installed on a server with either a static public IP address (unlikely at home!) or an appropriately configured dynamic DNS address by way of a service like [Duck DNS][duckdns]

- The [Node-RED][] instance has been configured to require HTTPS using valid certificates issued by a CA like [Let's Encrypt][certbot]

- Similarly, the MQTT broker (e.g. [Mosquitto][]) has been configured to require a valid password and encryption

- The router acting as a gateway for the home network has been configured to forward port 1880 to the [Node-RED][] server and port 1883 to the machine running the MQTT broker (which in my case are the same but they do not have to be)

- A number of particular [Philips Hue][hue] devices, groups and scenes

- A number of particular [Z-Wave][] power switch and multi-sensor devices

- The standard Linux `cron` daemon is running for use by the `exec` node

- The standard Linux `date` command is accessible via the `exec` node

- A [Node-RED][] context store named `file` that persists global context to the file system

- The microphones used with [Rhasspy][] are capable of "far field" voice capture ([see below](#voice-capture))

See <http://kirkrader.gitlab.io/docs/rpi-config/> for information on how to configure a Raspberry Pi to meet the preceding requirements and assumptions. You will have to consult your router documentation for information on setting up DHCP and port forwarding to support out-of-home access.

**N.B.** obviously, you will not be able to use these flows "as is" unless you were to somehow magically have exactly the same collection of makes and models of "smart" gear and have configured them with exactly the same names and id's as in my home. You can use these flows as examples and inspiration for some of the things that are possible using tools like [Node-RED][], [Owntracks][], [Rhasspy][] etc.

## Installation

> Again, see <http://kirkrader.gitlab.io/docs/rpi-config/> for information on configuring a [Raspberry Pi][pi] to meet all of the requirements and assumptions made by these flows. This includes installing and configuring self-hosted instances of [Mosquitto][] and [Node-RED][], configuring such servers for secure, remote access using the [Duck DNS][duckdns] dyanmic DNS service, certificates issued by [Let's Encrypt][certbot] and so on.

> The location of the [Node-RED][] _settings.js_ file can vary depending on how you initially installed and configured it, but is typically _~/.node-red/settings.js_ for the user who ran the installation script. The _settings.js_ file can be modified using any text editor, e.g. `nano ~/.node-red/settings.js`
>
> After making any changes to _settings.js_ you must restart the daemon, e.g. `sudo systemctl restart nodered`

1. Make sure that the _projects_ feature is enabled

   ```javascript
   // Customising the editor
   editorTheme: {
       projects: {
           // To enable the Projects feature, set this value to true
           enabled: true
       }
   }
   ```

2. Make sure flows are written in "pretty" format

   ```javascript
   // To enabled pretty-printing of the flow within the flow file, set the following
   //  property to true:
   flowFilePretty: true,
   ```

3. Verify that all of the prerequisites are satisfied listed under [Dependencies and Assumptions](#dependencies-and-assumptions)

4. Import `https://gitlab.com/parasaurolophus/rhasspy-flow.git` as a new project

   ![](./images/clone-cheznous.png)

5. Modify the settings for the [MQTT][] broker address and credentials, [Z-Wave][] controller serial device, latitude and longitude in the [node-red-contrib-suncalc][] node, [Hue][] bridge API key, device id's, group names, [Z-Wave][] node id's, command classes and indices etc. to match your set-up.

## Integration

These flows rely on various mechanisms for integrating with "smart home" devices and other external (in the sense of not running on the same server hardware as [Node-RED][]) systems. [MQTT][] is preferred wherever practical since it is designed from the ground up to support scenarios typical of home automation. Where a given device or system does not directly support [MQTT][], other protocols and API's are used. For example, [node-red-contrib-huemagic][] is used to access the native [Philips Hue][hue] API for accessing its "bridge" device via WiFi. Similarly, [node-red-contrib-openzwave][] is used to communicate natively with [Z-Wave][] based devices. And so on.

Not all external systems correspond to what are conventionally regarded as "smart home" devices. For example, the [Node-RED][] dashboards include hardware and software monitoring for a number of servers and desktop computers, not all of which are really part of my "home automation" setup. For convenience and consistency, the monitored measurements are broadcast from each such computer using `cron` jobs that invoke command-line [MQTT][] clients. For example, each [Raspberry Pi][] in my home is configured to send its current CPU temperature as a [MQTT][] message. This is done by adding the following `cron` job using `sudo crontab -e`

```
*/5 * * * * /usr/local/bin/check-cpu
```

where _/usr/local/bin/check-cpu_ is a `bash` shell script that uses <https://github.com/hivemq/mqtt-cli> to send the output of invoking `vcgencmd measure_temp` as a [MQTT][] message:

```bash
#!/bin/bash

export MQTT_CLI_PW='xxxx'

trap 'catch $? $LINENO' ERR

catch() {
  mqtt pub -V 3 -s -h 192.168.1.100 -p 1883 -u root -pw:env -t "$(hostname)/cpu-check/error" -m "{\"line\":$2,\"error\":$1,\"date\":\"$(date --iso-8601=ns)\"}" -q 2 -r
  exit "$1"
}

TEMPERATURE=$(vcgencmd measure_temp)
TEMPERATURE="${TEMPERATURE#temp=*}"
TEMPERATURE="${TEMPERATURE%\'C*}"
mqtt pub -V 3 -s -h 192.168.1.100 -p 1883 -u root -pw:env -t "$(hostname)/cpu/temperature" -m "${TEMPERATURE}" -q 2
```


## OpenZWave Usage

> As noted above, the user account that runs the [Node-RED][] service must be a member of the _dialout_ group, e.g.
>
>     sudo adduser jdoe dialout
>
> in order to use [node-red-contrib-openzwave][] or any other software component that relies on [OpenZWave][] to access a controller device that manifests as a TTY device.

| Node Id                      | Command Class | Instance               | Command Index | Value             | Description                                                                                                               |
|:-----------------------------|:--------------|:-----------------------|:--------------|:------------------|:--------------------------------------------------------------------------------------------------------------------------|
| 1                            | -             | -                      | -             | -                 | _Aeotec Z-Stick Gen5_[^zstick]                                                                                            |
| 5 or 7[^jasco-nodes]         | 37            | 1                      | 0             | `true` or `false` | _JASCO Outdoor Smart Switch_ on or off                                                                                    |
| 12[^battery-powered-devices] | 49            | 1                      | 1             | Decimal           | _Sensative Strips Comfort_ temperature report in Celsius                                                                  |
| 12                           | 49            | 1                      | 3             | Integer           | _Sensative Strips Comfort_ illuminance report in Lux                                                                      |
| 12                           | 132           | 1                      | 0             | Integer           | _Sensative Strips Comfort_ frequency to check for commands in seconds (minimum 1800)[^sensor-checking-frequency]          |
| 14                           | 48            | 1                      | 0             | `true` or `false` | _Aeotec MultiSensor 6_ motion detected                                                                                    |
| 14                           | 49            | 1                      | 1             | Decimal           | _Aeotec MultiSensor 6_ temperature in degrees as specified by the value of `msg.payload.units`                            |
| 14                           | 49            | 1                      | 3             | Integer           | _Aeotec MultiSensor 6_ illuminance in Lux                                                                                 |
| 14                           | 112           | 1                      | 4             | String            | _Aeotec MultiSensor 6_ motion sensitivity setting in the form "Enabled level _n_" where _n_ is a digit in the range 0 - 5 |
| 14                           | 112           | 1                      | 3             | Integer           | _Aeotec MultiSensor 6_ motion timeout in seconds                                                                          |

Note that the _PARSE OZWCACHE_ button can be used to write a spreadsheet with the complete repertoire of supported "value id's" to _/data/nodered/z-wave-values.csv_.

<a href="voice-capture"></a>

## Voice Assistants

In addition to the [Node-RED][] and [Mosquitto][] servers, these flows assume the use of [Rhasspy][] on additional machines located in various rooms. My setup again uses [Raspberry Pi's][pi] for this purpose. These additional [Pi's][pi] use the [Official 7" Touch Screen][touchscreen] and are configured to present the [Node-RED][] dashboard in "kiosk mode." Here is what one of the shelves of the entertainment center in my living room looks like:

![](./images/home-automation-hub.jpg)

It shows the [Philips Hue][hue] bridge, the [Node-RED][] server [Pi][] with [Z-Stick][] attached, the [ReSpeaker Mic Array V2.0][mic-array] attached to the [touchscreen][] enabled [Pi][] and the top of a [Logitech Z50](https://www.logitech.com/en-us/product/multimedia-speaker-z50) peeking out from behind. The speaker is plugged into the output of the mic array, not the [Pi][]. See below for information on [selecting microphones for voice capture](#selecting-microphones-for-voice-capture).

The mic / speaker / touchscreen enabled [Pi][] setup needs to be replicated in each room where you want a voice assistant / [Node-RED][] dashboard "kiosk." They will all send commands via MQTT, WebSockets or the like to the central [Node-RED][] server. [Node-RED][], in turn, controls the "smart" gear like [Hue][] lights, [Z-Wave][] devices and so on.

To set up a [Pi][] as a voice assistant with [Node-RED][] dashboard display:

1. Attach the [Pi][] to the [touchscreen][] as per the instructions... oh, wait! It doesn't come with any. I found <https://www.instructables.com/id/Raspberry-Pi-Touchscreen-Setup/> helpful, and a [DuckDuckGo](https://duckduckgo.com) search will produce quite a number of such tutorials.

2. Install the assembled [Pi][] / [touchscreen][] into an appropriate case, temporarily attach a keyboard and mouse, and boot up

3. Install the _unclutter_ package to eliminate the mouse cursor:

   ```
   sudo apt update
   sudo apt install unclutter
   ```

4. Install and configure [Rhasspy][] as described [below](#configuring-rhasspy)

5. Copy _/etc/xdg/lxsession/LXDE-pi/autostart_ to _~/.config/lxsession/LXDE-pi/autostart_ and edit it

   ```
   cp /etc/xdg/lxsession/LXDE-pi/autostart ~/.config/lxsession/LXDE-pi/autostart
   nano ~/.config/lxsession/LXDE-pi/autostart
   ```
6. Replace

   ```
   @xscreensaver -no-splash
   ```

   with

   ```
   @unclutter -idle 0
   @sed -i 's/"exited_cleanly": false/"exited_cleanly": true/' ~/.config/chromium-browser Default/Preferences
   @chromium-browser --incognito --kiosk https://user:password@host:1880/ui/
   ```

   replacing `user`, `password` and `host` with the credentials and host name for your [Node-RED][] instance

   > Note that many tutorials, blog posts, forum messages etc. will advise adding a number of `xset s ...` commands and the like when enabling "kiosk mode" but these aren't appropriate in this use case unless you really want to burn out your touchscreen really fast by keeping it on all the time.

7. Uncomment the following lines from _/etc/lightdm/lightdm.conf_, adjusting the user name as appropriate (since you _don't_ run as `pi` do you?)

   ```
   autologin-user=pi
   autologin-user-timeout=0
   ```

6. Reboot

If all goes well, your [touchscreen][] enabled [Pi][] should boot directly into your [Node-RED][] dashboard. You may now remove the keyboard and mouse.

### Configuring Rhasspy

There a number of ways to [install Rhasspy](https://rhasspy.readthedocs.io/en/latest/#getting-started). I use Docker but using the Python `venv` approach should work at least as well.

One of the features of these flows not called out so far is that they automatically fetch the current set of lights, groups and scenes from the [Hue][] bridge and create files in the format required by [Rhasspy][] to configure voice control. In particular, they create three "slots" files that can be copied into the [Rhasspy][] profile directory and referenced from _sentences.ini_ when constructing voice commands to control lighting. The three files are:

- _/data/rhasspy/profile/en/slots/lights_
- _/data/rhasspy/profile/en/slots/groups_
- _/data/rhasspy/profile/en/slots/scenes_

These files will be re-created each time [Node-RED][] starts and will contain the latest and greatest metadata about the light setup as obtained in real time from the [Hue][] bridge. Copy them into the corresponding _~/.config/rhasspy/profile/en/slots/_ directories on the [Rhasspy][] servers any time the [Hue][] configuration changes by adding, removing or renaming lights, groups and scenes.

Here is an example _sentences.ini_ that uses these slots files:

```
[ActivateScene]
scene = $scenes {scene}
activate <scene>
turn on <scene>
switch <scene> on

[ChangeGroupState]
group = $groups {group}
state = $binary_switch_states {on}
turn [the] <group> [lights] <state>
turn <state> [the] <group> [lights]
switch [the] <group> [lights] <state>

[ChangeGroupColor]
group = $groups {group}
color = $colors {color}
turn [the] <group> [to] <color>
set [the] <group> [to] <color>
make [the] <group> <color>

[ChangeGroupBrightness]
group = $groups {group}
brightness = (0..100) {brightness}
set [the] <group> to <brightness> [percent]

[ChangeLightState]
light = $lights {light}
state = $binary_switch_states {on}
turn [the] <light> <state>
turn <state> [the] <light>
switch [the] <light> <state>

[ChangeLightColor]
light = $lights {light}
color = $colors {color}
turn [the] <light> [to] <color>
set [the] <light> [to] <color>
make [the] <light> <color>

[ChangeLightBrightness]
light = $lights {light}
brightness = (0..100) {brightness}
set [the] <light> to <brightness> [percent]

[ChangeOutletState]
outlet = $zwave-outlets {outlet}
state = $binary_switch_states {on}
turn [the] <outlet> <state>
turn <state> [the] <outlet>
switch [the] <outlet> <state>
```

The careful reader will have already noticed that this _sentences.ini_ relies on additional slots files for things like the set of supported [Z-Wave][] value id strings. A future revision of these flows may include the ability to generate those slots files automatically, as well. For now they must be manually maintained.

For both [Hue][] and [Z-Wave][] devices, the end results of [Rhasspy][]'s intent processing are sent as MQTT messages to be processed by [Node-RED][].

Here is an example of a typical MQTT message sent when [Rhasspy][] successfully processes an intent:

```json
{
  "input": "activate u-obzIzqOljhhrD",
  "intent": {
    "intentName": "ActivateScene",
    "confidenceScore": 1
  },
  "siteId": "office-pi4",
  "id": null,
  "slots": [
    {
      "entity": "scenes",
      "value": {
        "kind": "Unknown",
        "value": "u-obzIzqOljhhrD"
      },
      "slotName": "scene",
      "rawValue": "living room sunset",
      "confidence": 1,
      "range": {
        "start": 9,
        "end": 24,
        "rawStart": 9,
        "rawEnd": 27
      }
    }
  ],
  "sessionId": "office-pi4-hey-mycroft-2-e8ef95d0-91a3-4cb7-a976-65b41f55cc3e",
  "customData": null,
  "asrTokens": [
    [
      {
        "value": "activate",
        "confidence": 1,
        "rangeStart": 0,
        "rangeEnd": 8,
        "time": null
      },
      {
        "value": "u-obzIzqOljhhrD",
        "confidence": 1,
        "rangeStart": 9,
        "rangeEnd": 24,
        "time": null
      }
    ]
  ],
  "asrConfidence": null,
  "rawInput": "activate living room sunset",
  "wakewordId": "hey-mycroft-2",
  "lang": null
}
```

The given _sentences.ini_ and slots files produce the following patterns of MQTT command messages:

| Intent                  | Slot Names            | Description                                                               |
|:------------------------|:----------------------|:--------------------------------------------------------------------------|
| `ActivateScene`         | `scene`               | [Hue][] scene id string (e.g. `"u-obzIzqOljhhrD"`                         |
| `ChangeGroupState`      | `group`, `on`         | [Hue][] group id, boolean `true` or `false`                               |
| `ChangeGroupColor`      | `group`, `color`      | [Hue][] group id, color name string (e.g. `"purple"`)                     |
| `ChangeLightBrightness` | `light`, `brightness` | [Hue][] light id, percentage (i.e. integer from`0` - `100`                |
| `ChangeLightState`      | `light`, `on`         | [Hue][] light id, boolean `true` or `false`                               |
| `ChangeLightColor`      | `light`, `color`      | [Hue][] light id, color name string (e.g. `"purple"`)                     |
| `ChangeLightBrightness` | `light`, `brightness` | [Hue][] light id, percentage (i.e. integer from`0` - `100`                |
| `ChangeOutletState`     | `outlet`, `on`        | [Z-Wave][] value id string (e.g. `"7-37-1-0"`), boolean `true` or `false` |

Notes:

1. The underlying [Hue][] scene id strings are used in the [Rhasspy][] profile and in [Node-RED][] due to bugs in the [node-red-contrib-huemagic][] package when more than one group has scenes with the same name (which is the default condition for groups created through the [Hue][] app)

2. Even though [Hue][] group and light id's are integers, they are treated as strings in the [Rhasspy][] profile and in [Node-RED][] due to the design of the [node-red-contrib-huemagic][] package

3. [Z-Wave][] value id strings have four components separted by dashes (`-`): node id, command class, instance number, command index; the combination of particular values in such a 'tuple uniquely identifies a particular parameter of a particular device

### Selecting Microphones for Voice Capture

The trickiest part of getting voice control working using a tool like [Rhasspy][] is finding and configuring the kinds of microphones required to capture wake words and commands correctly and reliably. This is especially challenging when running [Rhasspy][] on a Raspberry Pi with its ARM processor since most consumer-level products assume they will be running on Windows or Macintosh computers using a more conventional Intel or AMD CPU. Even on such conventional computers, basic analog and USB based microphones of the sort built into laptops or used for normal desktop audio and video chats are not up to the task of capturing voice commands from across a room with normal household background noise.

To get anything close to the experience that dedicated voice assistants like Amazon Echo and Google (now Nest) Home provide you will have to duplicate their fairly sophisticated mic arrays and on-board signal processing hardware. To date, after experimenting with a number of different microphone options, the only device from which I have obtained satisfactory results on a Raspberry Pi is the USB based [ReSpeaker Mic Array V2.0][mic-array] designed specifically for this kind of voice capture application. I have heard reports that the more modestly priced [ReSpeaker 2-Mics Pi HAT][mic-hat] also works well with [Rhasspy][], but I have not had a chance yet to try it out for myself.

When using the [ReSpeaker Mic Array][mic-array] I strongly recommend also using a standalone analog speaker plugged into the audio output jack of the mic array. This supports the built in echo cancellation features of the mic array. It also reduces contention for audio device driver control in the Raspberry Pi OS. For example, when using the mic array only for input and relying on the default HDMI audio for output, I frequently experienced lags and dropouts in hearing [Rhasspy][]'s audio feedback during wake word and command processing. This was not just annoying but often led to failed dialog management sessions.

## Miscellaneous Notes

The cost for the combination of [Raspberry Pi 4][pi], [ReSpeaker Mic Array V2.0][mic-array] and its case (sold separately), [Official 7" Touchscreen][touchscreen] and its case (also sold separately) is actually in the same ballpark for a high end Google Nest Home Hub Max or comparable Amazon Echo product. So the motivation here isn't cost savings. It is the satisfaction of knowing that what my loved ones and I say and do in our home, stays in our home!

For the effort involved in setting all this up, the result is a combination of home automation hub and voice assistant that that not only surpasses off-the-shelf solutions in features but which is exactly tailored to my home's occupants' needs and preferences. Anyone who has tried to make do with very generic device control UI's or tried to teach a skeptical spouse to speak commands in the way that Alexa or Google Assistant expects them will know what I'm talking about.

[^cloud]: Cloud-based services are best avoided in home automation systems for a variety of reasons. Notably: increased performance and latency overhead; increased surface area for security and privacy vulnerabilities; increased number of points of failure, many of which will be totally outside of your control to diagnose or fix.

[^zstick]: Non-security related devices may be paired directly to the _Z-Stick_ using its built in include / exclude button. Extra configuration is required to use a network security key with [OpenZWave][] and [Node-RED][]. Some _Z-Wave Plus_ devices will not work, or will work only in a restricted way, when paired directly with the _Z-Stick_ (i.e. without a security key).

[^jasco-nodes]: Node 5 corresponds to the device referred to as _JASCO 1_ in the [Node-RED][] flows and as _Holiday _Light_ in the dashboard; node 7 corresponds to the device referred to as _JASCO 2_ in the flows and as _Laser Up Light_ in the dashboard.

[^battery-powered-devices]: To enable battery powered devices in [node-red-contrib-openzwave][], check the box labeled "Allow updates from devices not fully scanned?" in the Z-Wave controller configuration node: ![](./images/z-wave-controller-settings.png) Note that such devices impose severe limits on the rate at which they send updates and wake up to listen for queued commands so they may appear "dead" for long periods while developing, debugging and testing [Node-RED][] flows.

[^sensor-checking-frequency]: As already noted, battery powered Z-Wave devices like the _Sensative Strips Comfort_ are always problematical. The commands sent to the [OpenZWave][] value id `12-132-1-0` are used to set the frequency, in seconds, the device will wake up to check for enqueued commands. The minimum supported value is 1800, meaning that it will not wake up any more frequently than once in each half hour so that is the fatest rate at which the device will respond unless you manually wake it up using its cumbersome, magnet-driven on-device "UI."

[mqtt]: https://mqtt.org/
[node-red]: https://nodered.org
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-contrib-suncalc]: https://flows.nodered.org/node/node-red-contrib-suncalc
[node-red-contrib-moment]: https://flows.nodered.org/node/node-red-contrib-moment
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-node-geofence]: https://flows.nodered.org/node/node-red-node-geofence
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-node-ui-led]: https://flows.nodered.org/node/node-red-node-ui-led
[node-red-contrib-semaphore]: https://flows.nodered.org/node/node-red-contrib-semaphore
[node-red-contrib-fan]: https://flows.nodered.org/node/node-red-contrib-fan
[owntracks]: https://owntracks.org
[hue]: https://www2.meethue.com/
[pi]: https://www.raspberrypi.org/
[mosquitto]: https://mosquitto.org/
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/
[zigbee]: https://zigbeealliance.org/
[javascript]: https://en.wikipedia.org/wiki/JavaScript
[openzwave]: http://openzwave.com/
[Z-Wave]: https://z-wavealliance.org
[z-stick]: https://aeotec.com/z-wave-usb-stick/
[hubitat]: https://hubitat.com/
[homie]: https://homieiot.github.io/
[Home Assistant]: https://www.home-assistant.io/
[OpenHAB]: https://www.openhab.org/
[Aeotec MultiSensor 6]: https://aeotec.com/z-wave-sensor/
[Sensative Strips Comfort]: https://sensative.com/sensors/strips-zwave/comfort/
[JASCO Outdoor Smart Switch]: https://byjasco.com/ge-z-wave-plus-plug-outdoor-smart-switch-500s
[Rhasspy]: https://rhasspy.readthedocs.io/en/latest/
[mic-array]: https://www.seeedstudio.com/ReSpeaker-Mic-Array-v2-0.html
[mic-hat]: https://www.seeedstudio.com/ReSpeaker-2-Mics-Pi-HAT.html
[touchscreen]: https://www.raspberrypi.org/products/raspberry-pi-touch-display/
