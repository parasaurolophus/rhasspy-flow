-- mysql -u kirk -p -h fs

use cheznous;

drop table if exists zwave_events;
drop table if exists zwave_values;
drop table if exists zwave_commands;
drop table if exists zwave_devices;

create table zwave_devices (
  nodeid int primary key,
  device_type varchar(255),
  manufacturer varchar(255),
  product varchar(255)
);

create table zwave_commands (
  cmdclass int primary key,
  cmdclass_label varchar(255)
);

create table zwave_values (
  nodeid int,
  cmdclass int,
  instance int,
  cmdidx int,
  cmdidx_label varchar(255),
  units varchar(255),
  value_type varchar(255),
  foreign key (nodeid)
    references zwave_devices(nodeid)
    on delete cascade,
  foreign key (cmdclass)
    references zwave_commands(cmdclass)
);

create table zwave_events (
  zwave_event_id int auto_increment primary key,
  ts datetime default current_timestamp not null,
  nodeid int not null,
  cmdclass int not null,
  instance int not null,
  cmdidx int not null,
  currState varchar(255),
  units varchar(255),
  foreign key (nodeid)
    references zwave_devices(nodeid)
    on delete cascade,
  foreign key (cmdclass)
    references zwave_commands(cmdclass)
    on delete cascade
);
