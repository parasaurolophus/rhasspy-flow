# "Closed Loop Feedback" Pattern

> This section uses a number of Unified Modeling Language (UML) diagrams for clarity. See [Understanding UML](../tutorial/uml.md) for more information if you are not familiar with how to interpret them.

Properly designed home automation is an example of what Software Engineers refer to as an _event-driven system_. A Node-RED flow is typically triggered by some _asynchronous event_ such as a user gesture on a dashboard, the arrival of a MQTT message or a signal that some device has changed state. Node-RED wakes up when such a triggering event occurs, executes some series of operations specified by the nodes in the flow that was trigger and then goes back to sleep until another event occurs.

The descriptive text for [node-red-dashboard][] _switch_ nodes (not to be confused with Node-RED's built-in flow-of-control _switch_ nodes... sigh) uses the phrase "closed loop feedback" when referring to a particular combination of settings but does not explain exactly what that is or why it is desirable. It is, in fact, one of the more generally useful patterns for Node-RED flows or any event-driven system.

In the context of dashboard switch controls connected to device nodes in a Node-RED flow, here is the "closed loop feedback" pattern depicted as a state machine:

```mermaid
stateDiagram-v2

  state Dashboard&nbsp;Switch&nbsp;Control {
    state "toggled on" as widget_on
    state "toggled off" as widget_off
  }

  state Physical&nbsp;On/Off&nbsp;Device {
    state "device on" as device_on
    state "device off" as device_off
  }

  device_on --> widget_on: send true<br/>when device<br/>state changes<br/>to on
  widget_on --> device_off: send false<br/>when clicked<br/>while widget<br/>is on
  widget_off --> device_on: send true<br/>when clicked<br/>while widget<br/>is off
  device_off --> widget_off: send false<br/>when device<br/>state changes<br/>to off
```

The assumptions here are:

1. Some piece of "smart" gear like a light, power outlet or the like responds to messages with boolean payloads by turning itself on or off

2. The same piece of gear will send a message with a boolean payload whenever its state changes _for whatever reason_ (e.g. the user directly interacts with its physical switch, other automation controls it etc. in addition to the depicted interaction with a dashboard control)

3. A [node-red-dashboard][] switch is wired up in the flow in such a way that the output of the GUI switch feeds the input of the physical device and vice versa (i.e. there is literal loop in the flow, as depicted in the following example)

4. The GUI switch is configured not to forward incoming messages and to display the state of its input (again as described in more detail below)

An actual example from <https://gitlab.com/parasaurolophus/cheznous/-/blob/master/flow.json> looks like:

![](living-room-dashboard-switch.png)

Here, the output of a [node-red-contrib-huemagic][] _group_ node configured to control the lights in the living room is sent to a _change_ node that moves `msg.payload.anyOn` (a boolean value indicating whether or not at least one light in the group is currently switched on) to `msg.payload`. The output of the change node is sent to the input of a [node-red-dashboard][] _switch_ node. The switch node's output is connected in a loop back to the input of the Hue group node.

The GUI _switch_ node is configured as:

![](living-room-dashboard-switch-settings.png)

See that the box labeled **Pass through msg if payload matches new state** is _not_ checked and **Switch icon shows the state of the input** is selected in the **indicator** drop-down list box. What this means is that the GUI switch will send a message with a boolean payload that is the opposite of the current state when clicked, but it will _not_ update its own state. It _will_ update its state to match the value it receives from an incoming message but will _not_ pass that message through. The physical device propagates messages to the GUI switch and vice-versa without causing any infinite event-firing loops. Variations of this arrangement are used not only to toggle Hue groups on and off but also to control Z-Wave outlets, to activate and deactivate motion sensors and so on.

Here is another real-world example, this time using a dashboard toggle switch to control an AC outlet using [node-red-contrib-openzwave][]:

![](zooz-5-dashboard-switch.png)

The preceding flow is sent state change messages from the Z-Wave device with a particular node id (note: _node id_ is used here in the sense defined by Z-Wave, not Node-RED). If `msg.value.cmdclss` is 37 indicating that it is reporting the outlets on/off state it caches the new value in flow context for later use and feeds `msg.payload` into a dashboard switch node that is configured in the same as that described above. The output of the dashboard switch node is transformed according to requirements of [node-red-contrib-openzwave][] and then sent to Z-Wave controller.

<a id="z-wave-start-up"></a>

A difference in the design of [node-red-contrib-openzwave][] and [node-red-contrib-huemagic][] is that the former, unlike the latter, does not automatically send the current state of Z-Wave devices when Node-RED starts up. To compensate, this flow is used to send a Z-Wave `RefreshValue` command to each device once at startup:

![](zooz-startup.png)

This causes each device to send its corresponding status message and everything proceeds as in the case already described for Hue devices. In both of the preceding flows, the output links connect to:

![](z-wave-output-flow.png)

The incoming Z-Wave messages originate from:

![](z-wave-input-flow.png)

In particular, the left-most incoming link to the flow shown above for the _Zooz 5_ device is connected to the bottom-most outgoing link the Z-Wave input flow.

Note also that unlike Philips Hue lights and groups, "smart" outlets like the Zooz powerstrip typically have physical power switches. I.e. the user can press a button or flip a switch in real life to toggle the outlet state in addition to having it controlled by automation. The complete state diagram for such devices looks like:

```mermaid
stateDiagram-v2

  state Physical&nbsp;Switch {
    state send_notify_off <<fork>>
    state send_notify_on <<fork>>
    device_off: off
    device_on: on
  }

  state Dashboard&nbsp;Control {
    control_off: off
    control_on: on
    state send_on_command <<fork>>
    state send_cmd_on <<fork>>
  }

  device_off --> send_notify_on: flip,cmd_on
  send_notify_on --> control_on: notify_on
  send_notify_on --> device_on: sent
  device_on --> send_notify_off: flip,cmd_off
  send_notify_off --> control_off: notify_off
  send_notify_off --> device_off: sent

  control_off --> send_on_command: clicked
  send_cmd_on --> control_on: sent
  send_cmd_on --> device_on: cmd_off
  control_on --> send_cmd_on: clicked
  send_on_command --> control_off: sent
  send_on_command --> device_off: cmd_off
```

The state transitions depicted by the diagram at the beginning of this section can be visualized as a series of annotated and color-coded flow charts highlighting the behavior implemented in Node-RED for each type of triggering event. Initially, assume the device and dashboard control are both off:

```mermaid
graph TD

  subgraph device states
    device_off("device off")
    device_on("device on")
  end

  subgraph dashboard switch states
    widget_off("toggled off")
    widget_on("toggled on")
  end

  device_off -. "send false<br/>when device<br/>state changes<br/>to off" .-> widget_off
  device_on -. "send true<br/>when device<br/>state changes<br/>to on" .-> widget_on
  widget_off -. "send true<br/>when clicked<br/>while off" .-> device_on
  widget_on -. "send false<br/>when clicked<br/>while on" .-> device_off

  classDef active fill:silver,color:black
  class device_off,widget_off active

  classDef inactive fill:white,color:gray
  class device_on,widget_on inactive

  linkStyle 0 color:gray,stroke-dasharray: 5, 5
  linkStyle 1 color:gray,stroke-dasharray: 5, 5
  linkStyle 2 color:gray,stroke-dasharray: 5, 5
  linkStyle 3 color:gray,stroke-dasharray: 5, 5
```

In this state, Node-RED is simply waiting for a relevant triggering event. When a user clicks the dashboard control while it is off, it sends `true`. It does not turn itself off because it is configured to show the state of its input rather than its output:

```mermaid
graph TD

  subgraph device states
    device_off("device off")
    device_on("device on")
  end

  subgraph dashboard switch states
    widget_off("toggled off")
    widget_on("toggled on")
  end

  device_off -. "send false<br/>when device<br/>state changes<br/>to off" .-> widget_off
  device_on -. "send true<br/>when device<br/>state changes<br/>to on" .-> widget_on
  widget_off -- "send true<br/>when clicked<br/>while off" --> device_on
  widget_on -. "send false<br/>when clicked<br/>while on" .-> device_off

  classDef active fill:silver,color:black
  class device_off,widget_off active

  classDef inactive fill:white,color:gray
  class device_on,widget_on inactive

  linkStyle 0 color:gray,stroke-dasharray: 5, 5
  linkStyle 1 color:gray,stroke-dasharray: 5, 5
  linkStyle 2 color:black
  linkStyle 3 color:gray,stroke-dasharray: 5, 5
```

When the device node receives `true` it, in turn, sends `true` after it successfully turns on the physical device:

```mermaid
graph TD

  subgraph device states
    device_off("device off")
    device_on("device on")
  end

  subgraph dashboard switch states
    widget_off("toggled off")
    widget_on("toggled on")
  end

  device_off -. "send false<br/>when device<br/>state changes<br/>to off" .-> widget_off
  device_on -. "send true<br/>when device<br/>state changes<br/>to on" .-> widget_on
  widget_off -. "send true<br/>when clicked<br/>while off" .-> device_on
  widget_on -. "send false<br/>when clicked<br/>while on" .-> device_off

  classDef active fill:silver,color:black
  class device_on,widget_off active

  classDef inactive fill:white,color:gray
  class device_off,widget_on inactive

  linkStyle 0 color:gray,stroke-dasharray: 5, 5
  linkStyle 1 color:black
  linkStyle 2 color:gray,stroke-dasharray: 5, 5
  linkStyle 3 color:gray,stroke-dasharray: 5, 5
```

When the dashboard control receives `true` it turns on:

```mermaid
graph TD

  subgraph device states
    device_off("device off")
    device_on("device on")
  end

  subgraph dashboard switch states
    widget_off("toggled off")
    widget_on("toggled on")
  end

  device_off -. "send false<br/>when device<br/>state changes<br/>to off" .-> widget_off
  device_on -. "send true<br/>when device<br/>state changes<br/>to on" .-> widget_on
  widget_off -. "send true<br/>when clicked<br/>while off" .-> device_on
  widget_on -. "send false<br/>when clicked<br/>while on" .-> device_off

  classDef active fill:silver,color:black
  class device_on,widget_on active

  classDef inactive fill:white,color:gray
  class device_off,widget_off inactive

  linkStyle 0 color:gray,stroke-dasharray: 5, 5
  linkStyle 1 color:gray,stroke-dasharray: 5, 5
  linkStyle 2 color:gray,stroke-dasharray: 5, 5
  linkStyle 3 color:gray,stroke-dasharray: 5, 5
```

This finishes the flow behavior in response to the user interface gesture because the dashboard control is configured not to pass messages through. Node-RED will again wait at this point for some other triggering event.

Later, when a user clicks the dashboard switch again. the dashboard control sends `false`, i.e. the dashboard control always sends a message with a payload indicating the opposite of the state it is in when clicked:

```mermaid
graph TD

  subgraph device states
    device_off("device off")
    device_on("device on")
  end

  subgraph dashboard switch states
    widget_off("toggled off")
    widget_on("toggled on")
  end

  device_off -. "send false<br/>when device<br/>state changes<br/>to off" .-> widget_off
  device_on -. "send true<br/>when device<br/>state changes<br/>to on" .-> widget_on
  widget_off -. "send true<br/>when clicked<br/>while off" .-> device_on
  widget_on -. "send false<br/>when clicked<br/>while on" .-> device_off

  classDef active fill:silver,color:black
  class device_on,widget_on active

  classDef inactive fill:white,color:gray
  class device_off,widget_off inactive

  linkStyle 0 color:gray,stroke-dasharray: 5, 5
  linkStyle 1 color:gray,stroke-dasharray: 5, 5
  linkStyle 2 color:gray,stroke-dasharray: 5, 5
  linkStyle 3 color:black
```

When the device node receives `false` it turns the physical device off and then sends `false`:

```mermaid
graph TD

  subgraph device states
    device_off("device off")
    device_on("device on")
  end

  subgraph dashboard switch states
    widget_off("toggled off")
    widget_on("toggled on")
  end

  device_off -. "send false<br/>when device<br/>state changes<br/>to off" .-> widget_off
  device_on -. "send true<br/>when device<br/>state changes<br/>to on" .-> widget_on
  widget_off -. "send true<br/>when clicked<br/>while off" .-> device_on
  widget_on -. "send false<br/>when clicked<br/>while on" .-> device_off

  classDef active fill:silver,color:black
  class device_off,widget_on active

  classDef inactive fill:white,color:gray
  class device_on,widget_off inactive

  linkStyle 0 color:black
  linkStyle 1 color:gray,stroke-dasharray: 5, 5
  linkStyle 2 color:gray,stroke-dasharray: 5, 5
  linkStyle 3 color:gray,stroke-dasharray: 5, 5
```

When the dashboard control receives `false` it turns off and we are back to the original state of affairs with both the dashboard control and device node in their off states and Node-RED waiting for more events.

```mermaid
graph TD

  subgraph device states
    device_off("device off")
    device_on("device on")
  end

  subgraph dashboard switch states
    widget_off("toggled off")
    widget_on("toggled on")
  end

  device_off -. "send false<br/>when device<br/>state changes<br/>to off" .-> widget_off
  device_on -. "send true<br/>when device<br/>state changes<br/>to on" .-> widget_on
  widget_off -. "send true<br/>when clicked<br/>while off" .-> device_on
  widget_on -. "send false<br/>when clicked<br/>while on" .-> device_off

  classDef active fill:silver,color:black
  class device_off,widget_off active

  classDef inactive fill:white,color:gray
  class device_on,widget_on inactive

  linkStyle 0 color:gray,stroke-dasharray: 5, 5
  linkStyle 1 color:gray,stroke-dasharray: 5, 5
  linkStyle 2 color:gray,stroke-dasharray: 5, 5
  linkStyle 3 color:gray,stroke-dasharray: 5, 5
```

So why arrange things in exactly this way? Why not leave the GUI switch with its default settings that would update its own state when clicked and pass messages through?

The default settings for a [node-red-dashboard][] switch node are fine if that is the only mechanism for controlling a given device and if Node-RED stays up and running continuously forever. In the real world, devices like Hue lights can be controlled directly by their native app, by voice assistants like Alexa, Google Assistant or (in our case) Rhasspy, by switches, motion sensors and so on and on. Also, Node-RED may go down at any time and stay down for any length of time. Using the default settings, Node-RED's GUI will not be automatically in sync with the current state of the underlying devices in such scenarios. Extra flow logic using cached state data would be required.

Using the "closed loop feedback" pattern, however, the state of the GUI stays in sync with the state of the devices with no additional effort required. If the corresponding devices change state while Node-RED is running for any reason, they send messages and the process repeats itself. The GUI switches track the state of the physical devices without the user clicking on the dashboard as if by magic. So long as the physical devices send messages indicating their new current state in a timely manner, the GUI switches will again receive messages indicating the state they should display. When the user sees the switch state change on the dashboard she can be confident that the device is actually in the desired state even if she is accessing the dashboard remotely and so cannot see the devices themselves.

Though the preceding state diagram is a far more evocative and concise way to visualize what is happening in an event-driven system, the more explicit and detailed flow-of-control depicted by a sequence diagram may help solidify the concepts underlying the "closed feedback loop" pattern. Here is a "sequence" for the use case where a user clicks on the toggle switch control in the dashboard:

```mermaid
sequenceDiagram

  participant user as User
  participant widget as Dashboard Control
  participant device as Device Node
  participant change as Change Node

  note over user,change: Pay close attention to the little x's indicating<br/>asynchronous interactions at critical steps!<br/>That is an essential aspect to how event-driven<br/>automation works and why this is such an important<br/>implementation pattern in the real world.

  loop forever
    user-Xwidget: click
    alt widget is on
      widget-Xdevice: payload: false
    else widget is off
      widget-Xdevice: payload: true
    end
    activate device
    alt payload = true
      device->>device: turn on
      device->>change: {anyOn: true}
    else payload = false
      device->>device: turn off
      device->>change: {anyOn: false}
    end
    deactivate device
    note over user,change: Any given "iteration" of this "loop" could start<br/>here, as shown in the next diagram, if the device<br/>changes state due to some other automation,<br/>direct operation by the native app etc.<br/>That is the whole point of this approach.
    activate change
    change->>change: payload := payload.anyOn
    change->>widget: payload
    deactivate change
    alt payload = true
      widget-Xwidget: turn on
    else payload = false
      widget-Xwidget: turn off
    end
  end
  note over user,change: The last operation by the dashboard<br/>control updating its state is marked as<br/>"asynchronous" to indicate that we are not<br/>actually looking at a loop but, rather, an<br/>event-driven system.
```

Paying close attention to the notes embedded in the preceding sequence diagram, we could render the same logic as a different arrangement of the same steps in depicting the case where a device "spontaneously" changes state, at least from the point of view of the dashboard switch control:

```mermaid
sequenceDiagram

  participant user as User
  participant assistant as Voice Assistant
  participant device as Device Node
  participant change as Change Node
  participant widget as Dashboard Control

  note over user,widget: Pay close attention to the little x's indicating<br/>asynchronous interactions at critical steps!<br/>That is an essential aspect to how event-driven<br/>automation works and why this is such an important<br/>implementation pattern in the real world.

  loop forever
    user-Xassistant: "Turn on the living room lights"
    assistant-Xdevice: {payload:true}
    activate device
    alt payload = true
      device->>device: turn on
      device->>change: {anyOn: true}
    else payload = false
      device->>device: turn off
      device->>change: {anyOn: false}
    end
    deactivate device
    activate change
    change->>change: payload := payload.anyOn
    change->>widget: payload
    deactivate change

    note over user,widget: Any given "iteration" of this "loop" could start<br/>here, as shown in the preceding diagram,<br/>if the dashboard control changes state due<br/>to a GUI gesture. That is the whole point<br/>of this approach.

    alt payload = true
      widget-Xwidget: turn on
    else payload = false
      widget-Xwidget: turn off
    end
  end
  note over user,widget: The last operation by the dashboard<br/>control updating its state is marked as<br/>"asynchronous" to indicate that we are not<br/>actually looking at a loop but, rather, an<br/>event-driven system.
```

## Trade-Offs

The primary down-side to the "closed loop feedback" approach is that there can be a noticeable delay between clicking on the GUI and seeing the switch state update (what System and Software Engineers refer to as "latency") depending on how long a given device takes to process the command and send out an updated status. This latency varies considerably between different makes and models of "smart" gear. I have a number of Z-Wave outlets in my setup for which the latency is so tiny as to be imperceptible. For Hue devices the latency can be enough to be noticed but is no worse when using this approach in Node-RED than when using the native app. I.e. the latency seems intrinsic to how the Hue bridge mediates the communication between the API it presents on the TCP/IP network and how it controls its paired devices via Zigbee. Overall, this small lag in the GUI is a very small price to pay for the simplicity yet robust behavior of the "closed loop feedback" pattern.

Another issue, [already discussed](#z-wave-start-up), arises in regards to how to "prime the pump" when flows start, ensuring that the initial state of the GUI reflects the current state of devices. If a device state's changed while Node-RED was not running then the "closed loop feedback" breaks down. In fact, the knowledgeable and observant reader may have already noticed that none of the state diagrams shown above include anything about state machine starting or ending states. To address this, all of my flows either benefit from the built-in behavior of _node-red-contrib-huemagic_ to send state messages automatically at start up or explicitly request the status of Z-Wave devices as discussed above. So the state machines do, in fact, operate as if they are always running with only a small amount of latency when the flows start before an inital batch of state change events appear, from the state machines' points of view, as if by magic.

## Other Use Cases

This pattern applies more generally than just [node-red-dashboard][] switches. The same pattern should be used, for example, when implementing a MQTT or WebSocket based API. In the case of MQTT, the corresponding implementation is:

1. Use separate topics to report status and issue commands, e.g. something like `device/state` to report the state of a device and `set/device/state` to issue a command to the device to change its state

2. Always set the `retain` flag to `true` when sending status messages; command messages should always be sent with `retain` set to `false`

3. Flows that send command messages should _not_ assume the command has taken effect when it is sent. Rather, their should be flows that act to update internal state based on incoming status messages from devices

If your flow subscribes to status messages and those status messages are retained by the broker, your Node-RED flows will automatically synchronize themselves at startup. Conversely, if you were to send retained commands they would inevitably be acted on multiple times in some cases, which in many circumstances will lead to chaotic behavior of your flows.

```mermaid
stateDiagram-v2

  state MQTT-based&nbsp;Client {
    client_on: device on
    client_off: device off
  }

  state Node-RED {
    control_on: dashboard control on
    control_off: dashboard control off
  }

  client_on --> control_on: retained message<br/>on topic</br>device/state<br/>with payload<br/>true

  client_off --> control_off: retained message<br/>on topic</br>device/state<br/>with payload<br/>false

  control_on --> client_on: unretained message<br/>on topic</br>set/device/state<br/>with payload<br/>true

  control_off --> client_off: unretained message<br/>on topic</br>set/device/state<br/>with payload<br/>false

  control_off --> control_on: automation,<br/>GUI etc.
  control_on --> control_off: automation,<br/>GUI etc.
```

The client may have its own internal logic such that its device can transition directly between states without receiving messages from Node-RED. This version of the "closed loop feedback" pattern will keep everything in sync, just as in the preceding examples, so long as a retained state change message is sent for each such transition.

A similar approach could be devised for WebSocket based or any other type of [publish/subscribe](../tutorial/api-patterns.md#publish-subscribe) API's. In that case, however, the lack of direct support for retained messages within the communications protocol means that extra effort would be needed to re-synchronize the client and server when either restarts. For MQTT, this is handled through the mediation provided by the broker.

Unlike WebSockets, so-called "web services" using normal HTTP [request / response](../tutorial/api-patterns.md#request-response) messaging are less amenable to this pattern since they directly support neither a publish / subscribe usage pattern nor state retention. Even more in-flow logic would be required to make devices or external clients that use web services conform which is why they are best avoided in home automation scenarios as much as practical.

## Exceptions

This pattern breaks down if the physical devices fail to send state change messages reliably and in a timely fashion. AC powered Z-Wave devices behave extraordinarily well in this regard but battery powered devices typically do not. This is because battery powered Z-Wave devices usually operate in a mode that trades responsiveness off for battery life. Depending on the particular make and model of device and how it is configured, a battery powered Z-Wave device would generally cause anywhere from tens of minutes to many hours' worth of latency if used with the "closed loop feedback" pattern. The alternative is to leave the GUI switch in such cases with its default settings and maintain a local cache of the last known state of such devices. The GUI will be much less accurate and reliable but at least it will be responsive. Users would just have to udnerstand and accept that what they saw in the dashboard in such cases does not necessarily reflect the current state of affairs but, rather, the most recent known state of affairs no matter how out of date that might be at any given moment.

[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
