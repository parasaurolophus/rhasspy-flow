# Dedicated Dashboard Clients

At a very high level of abstraction, my overall home automation network looks like:

```mermaid
graph TD

  client["Dashboard Kiosk /<br/>Voice Assistant<br/>(one per room)"]

  server["Home Automation Hub<br/>(Node-RED, Mosquitto,<br/>Z-Wave controller)"]

  bridge["Philips Hue Bridge"]
  hue["Multiple Hue Devices"]

  zwave["Multiple Z-Wave Devices"]

  client -- HTTPS / MQTT --- server
  server -- Z-Wave --- zwave
  server -- API over TCP/IP --- bridge
  bridge -- Zigbee --- hue
```

Zooming in on the _Dashboard Kiosk / Voice Assistant_ (_client_ for short), each room has a [Raspberry Pi](pi) with [touchscreen][], [mic array][mic-array] and [speaker][]. Each such client is configured to launch the [Node-RED][] dashboard using a web browser in "kiosk mode." It also has a [Rhasspy][] server listening for voice commands:

```mermaid
graph TD

  subgraph "Dashboard Kiosks / Voice Assistants (one per room)"
    subgraph Raspberry Pi
      rhasspy["Voice assistant (Rhasspy server)"]
      browser["Web Browser<br/>(dashboard in kiosk mode)"]
    end
    touchscreen[Touchscreen]
    mic[Mic array]
    speaker[Speaker]
  end

  server["Home Automation Hub<br/>(Node-RED, Mosquitto,<br/>Z-Wave controller)"]

  bridge["Philips Hue Bridge"]
  hue["Multiple Hue Devices"]

  zwave["Multiple Z-Wave Devices"]

  browser -- HTTPS --- server
  rhasspy -- MQTT --- server
  rhasspy --- mic
  mic --- speaker
  browser --- touchscreen
  server -- Z-Wave --- zwave
  server -- Wi-Fi --- bridge
  bridge -- Zigbee --- hue
```

This hardware and software set up supports both web-browser based dashboard and voice interactivity without sending information outside the home. By contrast, big-name automation hubs like Samsung SmartThings, Wink and voice assistants like Amazon Alexa and Google Assistant exchange detailed information including actual recordings of a room's occupants' speech to corporate-owned servers for every automation activity. In the case described here, all user activity including all voice processing are performed locally. [Rhasspy][] intents are automatically converted to MQTT messages that are sent to [Node-RED][] for execution as home automation commands.

## Dashboard Client

### Hardware

Each [Pi][] that is set up as a home automation client uses the following hardware setup:

- [Raspberry Pi 4B][pi] [<sup>*</sup>](#pi4)
- Touchscreen[<sup>**</sup>](#touchscreen)

#### Hardware Notes

<a id="pi4"></a>

<sup>*</sup> A Pi 3 or even lower would be more than sufficient as a dashboard kiosk. The extra CPU performance of a Pi 4 is required to function as a standalone [Rhasspy][] server. You could also configure lower end Pi's to run [Rhasspy][] as "satellites" sending all speech-to-text and intent processing to a central server. You then risk saturating your Wi-Fi bandwidth depending on how many satellites you set up and how many are active at a time (unless, of course, you have an Ethernet drop in every room).

<a id="touchscreen"></a>

<sup>**</sup> I have successfully used both the [Official 7" Touchscreen][touchscreen] and third-party alternatives that plug into a USB port and act like plug-and-play mice. The former is more tightly integrated from a hardware point of view, the latter are far easier to set up if you are willing to use standard USB and HDMI cabling. The going price at the time of this writing is about the same for 7" models when ordered from big online retailers in the US.

### Software

- Use _unclutter_ to remove the mouse pointer when using a touchscreen in kiosk mode

    ```bash
    sudo apt install unclutter
    ```

* Configure the [Pi][] to automatically log in as your preferred user[<sup>&dagger;</sup>](#autologin)

* Configure the dashboard to open in kiosk mode when the _autologin_ user environment starts up[<sup>&ddagger;</sup>](#kiosk)

#### Software Notes

<a id="autologin"></a>

<sup>&dagger;</sup> While you can use `raspi-config` to enable automatic login for the user `pi`, running as `pi` is not a good practice from a security point of view. To enable automatic login for an arbitrary user, edit _/etc/lightdm/lightdm.conf_:

```bash
sudo nano /etc/lightdm/lightdm.conf
```

Uncomment and modify the following lines:

```
autologin-user=mydashboarduser
autologin-user-timeout=0
```

replacing `mydashboarduser` with your actual dashboard user's name. Note that this should be the same user as that which runs [Rhasspy][] to keep things simple.

<a id="kiosk"></a>

<sup>&ddagger;</sup> Place the following in _~/.config/lxsession/LXDE-pi/autostart_:

```
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
@unclutter -idle 0
@sed -i 's/"exited_cleanly": false/"exited_cleanly": true/' ~/.config/chromium-browser Default/Preferences
@chromium-browser --incognito --kiosk https://someuser:somepassword@noderedhost:1880/ui/
```

replaceing _someuser_, _somepassword_ and _noderedhost_ with the credentials and host name for accessing your [Node-RED][] server's dashboard.

## Voice Assistant

> **Warning!** You should use a case with a fan or some other form of active cooling for any [Raspberry Pi][pi] running [Rhasspy][]. If you use an all-in-one case for the combination of a touchscreen and [Pi][] make sure to choose one that supports a fan. Similarly, the choice of mic could impact the choice of case if, for example, you choose one in the form of a HAT.

### Hardware

- Mic array suitable for speech-to-text processing[<sup>&sect;</sup>](#microphone)
- Speaker compatible with your voice capture mic[<sup>&sect;</sup>](#microphone)

#### Hardware Notes

<a id="microphone"></a>

<sup>&sect;</sup>  Getting a mic / speaker combination working with [Rhasspy][] is by far the trickiest part of this setup. The only configuration I have personally found successful is the rather pricey [ReSpeaker V2.0 USB Mic Array][mic-array] with a standalone analog speaker plugged into to the mic array's 3.5mm output jack. Do not expect [Rhasspy][] to respond reliably, if at all, to ordinary desktop microphones. Also, do not expect echo cancellation nor a smooth hand-off between audio drivers when trying to use the ReSpeaker USB mic array and the built-in audio output in the [Pi][]. I have heard reports that the more modestly priced ReSpeaker Mic HAT also works well with [Rhasspy][] but I have not personally tried it.

### Software

- [Rhasspy][] [<sup>&verbar;</sup>](#rhasspy)

#### Software Notes

<a id="rhasspy"></a>

<sup>&verbar;</sup> Configuring [Rhasspy][] requires an [entire section](../features/voice-assistance.md) of its own.

[mqtt]: http://mqtt.org/
[node-red]: https://nodered.org
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-node-geofence]: https://flows.nodered.org/node/node-red-node-geofence
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-node-ui-table]: https://flows.nodered.org/node/node-red-node-ui-table
[owntracks]: https://owntracks.org
[hue]: https://www2.meethue.com/
[pi]: https://www.raspberrypi.org/
[mosquitto]: https://mosquitto.org/
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/
[zigbee]: https://zigbeealliance.org/
[javascript]: https://en.wikipedia.org/wiki/JavaScript
[openzwave]: http://openzwave.com/
[Z-Wave]: https://z-wavealliance.org
[z-stick]: https://aeotec.com/z-wave-usb-stick/
[hubitat]: https://hubitat.com/
[homie]: https://homieiot.github.io/
[Home Assistant]: https://www.home-assistant.io/
[OpenHAB]: https://www.openhab.org/
[Aeotec MultiSensor 6]: https://aeotec.com/z-wave-sensor/
[Sensative Strips Comfort]: https://sensative.com/sensors/strips-zwave/comfort/
[JASCO Outdoor Smart Switch]: https://byjasco.com/ge-z-wave-plus-plug-outdoor-smart-switch-500s
[Zooz S2 Power Strip]: https://www.getzooz.com/zooz-zen20-power-strip.html
[Rhasspy]: https://rhasspy.readthedocs.io/en/latest/
[mic-array]: https://www.seeedstudio.com/ReSpeaker-Mic-Array-v2-0.html
[mic-hat]: https://www.seeedstudio.com/ReSpeaker-2-Mics-Pi-HAT.html
[touchscreen]: https://www.raspberrypi.org/products/raspberry-pi-touch-display/
[speaker]: https://www.logitech.com/en-us/product/multimedia-speaker-z50
