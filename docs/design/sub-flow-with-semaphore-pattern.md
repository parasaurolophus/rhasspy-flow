# Sub-Flow With Semaphore Pattern

[Multi-threaded, event-driven systems](../tutorial/parallel-execution.md) like [Node-RED][] require mechanisms to allow multiple threads to share access to individual "resources" such as a connection to a database, the API for some device controller and so on.

When sending queries to a database using [node-red-node-mysql][], for example, you need to make sure not to send messages to a `mysql` node too close together in time or it may causes errors. One way to achieve this would be to have only a single `mysql` node throughout all of your flows. All queries sent to the database would have to be routed through that one node. You could then impose an _ad hoc_ rate limit by putting a `delay` node in front of the `mysql` node. As long as the rate limit was long enough to avoid the timing bug inside `mysql`, everything should work. There are a few problems with this approach. It involves guess-work as to how long the delay needs to be and it also makes it difficult to make use of the results of any given query from any given point in a flow if they all have to pass through a single node. A far better approach is to use explicit thread synchronization facilities in your flows, such as those provided by [node-red-contrib-semaphore][].

Here is the sub-flow I use to send database query and command messages from anywhere in my flows and receive their results synchronously:

![](sql-command-subflow.png)

There is also another variant that is used specifically to send SQL `insert` commands based on the key / vaue pairs in JSON objects:

![](sql-insert-subflow.png)

There will be as many `mysql` nodes as there are instances of these sub-flows. Each instance of the sub-flow will act as a single, independent node that receives a message containing a SQL statement and emits the results of running the statement on the MySQL database:

![](z-wave-metadata-flow.png)

All the sub-flow instances use `semaphore-take` nodes for the same [semaphore](https://en.wikipedia.org/wiki/Semaphore_(programming)) before accessing the database and use `semaphore-leave` as soon they are done. The semaphore is configured to have a resource count of 1. This means that only a single thread at a time can access the database no matter how many asynchronous paths contain instances of this sub-flow. If multiple threads try to send SQL commands at the same time, which ever one's flow enters `semaphore-take` first will have the opportunity to complete its use of the database before any other is allowed to access it. The additional threads will be forced to wait when their flows enter `semaphore-take` until the first thread's flow exits `semaphore-leave`. This has the desired effect of not introducing any _ad hoc_ delays that are risky in terms of actually solving the synchronization issue and can slow the overall flow down unnecessarily. It also gives any flow easy access to the result of any SQL query directly at the point in the flow at which it is needed which would be difficult to achieve using a "singleton" `mysql` node.

Note the use of a `catch` node to make sure that `semaphore-take` is invoked even if the `mysql` node encounters an error. This is a critical aspect of this pattern. Failing to release locking mechanisms like semaphores is one very common source of thread synchronization bugs. Another critical aspect of this pattern is that the `mysql` node can be relied on always to send an output message other than cases where it throws an exception. Again, this is critical to making sure that the semaphore is made available to other threads once a given one has finished its current database operation. While this semaphore based technique can be used with other kinds of nodes representing shared resources, not all node types offer the required behavior.

For example, some node types are designed specifically as terminal outputs of a flow. Such nodes can accept inputs but have no output path of their own. The built-in `debug` node works this way as do many node types that send messages to hardware devices. This is in accordance with [Node-RED][]'s maintainers' style guidelines for node developers. See the "General guidance" section at <https://nodered.org/docs/creating-nodes/>, in particular:

> - sit at the beginning, middle or end of a flow - not all at once.

But this can make it challenging to use such nodes safely in a multi-threaded, event-driven context -- which is also mandated by [Node-RED][]'s overall design. To address this, [Node-RED][] version 1.0 introduced a new built-in type of node, `complete`. A `complete` node can be used to detect when another node finishes execution even if that other node does not send an output of its own. This allows a flow to continue executing safely after, for example, sending a command to a device using a node that is designed to "sit at the end." This is a far more common requirement than it would appear the authors of those style guidelines originally considered, hence their decision to add the `complete` node as a way of circumventing these limitations in a backwards-compatible fashion. The trouble is that at the time of this writing, relatively few third-party node types are implemented in the way required to support the use of `complete` nodes.

As a concrete example, the `zwave-out` node from [node-red-contrib-openzwave][] has a similar behavioral issue to that of `mysql` nodes described above. Sending too many [Z-Wave][] commands too close together in time causes the [node-red-contrib-openzwave][] package to fail. Unlike the `mysql` node, the `zwave-out` node cannot be relied on to send a message by which to continue a flow even in cases where the [Z-Wave][] command was successful. I.e. even though some [Z-Wave][] commands do return results and so the `zwave-out` node can have an output, most commands do not send an output message and flow execution stops whether or not the command was successful just as for any node that is designed to "sit at the end." (The [node-red-contrib-openzwave][] package developers are attempting here to both have their cake of conforming to [Node-RED][] node design guidelines and eat it too in supporting those [OpenZWave][] commands that do, in fact, send an output using a single type of `zwave-out` node. Looking at the source, I suspect that the `zwave-out` node was originally designed not to ever generate output messages and that feature was added some time later as part of expanding its repertoire of [OpenZWave][] commands.) At the time of this writing, the released version of [node-red-contrib-openzwave][] does not implement the features required to support the `complete` node as a work-around for continuing execution after a flow has reached a terminal node.

To remedy this, I created a fork of the [node-red-contrib-openzwave][] package, <https://github.com/parasaurolophus/node-red-contrib-openzwave>. In it, I added the ability to use the `complete` node to the implementation of `zwave-out`. In this way, I am able to use the following sub-flow for issuing [Z-Wave][] commands in a fashion that is analogous to that shown above for `mysql` nodes:

![](z-wave-out-subflow.png)

Note the apparent discontinuity between the sub-flow's input and output. This works because the `complete` and `catch` nodes, between them, ensure that the flow will continue and thus the semaphore will be released as soon as the `zwave-out` node has finished sending the given command to the [Z-Wave][] controller. _**Note again:** this will not work with the currently released version of [node-red-contrib-openzwave][]. It relies on modifications to `zwave-out` behavior in my fork. Hopefully, the [node-red-contrib-openzwave][] maintainers will accept my pull request and then this pattern will work for everyone using the official version of that package._

Finally, though it is not visually obvious from the graphical representation of the preceding three sub-flows, there are two separate semaphores in use. One is for `mysql` and the other for `zwave-out`. This is because while it causes issues for `mysql` if SQL statements are sent too close together in time and for `zwave-out` if Z-Wave messages are sent too close together in time, neither type of node interferes with the operation of the other. If a single semaphore were used in both sub-flows, SQL commands would be blocked from time to time waiting for Z-Wave commands to complete and vice-versa. This would sometimes cause unnecessary delays in the overall flow execution.

[mqtt]: http://mqtt.org/
[node-red]: https://nodered.org
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-contrib-suncalc]: https://flows.nodered.org/node/node-red-contrib-suncalc
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-node-mysql]: https://flows.nodered.org/node/node-red-node-mysql
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-contrib-semaphore]: https://flows.nodered.org/node/node-red-contrib-semaphore
[node-red-node-ui-table]: https://flows.nodered.org/node/node-red-node-ui-table
[owntracks]: https://owntracks.org
[hue]: https://www2.meethue.com/
[pi]: https://www.raspberrypi.org/
[mosquitto]: https://mosquitto.org/
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/
[zigbee]: https://zigbeealliance.org/
[javascript]: https://en.wikipedia.org/wiki/JavaScript
[openzwave]: http://openzwave.com/
[Z-Wave]: https://z-wavealliance.org
[z-stick]: https://aeotec.com/z-wave-usb-stick/
[hubitat]: https://hubitat.com/
[homie]: https://homieiot.github.io/
[Home Assistant]: https://www.home-assistant.io/
[OpenHAB]: https://www.openhab.org/
[Aeotec MultiSensor 6]: https://aeotec.com/z-wave-sensor/
[Sensative Strips Comfort]: https://sensative.com/sensors/strips-zwave/comfort/
[JASCO Outdoor Smart Switch]: https://byjasco.com/ge-z-wave-plus-plug-outdoor-smart-switch-500s
[Zooz S2 Power Strip]: https://www.getzooz.com/zooz-zen20-power-strip.html
[Rhasspy]: https://rhasspy.readthedocs.io/en/latest/
[mic-array]: https://www.seeedstudio.com/ReSpeaker-Mic-Array-v2-0.html
[mic-hat]: https://www.seeedstudio.com/ReSpeaker-2-Mics-Pi-HAT.html
[touchscreen]: https://www.raspberrypi.org/products/raspberry-pi-touch-display/
[speaker]: https://www.logitech.com/en-us/product/multimedia-speaker-z50
