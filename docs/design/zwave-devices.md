# Z-Wave Devices

To understand the contents of the tables described below, it is necessary to have some familiarity with the device monitoring and control paradigm defined by the [Z-Wave Alliance][Z-Wave] as represented by the [OpenZWave][] library and exposed to [Node-RED][] via the [node-red-contrib-openzwave][] package.

[Z-Wave][] devices inhabit a [mesh network](https://en.wikipedia.org/wiki/Mesh_networking). Each device in the mesh is identified by its node id (_nodeid_). Each [Z-Wave][] network requires at least one _controller_, in my case an [Aeotec Z-Stick Gen5][z-stick] plugged into a USB port of the [Raspberry Pi][pi] running [Node-RED][]. Devices are added as nodes in a Z-Wave mesh through a process known as _inclusion_. When a controller includes a device, it assigns it a node id and queries it for self-describing [metadata](https://en.wikipedia.org/wiki/Metadata) and initial state.

The metadata each device provides to the controller includes a description of all the values that device supports. "Values," in this sense, define the structure and content of messages that are exchanged between that device and others in the same mesh. Some values are used to report events like changes in measurements (temperature, light level, electric power consumption etc.) Other values correspond to instantaneous events like motion detected, door opened and so on. All of these messages arising from devices are used to drive [Node-RED][] flows in an [event-driven](https://en.wikipedia.org/wiki/Event-driven_architecture) fashion.

The [Z-Wave][] specification also mandates the use of these same kinds of "values" as commands. For example, there is only a very small difference between a message sent by what the [Z-Wave][] specification refers to as a "binary switch" device to report that it was turned on or off manually and the message sent to it to instruct it to turn on or off under software control. For this reason, values are grouped into what are called "command classes." In order to simplify the implementation of [Z-Wave][] monitoring and control software, the [Z-Wave Alliance][Z-Wave] has predefined a number of generic types of devices, each of which is expected to support similar kinds of properties using these pre-defined command classes. For example, "binary switch" devices are expected to support values with command class 37 (a.k.a. `COMMAND_CLASS_SWITCH_BINARY`). Devices like dimmers that support a range of numbers as a value are expected to support values with command class 38 (`COMMAND_CLASS_SWITCH_MULTILEVEL`). And so on.

The identifier of a "value" in the preceding sense is actually a four-port [tuple](https://en.wikipedia.org/wiki/Tuple), referred to in the [OpenZWave][] library code as a "value id." The first two components of a value id are the node id and command class described above. The third component of a value is the "instance id." Most devices use only the single value, 1, in all value messages they send and consume. Some devices, however, represent a collection of individual sub-devices. The [Zooz S2 Power Strip][], for example, consists of five individually controllable AC outlets and two USB ports. It also has a master switch to control all five outlets at once. It uses instance id's 1 - 8 to denote the master switch, the five outlets and the two USB ports, respectively. The fourth component of a value id is its "command index." As already discussed, command classes correspond to a pre-defined repertoire of properties and commands for generic types of devices with standardized behavior like binary switches that can only be turned on or off, dimmers that can be set to a particular level and the like. In order to support "smart" devices with more advanced and unique behavior a given value can have multiple "sub-commands" for a given command class. The command index is used to specify a particular such sub-command. Devices which only support the standard behavior for a given command class use a command index of 0. Devices with more advanced or exotic behavior can use additional values as command indices. This mechanism allows device manufacturers to produce devices with custom features without having to lobby the [Z-Wave][] consortium for new, unique command class class values.

As concrete examples, the value id `10-50-6-2` is used by my [Zooz S2 Power Strip][] (node 10) to report power consumption measurements (50 = `COMMAND_CLASS_METER`) for the for the AC outlet labeled "Ch 5" (instance 6) in Watts (command index 2). The same device reports the cumulative power consumption of the same instance in kWh using the value id `10-50-6-0`. Note that the preceding value id's differ only in their command indices. That same device reports the on / off state of the AC outlet labeled "Ch 1" as `10-37-2-0`. One of my two [JASCO Outdoor Smart Switch][] devices reports its on / off state using value id `5-37-1-0` and the other using `7-37-1-0`. I.e. their respective node id's are 5 and 7 while the rest of the value id components are the same for the same type of command or value. Since, despite the word "Smart" in the product names, the JASCO devices are simple binary switch devices with only a single instance each an no power meter capabilities, the latter two value id's are the only ones relevant to this discussion for those particular devices.

The [node-red-contrib-openzwave][] package uses the [OpenZWave][] library to communicate with the Z-Wave controller. When the [OpenZWave][] library initiates communication on behalf of [Node-RED][] with a controller like the [Z-Stick][], it writes metadata for all currently included devices describing the whole [Z-Wave][] network to a file named _~/.node-red/ozwchache_XXXX.xml_, where _XXXX_ is a [hexadecimal](https://en.wikipedia.org/wiki/Hexadecimal) string representing the local _home id_ for the given controller. The flow on the _Z-Wave Metadata_ tab reads this cached metadata and stores salient bits into a [MySQL][] database. In addition, The flow on the _Z-Wave_ tab can be configured to log each incoming [Z-Wave][] event message to the database. This data can be used in UI's, external monitoring and analytics applications and so on.

_**Note:** Throughout this document, the terminology and programming paradigm used are those defined by the [OpenZWave][] library and the [node-red-contrib-openzwave][] package that uses it. These differ in significant ways from the underlying [Z-Wave][] protocol specifications. For example, the [node-red-contrib-openzwave][] package uses exactly the same JSON payload objects to send commands and receive corresponding status messages while the actual [Z-Wave][] packets traveling across the mesh network have slightly different contents for messages sent to devices as commands and those sent by devices to report status._

## Z-Wave Nodes

The `zwave_devices` table contains significant information about each node in the [Z-Wave][] network. It is populated by reading _~/.node-red/ozwchache_XXXX.xml_ as described above. Since this data changes only very infrequently, i.e. when nodes are included or excluded in the network, populating this table is initiated manually using a `trigger` node on the _Z-Wave Metadata_ flow.

Here is the contents of `zwave_devices` as of the time of this writing:

    > select * from zwave_devices;

| nodeid | device_type               | manufacturer        | product                     |
|--------|---------------------------|---------------------|-----------------------------|
|      1 | Static PC Controller      | AEON Labs           | ZW090 Z-Stick Gen5 US       |
|      5 | Binary Power Switch       | Unknown: id=0063    | Unknown: type=6363, id=3130 |
|      7 | Binary Power Switch       | GE (Jasco Products) | 12720 Outdoor Smart Switch  |
|     10 | Power Strip               | Zooz                | ZEN20 V2.0 Power Strip      |
|     12 | Routing Multilevel Sensor | Sensative           | Strips Comfort              |
|     14 | Routing Multilevel Sensor | AEON Labs           | ZW100 MultiSensor 6         |

It should be noted that nodes 5 and 7 are actually the same type of physical device. Node 5 is the JASCO-branded version, while node 7 is the version JASCO supplies as an OEM to GE. Other than the difference in how these devices identify themselves in metadata, they function identically. Node 5 is referred to as `JASCO 1` in the [Node-RED][] flows. Node 7 is referred to as `JASCO 2`.

## Z-Wave Command Classes

The pre-defined [Z-Wave][] command classes supported by the devices listed above are stored in the `zwave_commands` table:

    > select * from zwave_commands;

| cmdclass | cmdclass_label                          |
|----------|-----------------------------------------|
|       32 | COMMAND_CLASS_BASIC                     |
|       37 | COMMAND_CLASS_SWITCH_BINARY             |
|       39 | COMMAND_CLASS_SWITCH_ALL                |
|       48 | COMMAND_CLASS_SENSOR_BINARY             |
|       49 | COMMAND_CLASS_SENSOR_MULTILEVEL         |
|       50 | COMMAND_CLASS_METER                     |
|       90 | COMMAND_CLASS_DEVICE_RESET_LOCALLY      |
|       94 | COMMAND_CLASS_ZWAVEPLUS_INFO            |
|       96 | COMMAND_CLASS_MULTI_INSTANCE/CHANNEL    |
|      112 | COMMAND_CLASS_CONFIGURATION             |
|      113 | COMMAND_CLASS_NOTIFICATION              |
|      114 | COMMAND_CLASS_MANUFACTURER_SPECIFIC     |
|      115 | COMMAND_CLASS_POWERLEVEL                |
|      117 | COMMAND_CLASS_PROTECTION                |
|      119 | COMMAND_CLASS_NODE_NAMING               |
|      128 | COMMAND_CLASS_BATTERY                   |
|      132 | COMMAND_CLASS_WAKE_UP                   |
|      133 | COMMAND_CLASS_ASSOCIATION               |
|      134 | COMMAND_CLASS_VERSION                   |
|      142 | COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION |

## Z-Wave Values

By providing the kind of metadata contained in _ozwcache_XXXX.xml_, the [Z-Wave][] protocol makes devices "discoverable" and supports constructing monitoring and control dashboards in a [data-driven](https://en.wikipedia.org/wiki/Data-driven_programming) fashion. The _zwave_values_ table can be used for such purposes:

    > select
        nodeid,
        cmdclass_label,
        instance,
        cmdidx,
        cmdidx_label,
        units,
        value_type
      from zwave_values
      join zwave_commands on zwave_values.cmdclass = zwave_commands.cmdclass
      order by
        nodeid,
        zwave_values.cmdclass,
        instance,
        cmdidx;

| nodeid | cmdclass_label                      | instance | cmdidx | cmdidx_label                                      | units   | value_type |
|--------|-------------------------------------|----------|--------|---------------------------------------------------|---------|------------|
|      1 | COMMAND_CLASS_BASIC                 |        1 |      0 | Basic                                             |         | byte       |
|      1 | COMMAND_CLASS_CONFIGURATION         |        1 |     81 | LED indicator configuration                       |         | list       |
|      1 | COMMAND_CLASS_CONFIGURATION         |        1 |    220 | Configuration of the RF power level               |         | list       |
|      1 | COMMAND_CLASS_CONFIGURATION         |        1 |    242 | Security network enabled                          |         | list       |
|      1 | COMMAND_CLASS_CONFIGURATION         |        1 |    243 | Security network key                              |         | int        |
|      1 | COMMAND_CLASS_CONFIGURATION         |        1 |    252 | Lock/Unlock Configuration                         |         | list       |
|      1 | COMMAND_CLASS_CONFIGURATION         |        1 |    255 | Reset default configuration                       |         | int        |
|      1 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      0 | Loaded Config Revision                            |         | int        |
|      1 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      1 | Config File Revision                              |         | int        |
|      1 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      2 | Latest Available Config File Revision             |         | int        |
|      1 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      3 | Device ID                                         |         | string     |
|      1 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      4 | Serial Number                                     |         | string     |
|      5 | COMMAND_CLASS_SWITCH_BINARY         |        1 |      0 | Switch                                            |         | bool       |
|      5 | COMMAND_CLASS_SWITCH_ALL            |        1 |      0 | Switch All                                        |         | list       |
|      5 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      0 | Loaded Config Revision                            |         | int        |
|      5 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      1 | Config File Revision                              |         | int        |
|      5 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      2 | Latest Available Config File Revision             |         | int        |
|      5 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      3 | Device ID                                         |         | string     |
|      5 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      4 | Serial Number                                     |         | string     |
|      5 | COMMAND_CLASS_POWERLEVEL            |        1 |      0 | Powerlevel                                        | dB      | list       |
|      5 | COMMAND_CLASS_POWERLEVEL            |        1 |      1 | Timeout                                           | seconds | byte       |
|      5 | COMMAND_CLASS_POWERLEVEL            |        1 |      2 | Set Powerlevel                                    |         | button     |
|      5 | COMMAND_CLASS_POWERLEVEL            |        1 |      3 | Test Node                                         |         | byte       |
|      5 | COMMAND_CLASS_POWERLEVEL            |        1 |      4 | Test Powerlevel                                   | dB      | list       |
|      5 | COMMAND_CLASS_POWERLEVEL            |        1 |      5 | Frame Count                                       |         | short      |
|      5 | COMMAND_CLASS_POWERLEVEL            |        1 |      6 | Test                                              |         | button     |
|      5 | COMMAND_CLASS_POWERLEVEL            |        1 |      7 | Report                                            |         | button     |
|      5 | COMMAND_CLASS_POWERLEVEL            |        1 |      8 | Test Status                                       |         | list       |
|      5 | COMMAND_CLASS_POWERLEVEL            |        1 |      9 | Acked Frames                                      |         | short      |
|      5 | COMMAND_CLASS_PROTECTION            |        1 |      0 | Protection                                        |         | list       |
|      5 | COMMAND_CLASS_VERSION               |        1 |      0 | Library Version                                   |         | string     |
|      5 | COMMAND_CLASS_VERSION               |        1 |      1 | Protocol Version                                  |         | string     |
|      5 | COMMAND_CLASS_VERSION               |        1 |      2 | Application Version                               |         | string     |
|      7 | COMMAND_CLASS_SWITCH_BINARY         |        1 |      0 | Switch                                            |         | bool       |
|      7 | COMMAND_CLASS_SWITCH_ALL            |        1 |      0 | Switch All                                        |         | list       |
|      7 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      0 | Loaded Config Revision                            |         | int        |
|      7 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      1 | Config File Revision                              |         | int        |
|      7 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      2 | Latest Available Config File Revision             |         | int        |
|      7 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      3 | Device ID                                         |         | string     |
|      7 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      4 | Serial Number                                     |         | string     |
|      7 | COMMAND_CLASS_POWERLEVEL            |        1 |      0 | Powerlevel                                        | dB      | list       |
|      7 | COMMAND_CLASS_POWERLEVEL            |        1 |      1 | Timeout                                           | seconds | byte       |
|      7 | COMMAND_CLASS_POWERLEVEL            |        1 |      2 | Set Powerlevel                                    |         | button     |
|      7 | COMMAND_CLASS_POWERLEVEL            |        1 |      3 | Test Node                                         |         | byte       |
|      7 | COMMAND_CLASS_POWERLEVEL            |        1 |      4 | Test Powerlevel                                   | dB      | list       |
|      7 | COMMAND_CLASS_POWERLEVEL            |        1 |      5 | Frame Count                                       |         | short      |
|      7 | COMMAND_CLASS_POWERLEVEL            |        1 |      6 | Test                                              |         | button     |
|      7 | COMMAND_CLASS_POWERLEVEL            |        1 |      7 | Report                                            |         | button     |
|      7 | COMMAND_CLASS_POWERLEVEL            |        1 |      8 | Test Status                                       |         | list       |
|      7 | COMMAND_CLASS_POWERLEVEL            |        1 |      9 | Acked Frames                                      |         | short      |
|      7 | COMMAND_CLASS_PROTECTION            |        1 |      0 | Protection                                        |         | list       |
|      7 | COMMAND_CLASS_VERSION               |        1 |      0 | Library Version                                   |         | string     |
|      7 | COMMAND_CLASS_VERSION               |        1 |      1 | Protocol Version                                  |         | string     |
|      7 | COMMAND_CLASS_VERSION               |        1 |      2 | Application Version                               |         | string     |
|     10 | COMMAND_CLASS_SWITCH_BINARY         |        1 |      0 | Switch                                            |         | bool       |
|     10 | COMMAND_CLASS_SWITCH_BINARY         |        2 |      0 | Switch                                            |         | bool       |
|     10 | COMMAND_CLASS_SWITCH_BINARY         |        3 |      0 | Switch                                            |         | bool       |
|     10 | COMMAND_CLASS_SWITCH_BINARY         |        4 |      0 | Switch                                            |         | bool       |
|     10 | COMMAND_CLASS_SWITCH_BINARY         |        5 |      0 | Switch                                            |         | bool       |
|     10 | COMMAND_CLASS_SWITCH_BINARY         |        6 |      0 | Switch                                            |         | bool       |
|     10 | COMMAND_CLASS_SWITCH_BINARY         |        7 |      0 | Switch                                            |         | bool       |
|     10 | COMMAND_CLASS_SWITCH_BINARY         |        8 |      0 | Switch                                            |         | bool       |
|     10 | COMMAND_CLASS_METER                 |        1 |      0 | Electric - kWh                                    | kWh     | decimal    |
|     10 | COMMAND_CLASS_METER                 |        1 |      2 | Electric - W                                      | W       | decimal    |
|     10 | COMMAND_CLASS_METER                 |        1 |    256 | Exporting                                         |         | bool       |
|     10 | COMMAND_CLASS_METER                 |        1 |    257 | Reset                                             |         | button     |
|     10 | COMMAND_CLASS_METER                 |        2 |      0 | Electric - kWh                                    | kWh     | decimal    |
|     10 | COMMAND_CLASS_METER                 |        2 |      2 | Electric - W                                      | W       | decimal    |
|     10 | COMMAND_CLASS_METER                 |        2 |    256 | Exporting                                         |         | bool       |
|     10 | COMMAND_CLASS_METER                 |        2 |    257 | Reset                                             |         | button     |
|     10 | COMMAND_CLASS_METER                 |        3 |      0 | Electric - kWh                                    | kWh     | decimal    |
|     10 | COMMAND_CLASS_METER                 |        3 |      2 | Electric - W                                      | W       | decimal    |
|     10 | COMMAND_CLASS_METER                 |        3 |    256 | Exporting                                         |         | bool       |
|     10 | COMMAND_CLASS_METER                 |        3 |    257 | Reset                                             |         | button     |
|     10 | COMMAND_CLASS_METER                 |        4 |      0 | Electric - kWh                                    | kWh     | decimal    |
|     10 | COMMAND_CLASS_METER                 |        4 |      2 | Electric - W                                      | W       | decimal    |
|     10 | COMMAND_CLASS_METER                 |        4 |    256 | Exporting                                         |         | bool       |
|     10 | COMMAND_CLASS_METER                 |        4 |    257 | Reset                                             |         | button     |
|     10 | COMMAND_CLASS_METER                 |        5 |      0 | Electric - kWh                                    | kWh     | decimal    |
|     10 | COMMAND_CLASS_METER                 |        5 |      2 | Electric - W                                      | W       | decimal    |
|     10 | COMMAND_CLASS_METER                 |        5 |    256 | Exporting                                         |         | bool       |
|     10 | COMMAND_CLASS_METER                 |        5 |    257 | Reset                                             |         | button     |
|     10 | COMMAND_CLASS_METER                 |        6 |      0 | Electric - kWh                                    | kWh     | decimal    |
|     10 | COMMAND_CLASS_METER                 |        6 |      2 | Electric - W                                      | W       | decimal    |
|     10 | COMMAND_CLASS_METER                 |        6 |    256 | Exporting                                         |         | bool       |
|     10 | COMMAND_CLASS_METER                 |        6 |    257 | Reset                                             |         | button     |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        1 |      0 | ZWave+ Version                                    |         | byte       |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        1 |      1 | InstallerIcon                                     |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        1 |      2 | UserIcon                                          |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        2 |      0 | ZWave+ Version                                    |         | byte       |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        2 |      1 | InstallerIcon                                     |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        2 |      2 | UserIcon                                          |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        3 |      0 | ZWave+ Version                                    |         | byte       |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        3 |      1 | InstallerIcon                                     |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        3 |      2 | UserIcon                                          |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        4 |      0 | ZWave+ Version                                    |         | byte       |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        4 |      1 | InstallerIcon                                     |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        4 |      2 | UserIcon                                          |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        5 |      0 | ZWave+ Version                                    |         | byte       |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        5 |      1 | InstallerIcon                                     |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        5 |      2 | UserIcon                                          |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        6 |      0 | ZWave+ Version                                    |         | byte       |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        6 |      1 | InstallerIcon                                     |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        6 |      2 | UserIcon                                          |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        7 |      0 | ZWave+ Version                                    |         | byte       |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        7 |      1 | InstallerIcon                                     |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        7 |      2 | UserIcon                                          |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        8 |      0 | ZWave+ Version                                    |         | byte       |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        8 |      1 | InstallerIcon                                     |         | short      |
|     10 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        8 |      2 | UserIcon                                          |         | short      |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |      1 | On/Off Status Recovery After Power Failure        |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |      2 | Power Wattage Report Value Threshold              | watts   | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |      3 | Power Wattage Report Frequency                    | seconds | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |      4 | Energy (kWh) Report Frequency                     | seconds | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |      5 | Overload Protection                               | watts   | short      |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |      6 | Enable/Disable Auto Turn-Off Timer for CH1        |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |      7 | Set Auto Turn-Off Timer for CH1                   | minutes | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |      8 | Enable/Disable Auto Turn-On Timer for CH1         |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |      9 | Set Auto Turn-On Timer for CH1                    | minutes | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     10 | Enable/Disable Auto Turn-Off Timer for CH2        |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     11 | Set Auto Turn-Off Timer for CH2                   | minutes | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     12 | Enable/Disable Auto Turn-On Timer for CH2         |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     13 | Set Auto Turn-On Timer for CH2                    | minutes | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     14 | Enable/Disable Auto Turn-Off Timer for CH3        |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     15 | Set Auto Turn-Off Timer for CH3                   | minutes | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     16 | Enable/Disable Auto Turn-On Timer for CH3         |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     17 | Set Auto Turn-On Timer for CH3                    | minutes | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     18 | Enable/Disable Auto Turn-Off Timer for CH4        |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     19 | Set Auto Turn-Off Timer for CH4                   | minutes | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     20 | Enable/Disable Auto Turn-On Timer for CH4         |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     21 | Set Auto Turn-On Timer for CH4                    | minutes | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     22 | Enable/Disable Auto Turn-Off Timer for CH5        |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     23 | Set Auto Turn-Off Timer for CH5                   | minutes | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     24 | Enable/Disable Auto Turn-On Timer for CH5         |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     25 | Set Auto Turn-On Timer for CH5                    | minutes | int        |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     26 | Enable/Disable Manual Control                     |         | list       |
|     10 | COMMAND_CLASS_CONFIGURATION         |        1 |     27 | LED Indicator Mode                                |         | list       |
|     10 | COMMAND_CLASS_NOTIFICATION          |        1 |      8 | Power Management                                  |         | list       |
|     10 | COMMAND_CLASS_NOTIFICATION          |        1 |    256 | Previous Event Cleared                            |         | byte       |
|     10 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      0 | Loaded Config Revision                            |         | int        |
|     10 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      1 | Config File Revision                              |         | int        |
|     10 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      2 | Latest Available Config File Revision             |         | int        |
|     10 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      3 | Device ID                                         |         | string     |
|     10 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      4 | Serial Number                                     |         | string     |
|     10 | COMMAND_CLASS_POWERLEVEL            |        1 |      0 | Powerlevel                                        | dB      | list       |
|     10 | COMMAND_CLASS_POWERLEVEL            |        1 |      1 | Timeout                                           | seconds | byte       |
|     10 | COMMAND_CLASS_POWERLEVEL            |        1 |      2 | Set Powerlevel                                    |         | button     |
|     10 | COMMAND_CLASS_POWERLEVEL            |        1 |      3 | Test Node                                         |         | byte       |
|     10 | COMMAND_CLASS_POWERLEVEL            |        1 |      4 | Test Powerlevel                                   | dB      | list       |
|     10 | COMMAND_CLASS_POWERLEVEL            |        1 |      5 | Frame Count                                       |         | short      |
|     10 | COMMAND_CLASS_POWERLEVEL            |        1 |      6 | Test                                              |         | button     |
|     10 | COMMAND_CLASS_POWERLEVEL            |        1 |      7 | Report                                            |         | button     |
|     10 | COMMAND_CLASS_POWERLEVEL            |        1 |      8 | Test Status                                       |         | list       |
|     10 | COMMAND_CLASS_POWERLEVEL            |        1 |      9 | Acked Frames                                      |         | short      |
|     10 | COMMAND_CLASS_VERSION               |        1 |      0 | Library Version                                   |         | string     |
|     10 | COMMAND_CLASS_VERSION               |        1 |      1 | Protocol Version                                  |         | string     |
|     10 | COMMAND_CLASS_VERSION               |        1 |      2 | Application Version                               |         | string     |
|     12 | COMMAND_CLASS_SENSOR_BINARY         |        1 |      0 | Sensor                                            |         | bool       |
|     12 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |      1 | Air Temperature                                   | C       | decimal    |
|     12 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |      3 | Illuminance                                       | Lux     | decimal    |
|     12 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |     31 | Moisture                                          | %       | decimal    |
|     12 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |    256 | Air Temperature Units                             |         | list       |
|     12 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |    258 | Illuminance Units                                 |         | list       |
|     12 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |    286 | Moisture Units                                    |         | list       |
|     12 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        1 |      0 | ZWave+ Version                                    |         | byte       |
|     12 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        1 |      1 | InstallerIcon                                     |         | short      |
|     12 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        1 |      2 | UserIcon                                          |         | short      |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |      2 | Led alarm reporting                               |         | list       |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |      3 | Temperature and light reporting                   |         | list       |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |      4 | Temperature reporting                             |         | list       |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |      5 | Temperature reporting unit                        |         | list       |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |      6 | Temperature alarms                                |         | list       |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |      7 | High temperature level                            |         | byte       |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |      8 | Low temperature level                             |         | byte       |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |      9 | Ambient light reporting                           |         | list       |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |     10 | High Ambient light report level                   | lux     | int        |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |     11 | Low ambient light report level                    | lux     | int        |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |     12 | Leakage alarm                                     |         | list       |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |     13 | Leakage alarm level                               |         | byte       |
|     12 | COMMAND_CLASS_CONFIGURATION         |        1 |     14 | Moisture reporting period                         | Hours   | byte       |
|     12 | COMMAND_CLASS_NOTIFICATION          |        1 |      4 | Heat                                              |         | list       |
|     12 | COMMAND_CLASS_NOTIFICATION          |        1 |      5 | Water                                             |         | list       |
|     12 | COMMAND_CLASS_NOTIFICATION          |        1 |      7 | Home Security                                     |         | list       |
|     12 | COMMAND_CLASS_NOTIFICATION          |        1 |    256 | Previous Event Cleared                            |         | byte       |
|     12 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      0 | Loaded Config Revision                            |         | int        |
|     12 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      1 | Config File Revision                              |         | int        |
|     12 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      2 | Latest Available Config File Revision             |         | int        |
|     12 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      3 | Device ID                                         |         | string     |
|     12 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      4 | Serial Number                                     |         | string     |
|     12 | COMMAND_CLASS_POWERLEVEL            |        1 |      0 | Powerlevel                                        | dB      | list       |
|     12 | COMMAND_CLASS_POWERLEVEL            |        1 |      1 | Timeout                                           | seconds | byte       |
|     12 | COMMAND_CLASS_POWERLEVEL            |        1 |      2 | Set Powerlevel                                    |         | button     |
|     12 | COMMAND_CLASS_POWERLEVEL            |        1 |      3 | Test Node                                         |         | byte       |
|     12 | COMMAND_CLASS_POWERLEVEL            |        1 |      4 | Test Powerlevel                                   | dB      | list       |
|     12 | COMMAND_CLASS_POWERLEVEL            |        1 |      5 | Frame Count                                       |         | short      |
|     12 | COMMAND_CLASS_POWERLEVEL            |        1 |      6 | Test                                              |         | button     |
|     12 | COMMAND_CLASS_POWERLEVEL            |        1 |      7 | Report                                            |         | button     |
|     12 | COMMAND_CLASS_POWERLEVEL            |        1 |      8 | Test Status                                       |         | list       |
|     12 | COMMAND_CLASS_POWERLEVEL            |        1 |      9 | Acked Frames                                      |         | short      |
|     12 | COMMAND_CLASS_BATTERY               |        1 |      0 | Battery Level                                     | %       | byte       |
|     12 | COMMAND_CLASS_WAKE_UP               |        1 |      0 | Wake-up Interval                                  | Seconds | int        |
|     12 | COMMAND_CLASS_WAKE_UP               |        1 |      1 | Minimum Wake-up Interval                          | Seconds | int        |
|     12 | COMMAND_CLASS_WAKE_UP               |        1 |      2 | Maximum Wake-up Interval                          | Seconds | int        |
|     12 | COMMAND_CLASS_WAKE_UP               |        1 |      3 | Default Wake-up Interval                          | Seconds | int        |
|     12 | COMMAND_CLASS_WAKE_UP               |        1 |      4 | Wake-up Interval Step                             | Seconds | int        |
|     12 | COMMAND_CLASS_VERSION               |        1 |      0 | Library Version                                   |         | string     |
|     12 | COMMAND_CLASS_VERSION               |        1 |      1 | Protocol Version                                  |         | string     |
|     12 | COMMAND_CLASS_VERSION               |        1 |      2 | Application Version                               |         | string     |
|     14 | COMMAND_CLASS_SENSOR_BINARY         |        1 |      0 | Sensor                                            |         | bool       |
|     14 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |      1 | Air Temperature                                   | C       | decimal    |
|     14 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |      3 | Illuminance                                       | Lux     | decimal    |
|     14 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |      5 | Humidity                                          | %       | decimal    |
|     14 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |     27 | Ultraviolet                                       | UV      | decimal    |
|     14 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |    256 | Air Temperature Units                             |         | list       |
|     14 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |    258 | Illuminance Units                                 |         | list       |
|     14 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |    260 | Humidity Units                                    |         | list       |
|     14 | COMMAND_CLASS_SENSOR_MULTILEVEL     |        1 |    282 | Ultraviolet Units                                 |         | list       |
|     14 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        1 |      0 | ZWave+ Version                                    |         | byte       |
|     14 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        1 |      1 | InstallerIcon                                     |         | short      |
|     14 | COMMAND_CLASS_ZWAVEPLUS_INFO        |        1 |      2 | UserIcon                                          |         | short      |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |      2 | Wake up 10 minutes on Power On                    |         | list       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |      3 | On time                                           | seconds | short      |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |      4 | Enable Motion Sensor                              |         | list       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |      5 | Command Options                                   |         | list       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |      8 | Awake timeout                                     |         | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |      9 | Current power mode                                |         | list       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     39 | Low Battery                                       |         | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     40 | Report Only On Thresholds                         |         | list       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     41 | Temperature Reporting Threshold                   | C/F     | int        |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     42 | Humidity Reporting Threshold                      | %       | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     43 | Luminance Reporting Threshold                     | LUX     | short      |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     44 | Battery Reporting Threshold                       | %       | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     45 | UV Reporting Threshold                            |         | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     46 | Low Temp Alarm                                    |         | list       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     48 | Enable/disable to send a report on Threshold      |         | bitset     |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     49 | Set the upper limit value of temperature sensor   |         | int        |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     50 | Set the lower limit value of temperature sensor   |         | int        |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     51 | Set the upper limit value of humidity sensor      | %       | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     52 | Set the lower limit value of humidity sensor      | %       | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     53 | Set the upper limit value of Lighting sensor      | lux     | short      |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     54 | Set the lower limit value of Lighting sensor      | lux     | short      |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     55 | Set the upper limit value of ultraviolet sensor   | UV      | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     56 | Set the lower limit value of ultraviolet sensor   | UV      | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     57 | Set the recover limit value of temperature sensor |         | short      |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     58 | Set the recover limit value of humidity sensor    | %       | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     59 | Set the recover limit value of Lighting sensor    | 10xlux  | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     60 | Set the recover limit value of Ultraviolet sensor | UV      | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     61 | Get the out-of-limit state of the Sensors         |         | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     64 | Temperature scale                                 |         | list       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |     81 | LED blinking report                               |         | list       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    100 | Reset Params 101-103 to Default                   |         | button     |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    101 | Group 1 Reports                                   |         | bitset     |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    102 | Group 2 Reports                                   |         | int        |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    103 | Group 3 Reports                                   |         | int        |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    110 | Reset Params 111-113 to Default                   |         | button     |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    111 | Group 1 Interval                                  | seconds | int        |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    112 | Group 2 Interval                                  | seconds | int        |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    113 | Group 3 Interval                                  | seconds | int        |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    201 | Temperature Calibration                           |         | short      |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    202 | Humidity Calibration                              |         | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    203 | Luminance Calibration                             |         | short      |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    204 | Ultraviolet Calibration                           |         | byte       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    252 | Enable/disable Lock Configuration                 |         | list       |
|     14 | COMMAND_CLASS_CONFIGURATION         |        1 |    255 | Reset To Factory Defaults                         |         | list       |
|     14 | COMMAND_CLASS_NOTIFICATION          |        1 |      7 | Home Security                                     |         | list       |
|     14 | COMMAND_CLASS_NOTIFICATION          |        1 |    256 | Previous Event Cleared                            |         | byte       |
|     14 | COMMAND_CLASS_NOTIFICATION          |        1 |    514 | Automatically Clear Events                        | ms      | int        |
|     14 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      0 | Loaded Config Revision                            |         | int        |
|     14 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      1 | Config File Revision                              |         | int        |
|     14 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      2 | Latest Available Config File Revision             |         | int        |
|     14 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      3 | Device ID                                         |         | string     |
|     14 | COMMAND_CLASS_MANUFACTURER_SPECIFIC |        1 |      4 | Serial Number                                     |         | string     |
|     14 | COMMAND_CLASS_POWERLEVEL            |        1 |      0 | Powerlevel                                        | dB      | list       |
|     14 | COMMAND_CLASS_POWERLEVEL            |        1 |      1 | Timeout                                           | seconds | byte       |
|     14 | COMMAND_CLASS_POWERLEVEL            |        1 |      2 | Set Powerlevel                                    |         | button     |
|     14 | COMMAND_CLASS_POWERLEVEL            |        1 |      3 | Test Node                                         |         | byte       |
|     14 | COMMAND_CLASS_POWERLEVEL            |        1 |      4 | Test Powerlevel                                   | dB      | list       |
|     14 | COMMAND_CLASS_POWERLEVEL            |        1 |      5 | Frame Count                                       |         | short      |
|     14 | COMMAND_CLASS_POWERLEVEL            |        1 |      6 | Test                                              |         | button     |
|     14 | COMMAND_CLASS_POWERLEVEL            |        1 |      7 | Report                                            |         | button     |
|     14 | COMMAND_CLASS_POWERLEVEL            |        1 |      8 | Test Status                                       |         | list       |
|     14 | COMMAND_CLASS_POWERLEVEL            |        1 |      9 | Acked Frames                                      |         | short      |
|     14 | COMMAND_CLASS_BATTERY               |        1 |      0 | Battery Level                                     | %       | byte       |
|     14 | COMMAND_CLASS_WAKE_UP               |        1 |      0 | Wake-up Interval                                  | Seconds | int        |
|     14 | COMMAND_CLASS_WAKE_UP               |        1 |      1 | Minimum Wake-up Interval                          | Seconds | int        |
|     14 | COMMAND_CLASS_WAKE_UP               |        1 |      2 | Maximum Wake-up Interval                          | Seconds | int        |
|     14 | COMMAND_CLASS_WAKE_UP               |        1 |      3 | Default Wake-up Interval                          | Seconds | int        |
|     14 | COMMAND_CLASS_WAKE_UP               |        1 |      4 | Wake-up Interval Step                             | Seconds | int        |
|     14 | COMMAND_CLASS_VERSION               |        1 |      0 | Library Version                                   |         | string     |
|     14 | COMMAND_CLASS_VERSION               |        1 |      1 | Protocol Version                                  |         | string     |
|     14 | COMMAND_CLASS_VERSION               |        1 |      2 | Application Version                               |         | string     |

Note that the columns of the _zwave_events_ table are named according to the [node-red-contrib-openzwave][] JSON payload property keys to make it easier to correlate its contents with [Node-RED][] logs. Note also that the records include a time stamp making them compatible with time-series based analysis tools.

[mqtt]: http://mqtt.org/
[node-red]: https://nodered.org
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-node-geofence]: https://flows.nodered.org/node/node-red-node-geofence
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-node-ui-table]: https://flows.nodered.org/node/node-red-node-ui-table
[node-red-node-mysql]: https://flows.nodered.org/node/node-red-node-mysql
[owntracks]: https://owntracks.org
[hue]: https://www2.meethue.com/
[pi]: https://www.raspberrypi.org/
[mosquitto]: https://mosquitto.org/
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/
[zigbee]: https://zigbeealliance.org/
[javascript]: https://en.wikipedia.org/wiki/JavaScript
[openzwave]: http://openzwave.com/
[Z-Wave]: https://z-wavealliance.org
[z-stick]: https://aeotec.com/z-wave-usb-stick/
[hubitat]: https://hubitat.com/
[homie]: https://homieiot.github.io/
[Home Assistant]: https://www.home-assistant.io/
[OpenHAB]: https://www.openhab.org/
[Aeotec MultiSensor 6]: https://aeotec.com/z-wave-sensor/
[Sensative Strips Comfort]: https://sensative.com/sensors/strips-zwave/comfort/
[JASCO Outdoor Smart Switch]: https://byjasco.com/ge-z-wave-plus-plug-outdoor-smart-switch-500s
[Zooz S2 Power Strip]: https://www.getzooz.com/zooz-zen20-power-strip.html
[Rhasspy]: https://rhasspy.readthedocs.io/en/latest/
[mic-array]: https://www.seeedstudio.com/ReSpeaker-Mic-Array-v2-0.html
[mic-hat]: https://www.seeedstudio.com/ReSpeaker-2-Mics-Pi-HAT.html
[touchscreen]: https://www.raspberrypi.org/products/raspberry-pi-touch-display/
[MySQL]: https://www.mysql.com/
[MariaDB]: https://mariadb.org/
