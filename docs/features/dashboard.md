# Remotely-Accessible, Mobile-Friendly Dashboard

A key feature of any whole-home automation system is the ability view and control "smart" home gear from anywhere, using any device. Ideally without having to juggle multiple manufacturers' sites and apps. This is achieved in my case using [Node-RED][] and its tightly integrate [node-red-dashboard][] component.

The [node-red-dashboard][] package supports a variety of standard web-based GUI control types: switches, sliders, drop-down lists etc. It also includes some automation-specific widgets like a color-picker that supports the kinds of features common among "smart" lighting products. It supports custom extensions such as [node-red-node-ui-table][].

Since [node-red-dashboard][] leverages the HTTP end-point security features built into [Node-RED][], it is simply a matter of specifying appropriate options in _settings.js_ to enable password protection and encryption as per <https://nodered.org/docs/user-guide/runtime/securing-node-red>. In particular, you will want to follow the instructions to require a user name, password and HTTPS access in order to view either the [Node-RED][] editor page or the dashboard.

```javascript
adminAuth: {
    type: "credentials",
    users: [{
        username: "...admin user name...",
        password: "...password encrypted using node-red-admin hash-pw...",
        permissions: "*"
    }]
},

httpNodeAuth: {user:"...ordinary user name...",pass:"...password encrypted using node-red-admin hash-pw..."},

https: {
    key: fs.readFileSync('...path to private key file...'),
    cert: fs.readFileSync('...path to certificate file...')
},

// The following property can be used to cause insecure HTTP connections to
// be redirected to HTTPS.
requireHttps: true,

```

In order to use encryption (HTTPS, MQTT with TLS etc.) you will need certificates. You could use self-signed certifates using, for example, the OpenSSH tool suite. But to avoid nasty-looking security warnings from web browsers I use free certificates issued by [Let's Encrypt][certbot] and managed by their `certbot` tool.

Similarly, unless you have an Internet-facing static IP address for your [Node-RED][] server (unlikely at home!) you will need to use some sort of reverse proxy or dynamic DNS service to access your editor and dashboard pages from outside the home. I use [Duck DNS][duckdns] for this purpose.

See <https://kirkrader.gitlab.io/docs/rpi-config/> for nitty-gritty details on configuring a [Raspberry Pi][pi] with everything required for these flows.

## Dashboard Design

The [node-red-dashboard][] package organizes the user interface into control _groups_ where each such group exists in a _tab_. (Note that the term _tab_ is retained in the documentation and the editor GUI for backwards compatibility with earlier version. Dashboard "tabs" do not appear visually like tabs but, rather, as separate web pages that are navigated using a drop-down menu.) My dashboard has six such "tabs," described below.

The [node-red-dashboard][] package renders a responsive UI that adapts automatically (within obvious limits) to the size and aspect ratio of the screen on which it is being viewed. Here are screenshots of the same page when viewed on a smartphone and a 7" touchscreen display:

| Phone                     | 7" Touchscreen                        |
|---------------------------|---------------------------------------|
| ![](groups-dashboard.jpg) | ![](groups-dashboard-touchscreen.png) |

Note that the touchscreen in question is attached to a [Pi][] that is configured to open [Node-RED][] dashboard directly in "kiosk" mode, as described in the section regarding [dedicated dashboard clients](../design/dedicated-dashboard-clients.md).

### Groups

Display a toggle switch control for each [Philips Hue][hue] group (room or zone). The GUI controls turn off the corresponding group or set it to its "last on state" as per the [Hue][] API.

![](groups-dashboard.jpg)

### Scenes

Display a drop-down list control for each [Hue][] group. Selecting a list item activates the correspond scene in that room or zone.

![](scenes-dashboard.jpg)

### Outlets

Display a toggle switch control for a number of [Z-Wave][] power outlets. The "Holiday Light" and "Laser Up Light" are two [JASCO Outdoor Smart Switch][] outlets (unfortunately discontinued by the manufacturer but still available from vendors with left-over stock at a higher price under the GE brand). The other five GUI switches correspond to the five outlets on a [Zooz S2 Power Strip][] power strip. I have a fan on a shelf plugged into outlet 1. The Zooz outlets 2 - 5 are not currently assigned to any specific purpose.

![](outlets-dashboard.jpg)

### Environment

Display the latest reported values from a [Hue][] motion sensor in the kitchen and a [Sensative Strips Comfort][] sensor on the balcony. The kitchen motion sensing can be toggled on or off.

![](environment-dashboard.jpg)

### Timer

Display a GUI for the [node-red-contrib-suncalc][] node that controls [lighting automation](lighting-automation.md).

### Presence

Display the current values for the global states that control [presence detection and home / away mode automation](presence.md). Provide buttons to request the current location or waypoints configuration from [Owntracks][].

![](presence-dashboard.jpg)

[hue]: https://www2.meethue.com/
[Z-Wave]: https://z-wavealliance.org
[Zooz S2 Power Strip]: https://www.getzooz.com/zooz-zen20-power-strip.html
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/

[JASCO Outdoor Smart Switch]: https://byjasco.com/ge-z-wave-plus-plug-outdoor-smart-switch-500s
[Sensative Strips Comfort]: https://sensative.com/sensors/strips-zwave/comfort/
[node-red]: https://nodered.org
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-contrib-suncalc]: https://flows.nodered.org/node/node-red-contrib-suncalc
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-node-geofence]: https://flows.nodered.org/node/node-red-node-geofence
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-node-ui-table]: https://flows.nodered.org/node/node-red-node-ui-table
[owntracks]: https://owntracks.org
[hue]: https://www2.meethue.com/
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/
[pi]: https://www.raspberrypi.org/
