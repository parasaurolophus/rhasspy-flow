# Lighting automation

My lighting automation takes into account the following three environmental conditions:

- [Time of day](#time-of-day) (day, evening or night)
- Day of year [theme](#theme)
- [Presence](#presence) (home / away mode)

Part of the point of using a home automation hub is that a single set of [Node-RED][] flows can provide integrated control and automation for devices from multiple manufacturers, using diverse communication protocols and API's. The majority of my lighting devices are [Philips Hue][hue], controlled using [node-red-contrib-huemagic][]. I also have a couple of [JASCO Outdoor Smart Switch][] devices, controlled using [node-red-contrib-openzwave][] through an [Aeotec Z-Stick Gen5][z-stick], that power special decorative and holiday lighting.

## Zones

For the purposes of lighting automation, I group the lights into multiple zones, making use of the native [Hue][] app's grouping features but also including non-[Hue][] devices like the [JASCO][JASCO Outdoor Smart Switch] switches.

### Interior

The "interior" zone are all the indoor lights in public spaces. I.e. the living room, hallway etc. but not private spaces like bedrooms and bathrooms.

### Exterior

The "exterior" zone includes devices like porch lights as well as special holiday lighting that only turns on automatically according to particular day-of-year [themes](#theme).

## Time of Day

These flows use [node-red-contrib-suncalc][] to divide each twenty-four hour period into three segments:

| Time of Day | Rule                       | Description                                                                 |
|-------------|----------------------------|-----------------------------------------------------------------------------|
| `day`       | Sunrise to sunset          | All lights off at sunrise                                                   |
| `evening`   | Sunset to 10:00pm (22:00)  | Activate appropriate [theme](#theme) for the lighting [zones](#zones)       |
| `night`     | 10:00PM (22:00) to sunrise | [Exterior](#exterior) zone off, [interior](#interior) zone to "night light" |

The output of the timer events is parsed using [node-red-contrib-moment][] and the `global.time-of-day` context variable updated.

![](suncalc-flow.png)

Time-of-day automation is triggered each time the value of `global.time-of-day` changes while the flows are running.

<a id="lighting-automation-flow"></a>

![](lighting-automation-flow.png)

The automation that is triggered when `global.time-of-day` changes takes the current [theme](#theme) into account. Sub-flows are used for modularity, i.e. to keep the flows more readable and maintainable. Here, as an example, is the _Interior Evening_ sub-flow:

![](interior-evening-subflow.png)

Note the use of a `catch` node in the sub-flow. It is configured to trap any uncaught exception thrown by any node in the given sub-flow. This makes sure that the overall flow continues to operate even if there is a problem controlling the [Hue][] lights. See [Error Handling](../tutorial/error-handling.md) for more guidance on error handling generally.

## Theme

| Theme      | Dates               | Description         |
|------------|---------------------|---------------------|
| `tribal`   | July 1-4            | US Independence Day |
| `spooky`   | Any day in October  | Halloween           |
| `jolly`    | Any day in December | Winter Solstice     |
| `standard` | Any other day       | Normal lighting     |

This is accomplished by periodically calling the Linux `date` command using [Node-RED][] `inject` and `exec` nodes. The `inject` nodes are configured to trigger the `exec` node once when the flows start and also every day at midnight while the flows are running. The output of the `date` command is parsed and used to set the `global.theme` context variable.

![](exec-date-flow.png)

## Presence

The lighting automation flow [shown above](#lighting-automation-flow) also has an entry point for handling _home / away_ state transitions. This is driven by the [presence detection](presence.md) logic described separately. Once it is determined that the last person has left home, i.e. the system transitions to _away_ mode, all the interior lights are set to their default state based on current time of day and theme. If it is daytime when the last person leaves, the lights are turned off. If it is after 10PM the interior lights are set to "nightlight" mode. Between sunset and 10PM, the lights return to whatever settings are appropriate based on the time of year. Continuing normal automation behavior while everyone is away, i.e. presence simulation, is a common security measure. Conversely, the interior lights are unconditionally set to their `evening` state when transitioning from _away_ to any other mode. (This ensures that the public spaces will be lit even if arriving home at night or during a gloomy day.)

[mqtt]: http://mqtt.org/
[node-red]: https://nodered.org
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-contrib-suncalc]: https://flows.nodered.org/node/node-red-contrib-suncalc
[node-red-contrib-moment]: https://flows.nodered.org/node/node-red-contrib-moment
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-node-geofence]: https://flows.nodered.org/node/node-red-node-geofence
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-node-ui-table]: https://flows.nodered.org/node/node-red-node-ui-table
[owntracks]: https://owntracks.org
[hue]: https://www2.meethue.com/
[pi]: https://www.raspberrypi.org/
[mosquitto]: https://mosquitto.org/
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/
[zigbee]: https://zigbeealliance.org/
[javascript]: https://en.wikipedia.org/wiki/JavaScript
[openzwave]: http://openzwave.com/
[Z-Wave]: https://z-wavealliance.org
[z-stick]: https://aeotec.com/z-wave-usb-stick/
[hubitat]: https://hubitat.com/
[homie]: https://homieiot.github.io/
[Home Assistant]: https://www.home-assistant.io/
[OpenHAB]: https://www.openhab.org/
[Aeotec MultiSensor 6]: https://aeotec.com/z-wave-sensor/
[Sensative Strips Comfort]: https://sensative.com/sensors/strips-zwave/comfort/
[JASCO Outdoor Smart Switch]: https://byjasco.com/ge-z-wave-plus-plug-outdoor-smart-switch-500s
[Zooz S2 Power Strip]: https://www.getzooz.com/zooz-zen20-power-strip.html
[Rhasspy]: https://rhasspy.readthedocs.io/en/latest/
[mic-array]: https://www.seeedstudio.com/ReSpeaker-Mic-Array-v2-0.html
[mic-hat]: https://www.seeedstudio.com/ReSpeaker-2-Mics-Pi-HAT.html
[touchscreen]: https://www.raspberrypi.org/products/raspberry-pi-touch-display/
