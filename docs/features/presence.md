# Presence

As discussed in the section on [lighting automation](lighting-automation.md), these [Node-RED][] flows define lighting scenes in terms of "themes" that are activated by time of day and day of year. These same themes are activated due to processing MQTT messages received from the [Owntracks][] app on mobile devices, as well as by timer events.

## Home and Away States

The simplest way to understand the feature is as a very basic [state machine](../tutorial/uml.md#state-diagrams) with just two states: home and away. When the last person departs while in the home state, the system activates the theme for the current time of day and day of year and transitions to the away state. When anyone arrives while in the away state, the system activates the evening scene for current day of the year and then transitions to the home state.

_Rationale:_ When no-one is home, standard automation rules will simulate normal behavior as if people were home, as a security measure. When someone arrives, the [interior zone](lighting-automation.md#interior) lights are turned on to their way, no matter how dark or gloomy it may be.

```mermaid
stateDiagram-v2

  home
  away

  home --> away: last person<br/>leaves /<br/>activate<br/>current<br/>time-of-day<br/>theme
  away --> home: first person<br/>arrives /<br/>activate<br/>evening<br/>theme for<br/>current day<br/>year
```

## Presence Detection Flow Chart

The state machine described above is implemented by use of global context variables, `global.presence` and `global.mode`. The value of `global.presence` is a JavaScript object whose keys are the topics of incoming `location` messages from the [Owntracks][] app on each occupant's mobile device. The values are `true` or `false` depending on whether or not the most recently received message indicated that sender was in a region named "Home." After updating `global.presence`, the handler for `location` messages checks all the keys. If none are `true` and `global.mode` is the string `"home"` then it initiates a transition to the `"away"` state. If at least one value in `global.presence` is `true` and `global.mode` is the string `"away"` then it initiates a transition to the `"home"` state.

![](owntracks-location-handler-flow.png)

The core logic is in the `function` node labeled _presence_:

```javascript
let presence = global.get("presence") || {}
let modeMsg = null
let presenceMsg = null

if (presence[msg.topic] !== msg.payload) {
    presence[msg.topic] = msg.payload
    global.set("presence", presence)
}

presenceMsg = {
    payload: presence
}

let newMode = "away"

for (let key in presence) {
    if (presence[key]) {
        newMode = "home"
        break
    }
}

let currentMode = global.get("mode") || "unknown"

if (currentMode !== newMode) {
    global.set("mode", newMode)
}

modeMsg = {
    payload: newMode,
}

return [modeMsg, presenceMsg]
```

As a [flow chart](../tutorial/uml.md#activity-diagrams):

```mermaid
graph TD

  start(( ))
  inhome{"in<br/>region<br/>'Home'?"}
  payloadTrue["set<br/>msg.payload<br/>to true"]
  payloadFalse["set<br/>msg.payload<br/>to false"]

  subgraph "presence"
    updatePresence["set<br/>global.presence[msg.topic]<br/>to msg.payoad"]
    anyPresent{"any value in<br/>global.presence<br/>is true?"}
    homeMode["set<br/>global.mode<br/>to 'home'"]
    awayMode["set<br/>global.mode<br/>to 'away'"]
  end

  modeChanged{"global.mode<br/>changed?"}
  home{"global.mode is 'home'?"}
  activateEvening["activate<br/>'Interior<br/>Evening'<br/>theme"]
  activateTheme["activate<br/>global.time-of-day<br/>theme"]
  stop(( ))

  start -- "location<br/>message" --> inhome
  inhome -- yes --> payloadTrue
  inhome -- no --> payloadFalse
  payloadTrue --> updatePresence
  payloadFalse --> updatePresence
  updatePresence --> anyPresent
  anyPresent -- yes --> homeMode
  anyPresent -- no --> awayMode
  homeMode --> modeChanged
  awayMode --> modeChanged
  modeChanged -- no --> stop
  modeChanged -- yes --> home
  home -- yes --> activateEvening
  home -- no --> activateTheme
  activateEvening --> stop
  activateTheme --> stop
```

A subtle but important aspect to the design of the preceding flow is that there is no need to alter the [Node-RED][] flows to add or remove occupants nor use anything like [node-red-node-geofence][] which would introduced yet another "cloud" dependency (in this case, on the _OpenStreetMap_ service). The code in the _presence_ function will add entries to `global.presence` as they start appearing as topic in MQTT messages from [Owntracks][]. You do need configure all occupants' mobile devices with compatible [Owntracks][] configurations to the extent that they must all define the same area designated as "Home". It would be easy to extend this logic to allow for automation driven by additional geofence regions defined by [Owntracks][] on particular occupants' devices. For example, "Send a notification when person X leaves or arrives home," "Adjust a thermostat when person Y leaves her office" and so on.

[mqtt]: http://mqtt.org/
[node-red]: https://nodered.org
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-node-geofence]: https://flows.nodered.org/node/node-red-node-geofence
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-node-ui-table]: https://flows.nodered.org/node/node-red-node-ui-table
[owntracks]: https://owntracks.org
[hue]: https://www2.meethue.com/
[pi]: https://www.raspberrypi.org/
[mosquitto]: https://mosquitto.org/
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/
[zigbee]: https://zigbeealliance.org/
[javascript]: https://en.wikipedia.org/wiki/JavaScript
[openzwave]: http://openzwave.com/
[Z-Wave]: https://z-wavealliance.org
[z-stick]: https://aeotec.com/z-wave-usb-stick/
[hubitat]: https://hubitat.com/
[homie]: https://homieiot.github.io/
[Home Assistant]: https://www.home-assistant.io/
[OpenHAB]: https://www.openhab.org/
[Aeotec MultiSensor 6]: https://aeotec.com/z-wave-sensor/
[Sensative Strips Comfort]: https://sensative.com/sensors/strips-zwave/comfort/
[JASCO Outdoor Smart Switch]: https://byjasco.com/ge-z-wave-plus-plug-outdoor-smart-switch-500s
[Zooz S2 Power Strip]: https://www.getzooz.com/zooz-zen20-power-strip.html
[Rhasspy]: https://rhasspy.readthedocs.io/en/latest/
[mic-array]: https://www.seeedstudio.com/ReSpeaker-Mic-Array-v2-0.html
[mic-hat]: https://www.seeedstudio.com/ReSpeaker-2-Mics-Pi-HAT.html
[touchscreen]: https://www.raspberrypi.org/products/raspberry-pi-touch-display/
