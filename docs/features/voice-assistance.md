# Cloud-Free Voice Assistance Using Rhasspy

Big-name voice assistant's like _Amazon Alexa_, _Google Assistant_, _Apple Siri_ and the like are all designed to perform the bulk of their speech processing "in the cloud." This means recordings of things said in your home are regularly transmitted over the public Internet to corporate-owned servers. This raises all of the usual performance, reliability, latency, security and privacy concerns as with any "cloud" based service but to an even greater degree. Even if you trust companies like Amazon and Google to keep their promises regarding not recording, transmitting and analyzing your speech without being triggered by the the _wake phrase_ for their device ("alexa", "hey google" etc.), anyone who has lived with such a device knows that they are notoriously inaccurate in detecting those triggering words. This means that they often send recordings of things said in people's homes that their occupants did not intend as voice commands.

[Rhasspy][] is an open-source project whose aim is to provide features comparable to those of brand-name voice assistants but in a way that most or all of the speech processing is done locally inside the home. It can be run on any popular home computer platform (Windows, Mac, various flavors of Linux) as well as supporting home automation friendly platforms like Raspberry Pi OS. A complete tutorial on installing, configuring and using [Rhasspy][] is beyond the scope of these pages documenting my [Node-RED][] flows, but some guidance on choosing and configuring a [Raspberry Pi][pi] for use with [Rhasspy][] is provided at [Dedicated Dashboard Clients](../design/dedicated-dashboard-clients.md). Suffice it to say here that in addition to launching the [Node-RED][] dashboard automatically in "kiosk" mode at startup, each dashboard client also runs an instance of [Rhasspy][] to act as a voice assistant.

There two main aspects to how [Rhasspy][] interfaces to these flows:

1. The flows automatically capture metadata describing the [Philips Hue][hue] lights, groups and scenes paired with the _Hue Bridge_ and write it to files in the "slots" format defined by [Rhasspy][]

2. [Rhasspy][] sends MQTT messages containing the "intents" generated by voice command processing

The intent processing is defined by _~/.config/rhasspy/profile/en/sentences.ini_ for the user as which the [Rhappy][] service is running.

```
[AllLightsOff]
turn off all [of] [the] lights
all [of] [the] lights off
switch all [of] [the] lights off

[ActivateScene]
scene = $scenes {scene}
activate <scene>
turn on <scene>
switch <scene> on

[ChangeGroupState]
group = $groups {group}
state = (on:true | off:false) {on!bool}
turn [the] <group> [lights] <state>
turn <state> [the] <group> [lights]
switch [the] <group> [lights] <state>

[ChangeGroupColor]
group = $groups {group}
color = $colors {color}
turn [the] <group> [to] <color>
set [the] <group> [to] <color>
make [the] <group> <color>

[ChangeGroupBrightness]
group = $groups {group}
brightness = (0..100) {brightness}
set [the] <group> to <brightness> [percent]

[ChangeLightState]
light = $lights {light}
state = (on:true | off:false) {on!bool}
turn [the] <light> <state>
turn <state> [the] <light>
switch [the] <light> <state>

[ChangeLightColor]
light = $lights {light}
color = $colors {color}
turn [the] <light> [to] <color>
set [the] <light> [to] <color>
make [the] <light> <color>

[ChangeLightBrightness]
light = $lights {light}
brightness = (0..100) {brightness}
set [the] <light> to <brightness> [percent]

[ChangeOutletState]
outlet = $zwave-outlets {outlet}
state = (on:true | off:false) {on!bool}
turn [the] <outlet> <state>
turn <state> [the] <outlet>
switch [the] <outlet> <state>

[ChangeRoomState]
state = (on:true | off:false) {on!bool}
turn <state> the lights
turn the lights <state>
```

The contents of the slots files referenced as _\$lights_, _\$groups_ and _\$scenes_ in _sentences.ini_ are generated automatically by fetching configuration data from the _Hue Bridge_ using features of [node-red-contrib-huemagic][]:

![](hue-metadata-flow.png)

(Doing the same for Z-Wave devices is trickier and not yet implemented. The most promising approach would be to parse the contents of _~/.node-red/ozwcache_0x-----.xml_, extracting the required "value id" fields for each slot option.)

Handling the intent messages sent as a result of the preceding _sentences.ini_ file is performed by the following flow:

![](rhasspy-intent-handler-flow.png)

The preceding flow supports the following features:

1. The raw utterance for each intent is displayed as a notification on the dashboard to provide feedback to the user and as an aid in debugging

2. The intent type and slot values are extracted from each MQTT message to drive the corresponding device control operations such as turning on or off switches, lights and groups; activating lighting scenes

## Remapping Intent Topics

Note that the MQTT topic to which this flow listens uses a `+` wild-card as both the first and last component. The last component denotes the type of intent as sent by the [Rhasspy][] command processing pipeline. These are defined by the _ini_-file style headers in _sentences.ini_, e.g. `ChangeGroupState`, `ChangeLightColor` and so on. The first component denotes the room in which the particular dashboard client / voice assistant resides. To keep configuring [Rhasspy][] in each room as simple and consistent as possible as well as to reduce unnecessary MQTT traffic across the Wi-Fi network, each [Rhasspy][] instance actually sends MQTT messages with the same set of topics to an internal broker. All intent messages sent by every [Rhasspy][] instance are grouped under the `hermes/#` topic tree. The primary broker used by [Node-RED][] bridges and remaps only the relevant `hermes/intent/+` topics from each [Rhasppy][] instance's broker. For example, here is a snippet from `/etc/mosquitto/mosquitto.conf` for the public-facing MQTT broker used directly by [Node-RED][]:

```
connection mycroft-lr
cleansession true
address mycroft-lr.local:1883
topic intent/+ in 2 living-room/ hermes/
```

The preceding block of configuration data tells the "primary" broker to subscribe to any topic of the form `hermes/intent/+` sent by the separate broker at address `mycroft-lr.local:1883` and remap it to the corresponding `living-room/intent/+` topic. There is a similar block of configuration for each room in which there is a dashboard client / voice assistant running. The result is that I can copy the same [Rhasspy][] profile files to each client and the primary broker will remap each client's messages such that [Node-RED][] can tell which room sent each intent. This feature is used in the last branch of the preceding flow, allowing the `ChangeRoomState` intent to turn on or off "the lights" without specifying a particular group to operate on the correct room. See the section on "Configuring Bridges" at <https://mosquitto.org/man/mosquitto-conf-5.html> for more information on bridging and remapping topics in [Mosquitto][]. In particular, note that this form of topic bridging requires no configuration on the individual [Rhasspy][] brokers. To them, the "primary" broker is just another client subscribed to a particular set of topics using the `hermes/intent/+` wild-card pattern. To clients of the "primary" broker like [Node-RED][], the fact that there are multiple [Rhasspy][] brokers is invisible. To them it simply appears that there are multiple distinct topic matching the `+/intent/+` pattern where the prefix corresponds to a particular [Rhasspy][] instance. To keep things readable and maintainable, I use the same string as the prefix in these remapped topics as is used as the particular "siteId" value in each [Rhasspy][] profile.

![](rhasspy-settings.png)

[mqtt]: http://mqtt.org/
[node-red]: https://nodered.org
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-node-geofence]: https://flows.nodered.org/node/node-red-node-geofence
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-node-ui-table]: https://flows.nodered.org/node/node-red-node-ui-table
[owntracks]: https://owntracks.org
[hue]: https://www2.meethue.com/
[pi]: https://www.raspberrypi.org/
[mosquitto]: https://mosquitto.org/
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/
[zigbee]: https://zigbeealliance.org/
[javascript]: https://en.wikipedia.org/wiki/JavaScript
[openzwave]: http://openzwave.com/
[Z-Wave]: https://z-wavealliance.org
[z-stick]: https://aeotec.com/z-wave-usb-stick/
[hubitat]: https://hubitat.com/
[homie]: https://homieiot.github.io/
[Home Assistant]: https://www.home-assistant.io/
[OpenHAB]: https://www.openhab.org/
[Aeotec MultiSensor 6]: https://aeotec.com/z-wave-sensor/
[Sensative Strips Comfort]: https://sensative.com/sensors/strips-zwave/comfort/
[JASCO Outdoor Smart Switch]: https://byjasco.com/ge-z-wave-plus-plug-outdoor-smart-switch-500s
[Zooz S2 Power Strip]: https://www.getzooz.com/zooz-zen20-power-strip.html
[Rhasspy]: https://rhasspy.readthedocs.io/en/latest/
[mic-array]: https://www.seeedstudio.com/ReSpeaker-Mic-Array-v2-0.html
[mic-hat]: https://www.seeedstudio.com/ReSpeaker-2-Mics-Pi-HAT.html
[touchscreen]: https://www.raspberrypi.org/products/raspberry-pi-touch-display/
[speaker]: https://www.logitech.com/en-us/product/multimedia-speaker-z50
