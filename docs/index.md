# (Nearly) Cloud-Free Home Automation

Documentation for <https://gitlab.com/parasaurolophus/cheznous>

> This documentation assumes that you understand home automation terminology generally and [Node-RED][] concepts in particular such as _flows_, _projects_, _context stores_, _nodes_ (in the senses used both by [Node-RED][] and [Z-Wave][]), _value id_ (consisting of a [Z-Wave][] _node id_, _command class_, _instance_ and _command index_) etc. Ditto for [Philips Hue][hue] concepts such as _group_ (both _room_ and _zone_), _scene_ etc.
>
> See:
>
> - <https://nodered.org/docs/getting-started/>
> - <https://nodered.org/docs/tutorials/>
> - <http://openzwave.com/dev/>
> - <https://www.philips-hue.com/en-us/support/apps-and-software>

## Overview

```mermaid
graph TD

  location[Mobile OS<br/>location<br/>services]
  certbot[Let's Encrypt<br/>service]
  duckdns[Duck DNS<br/>service]

  subgraph Mobile Device
    owntracks[Owntracks<br/>app]
    browser["Web browser<br/>(dashboard)"]
  end

  subgraph Home Network
    nodered[Node-RED<br/>server]
    broker["Mosquitto<br/>(MQTT broker)"]
    database[("MariaDB<br/>(database)")]
    client["Rhasspy<br/>voice assistant /<br/>dashboard client<br/>(one per room)"]
    bridge[Hue Bridge]
    hue[Multiple<br/>Hue devices]
    zwave[Multiple<br/>Z-Wave devices]
    router["Router<br/>(port<br/>forwarding)"]
  end

  certbot -- CA service --- nodered
  nodered -- MQTT --- broker
  location --- owntracks
  owntracks -- MQTT / TLS --- broker
  browser -- HTTPS --- nodered
  nodered --- database
  nodered -- HTTP --- client
  broker -- MQTT --- client
  nodered --- bridge
  bridge -- Zigbee --- hue
  nodered -- Z-Wave --- zwave
  router -- Dynamic DNS service --- duckdns

  classDef cloud fill:pink,color:black
  class location,certbot,duckdns cloud
```

The scenarios supported by these flows include:

- [Presence detection](features/presence.md) ("home / away mode") that takes account of all household members, not just a single person, using [Owntracks][] in as close to a "cloud free"[^cloud] fashion as practical

- Time-of-day and day-of-year based [lighting automation](features/lighting-automation.md) (e.g. holiday lighting themes after sunset, night-light mode at bedtime) using [node-red-contrib-suncalc][] and an `exec` node that periodically invokes the Linux `date` command

- Automatically turn off particular [Z-Wave][] and [Hue][] outlets after they have been on for a certain amount of time or in coordination with particular overall lighting themes

- [Cloud-free voice assistance](features/voice-assistance.md) using locally hosted [Rhasspy][] services in each room

- Present a single, unified [dashboard](features/dashboard.md) for displaying and controlling the state of this heterogeneous home automation network in mobile-friendly, remotely-accessible, secure web based interface

Note that these scenarios highlight the difference between true _automation_ and simple _control_ of so-called "smart" devices. Most manufacturers' native apps, including the one for [Philips Hue][hue], provide more than adequate remote control of their respective families of devices. Most fail to provide much in the way of automation that would actually justify their marketing departments' claims about being "smart." That is where platforms like [Node-RED][] come in. In short, these flows use [Node-RED][] based automation to compensate for lack of intelligence in the off-the-shelf functionality of any particular family of supposedly "smart" devices. In addition, they provide a single, web based dashboard for controlling devices from a variety of manufacturers that use different communication protocols and API's.

[mqtt]: http://mqtt.org/
[node-red]: https://nodered.org
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-contrib-suncalc]: https://flows.nodered.org/node/node-red-contrib-suncalc
[node-red-contrib-moment]: https://flows.nodered.org/node/node-red-contrib-moment
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-node-geofence]: https://flows.nodered.org/node/node-red-node-geofence
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-node-ui-table]: https://flows.nodered.org/node/node-red-node-ui-table
[owntracks]: https://owntracks.org
[hue]: https://www2.meethue.com/
[pi]: https://www.raspberrypi.org/
[mosquitto]: https://mosquitto.org/
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/
[zigbee]: https://zigbeealliance.org/
[javascript]: https://en.wikipedia.org/wiki/JavaScript
[openzwave]: http://openzwave.com/
[Z-Wave]: https://z-wavealliance.org
[z-stick]: https://aeotec.com/z-wave-usb-stick/
[hubitat]: https://hubitat.com/
[homie]: https://homieiot.github.io/
[Home Assistant]: https://www.home-assistant.io/
[OpenHAB]: https://www.openhab.org/
[Aeotec MultiSensor 6]: https://aeotec.com/z-wave-sensor/
[Sensative Strips Comfort]: https://sensative.com/sensors/strips-zwave/comfort/
[JASCO Outdoor Smart Switch]: https://byjasco.com/ge-z-wave-plus-plug-outdoor-smart-switch-500s
[Zooz S2 Power Strip]: https://www.getzooz.com/zooz-zen20-power-strip.html
[Rhasspy]: https://rhasspy.readthedocs.io/en/latest/
[mic-array]: https://www.seeedstudio.com/ReSpeaker-Mic-Array-v2-0.html
[mic-hat]: https://www.seeedstudio.com/ReSpeaker-2-Mics-Pi-HAT.html
[touchscreen]: https://www.raspberrypi.org/products/raspberry-pi-touch-display/
