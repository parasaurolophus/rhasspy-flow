# Concurrent Execution

_**Note:** For those who already know what "threads" are in the context of multi-tasking software, it is important to understand that under the covers, [Node-RED][] flows actually run in a single such thread. However, [Node-RED][] supports many features, as described below, which allow flows to behave as if they were multi-threaded. They borrow thread-related terminology, such as the name of the built-in `join` node. In other words, each path through a flow is in reality best understood as a "co-routine" rather than a true thread but the best way to understand how to safely use such features is by understanding how concurrent execution works in general. To make the exposition simpler and reduce confusion, this page does not make a distinction between the kind of "co-operative multitasking" that is really happening inside the [Node-RED][] run-time and true multi-threaded applications written in languages like C++, Java or the like for platforms that support preemptive multitasking._

Computers are always doing multiple things at the same time. Consider a typical desktop environment. A user can run more than one application at a time. Each application can potentially display more than one GUI window. There will usually be multiple desktop "widgets" displaying the current time, date, weather and so on in real time. It can pop up notifications for incoming messages, reminders, calendar events etc. at any time without causing any of the application windows to pause. Even a single application displaying a single windows needs to be capable of performing time-consuming calculations or similar operations while still remaining responsive to the keyboard and mouse so that such a time consuming operation can be canceled, if nothing else.

These are all examples of what programmers refer to as "multitasking" or "concurrent execution." It is one of the most ubiquitous features in computer software and yet one of the most difficult to get right. It is the cause of more bugs than any other single factor. Any time a bug is intermittent -- only affecting some users or affecting all users only some of the time -- it is very likely an issue caused by incorrect implementation of some set of features that execute concurrently. When programmers talk about "timing issues," "race conditions," "deadlock" or similar kinds of defects, they are referring to bugs related to badly implemented concurrent execution.

## Event-Driven Flows

Automation flows implemented in Node-RED almost always operate according to [event-driven](https://en.wikipedia.org/wiki/Event-driven_architecture) software design principles. Packages like [node-red-contrib-huemagic][] and [node-red-contrib-openzwave][] include nodes that can deliver device state change messages at any time. The [node-red-contrib-suncalc][] package exists to generate messages at programmable times of day, including times like sunrise and sunset that change dynamically over the course of a year. Flows that subscribe to [MQTT][] messages can receive them at any time. Things that can happen at unpredictable times are called _asynchronous events_.

Much of the time, an event-driven system like a [Node-RED][] flow is doing little or nothing but idly waiting for asynchronous events to occur. When they do, the paths emanating from nodes that can generate asynchronous events become active. Each path through a Node-RED flow corresponds to an independent task. When multiple tasks can run asynchronously, there are a number of possibilities for how they might or might not overlap in time and in the regions of executable code defining their behavior:

1. The task caused by one asynchronous event may complete and the system go back to an idle state before another asynchronous event occurs

2. While a task is executing as a result of one asynchronous event, a different asynchronous event may occur triggering a different task along a different path through the program's overall flow

3. While a task is executing as a result of an asynchronous event the same event may occur again before the first task has completed its execution

The first case cannot cause timing issues because only a single task is running at a time. Even though this may be true much or most of the time for many systems in the real world, programmers cannot rely on this for event-driven systems that can receive asynchronous events. The second only causes timing issues if the two tasks interact in some way, most probably by both needing access to the same resource like a particular physical device, database or the like. The third example is really just a special case of the second one, where the two tasks are more or less guaranteed to contend for the same set of resources.

For example, consider the physical architecture of a home automation network that includes [Philips Hue][hue] devices connected via a _Hue Bridge_ and [Node-RED][] flows that interface with the _Hue Bridge_ using [node-red-contrib-huemagic][].

```mermaid
graph TD

  nodered["Node-RED<br/>Server"]
  bridge["Hue<br/>Bridge"]
  device1["Hue Device 1"]
  device2["Hue Device 2"]

  nodered -- Wi Fi --- bridge
  bridge -- Zigbee --- device1
  bridge -- Zigbee --- device2
```

The [Node-RED][] flows might include something like:

![](two-hue-flows.png)

While those two flows appear to be totally independent of one another, note that in reality both of the [node-red-contrib-huemagic][] `hue-group` nodes labeled _Interior_ and _Balcony_ interact with the single Hue Bridge. Each time someone turns on or off a member of the _Interior_ or _Balcony_ groups using the native Hue app, a physical switch, motion sensor or the like, the bridge will generate corresponding messages that arise asynchronously from the `hue-group` nodes in [Node-RED][]. Simultaneously, anyone can click at any time on either of the [node-red-dashboard][] `ui_switch` controls in a web browser. When they do, [Node-RED][] will send a message from the output of the `ui_switch` to the input of the corresponding `hue-group` node according to the [closed-loop feedback pattern](../design/closed-loop-feedback-pattern.md). If two such events, say a click in the dashboard and a state change of some device, are widely enough spaced in time such that the task triggered by the UI gesture completes before the state change message arrives, this is the first kind of timing scenario described above and life is good. If the second event occurs before the first task has completed, this is the second kind of timing scenario if the UI event was for the _Interior_ group and the state change is for _Balcony_ group, or vice versa. This is the third kind of timing scenario if the UI event and state change message are for the same [Hue][] group.

In this case, there appears visually to be a difference between the second and third scenarios since at first glance they appear not to interact. This is not actually the case. As noted previously, there is only a single Hue Bridge in our system. Both `hue-group` nodes in reality must access some set of resources in common, even though that is not obvious just by looking at the graphical representation of the program's logic as a "flow." So whether or not we are in the second or third kind of asynchronous task execution actually makes little difference in this case. Experienced programmers will recognize that is more often true of most multitasking systems than not. Hence the ubiquity of so-called "timing issues" -- i.e. bugs due to misbehaving, overlapping tasks. Suppose, in this case, that a message is coming into a `hue-group` node from the output of a `ui_switch` node at the exact moment that a message is arriving from the Hue Bridge that the corresponding device state has changed externally. As unlikely as that sounds, if you run this kind of flow long enough with real devices and real users, it _will_ happen from time to time. One can easily imagine the internal logic implementing the `hue-group` getting confused.

If the maintainers of [node-red-contrib-huemagic][] have done a good job, the implementation of the `hue-group` node is able to handle such cases. There are standard software coding techniques that can be used to make sure that once one task has started to use a component like the `hue-group` node, other tasks are forced to wait such that only one task at a time is using that particular shared resource. Since the human brain tends to think linearly, getting such logic just right can be challenging. There are any number of different ways to get this kind of resource locking and sharing logic wrong, resulting in intermittent, hard to diagnose bugs of various kinds, commonly referred via quaint names like "race condition," "deadly embrace" and so on. At the time of this writing, the currently released versions of many very popular [Node-RED][] packages specifically including [node-red-contrib-huemagic][], [node-red-contrib-openzwave][], [node-red-node-mysql][] each do a rather mixed job of properly handling task synchronization issues. The `ui-group` node type, for example, does a good job of hiding such issues from flow developers that use it. But the `hue-bridge-node` node type from the same package does not do as good a job. The result is that if you use `hue-bridge-node` in any flows you will get various kinds of intermittent errors during your flows' execution unless you take your own [task-synchronization measures](../design/sub-flow-with-semaphore-pattern.md). The same is true of many of the [Node-RED][] packages I have used.

## Forking and Joining

As stated above, every distinct path through a flow represents a distinct task any of which could, in principle, be executing concurrently. For example, clicking on the `trigger` node in the following flow causes two separate tasks to execute, as can be seen by the two separate `debug` messages that appear in the log console for each single triggering event:

<a id="basic-fork-flow"></a>

![](basic-fork-flow.png)

There are a few things to note about the preceding example:

1. Where multiple paths emanate from a single output of a node, the same message is sent concurrently along each path

2. As messages flow along concurrent paths, different transformations and side-effects may occur on each path without affecting the others

3. When multiple paths converge on a single input of a node, that node is invoked as many times as there are tasks that arrive at that input

4. One cannot predict the order in which multiple inputs will arrive at a given node along different tasks based on the visual layout of a flow

The third and fourth points are crucial. Even though the paths appear to "join together" at the `debug` node, the fact that the `debug` node is invoked twice shows that there actually remain two distinct flows of control. If we put some kind of "pass-through" node in front of `debug`, two messages would still appear but now after having traveled independently, one after the other, along a single path for part of the length of the flow. There would still be two distinct tasks the whole way, but each one would be executing the same flow logic from the point at which the paths converge onward:

![](converging-flows.png)

The preceding flow from the output of the `delay` node onward is the same case as described above where a second asynchronous event occurs before execution has completed for a prior occurrence of the same event. I.e. it is a case where there are two messages flowing along the same logic path. Each message represents a separate task that happens to be going through the same sequence of nodes, starting at different points in time. Other things being equal, whichever started first will remain "ahead" in the "race" between messages. However, if the path forks such that a message that started later ends up going through a branch of logic that completes faster, it can "catch up" to an earlier message and reach some point where the paths re-converge ahead of the other message. These sorts of analogies is where terms like "race condition" arise. Realistically complex multitasking systems can easily have logic such that sometimes one task "wins the race" to some synchronization point and sometimes another one does due to dynamically changing factors that occur during the execution of each task. If the program has a bug such that things go as expected when a given task "wins the race" but misbehavior occurs when some other task "wins," that is a "race condition."

A "synchronization point," as referred to in the preceding paragraph, is where two or more tasks actually join together into a single subsequent task. To actually cause multiple paths in a [Node-RED][] flow to join together into a single downstream task, use a `join` node:

![](join-flow.png)

Note that a single click of the `trigger` in the preceding example results in a single output message in the `debug` console. That single message contains information provided by the distinct messages that arrived as inputs to the `join` node. This capability is critical to the design of multitasking, event-driven flows. You can use `join` nodes to force the overall flow execution to wait until multiple tasks have converged at the same time at a given `join`. This is called "task synchronization" for obvious reasons. Once a collection of tasks have been synchronized by `join`, flow execution resumes with access to information obtained from all of them.

## The Deceptively Visual Programming Paradigm

As demonstrated by some of the preceding screen shots, it is unwise to make assumptions about the order in which messages will arrive at the end of divergent flows. In fact, it is not possible to discern from just looking at a flow how many tasks might be running concurrently. As already seen, more than one task can be active at the same time on a single path through the flow. There can also be distinct paths with messages traveling along none, some or all at the same time. Question: how many outputs would one expect to see for each click of the `trigger` node in the following flow?

![](switch-flow.png)

Based only on the discussion about paths with forks in them and how this flow _looks_, one would expect the answer to be 2, just as for the first flow in the [Forking and Joining](#basic-fork-flow) section.

The real answer is, "it depends." If you open the settings dialog for the `switch` node, you will see that:

1. There are two [JSONata][] rules
    - The first rule matches if the input payload is divisible by 2
    - The second rule matches if the input payload is divisible by 3
2. The node is configured to evaluate all rules rather than stopping after the first match

![](switch-settings.png)

The resulting behavior is that for each click of the `trigger` node there may be 0, 1 or 2 output tasks from the `switch` node depending on the timestamp value at the exact moment the `trigger` is clicked. If the timestamp happens to be divisible by 2 a task will emerge from the upper output from the `switch` node. If the timestamp happens to be divisible by 3 a task will emerge from the lower output. If it happens to be divisible by both 2 and 3, two concurrent tasks will merge -- one from each of the two outputs. If the time happens not to be divisible by either 2 or 3 then execution stops at the `switch` node and nothing is written to the `debug` log.

Conversely, the following flow _looks_ as if it represents a single task. However, each click of its `trigger` outputs two separate debug messages and so was the result of two messages traveling along the same logic path, one after the other:

![](split-flow.png)

The moral of all this is that you cannot tell what a flow's logic is just by looking at its graphical representation. Where nodes like `split`, `switch` and `join` are concerned, you must look at their decidedly non-graphical settings to tell what sort of flow of control they actually represent. Any sort of non-trivial flow logic will rely on constructs like [JSONata][] and JavaScript `function` nodes which removes the logic even farther from the graphical paradigm.

Anyone familiar with traditional programming languages will recognize the names of many [Node-RED][] node types as corresponding to basic features common to most programming languages and platforms. A [Node-RED][] `switch` node (not to be confused with the `ui_switch` node type from the [node-red-dashboard][] package) is more or less equivalent to a _switch_ statement in languages like JavaScript, Java, C++ or C#. (Actually, it is closer to Lisp's _cond_ statement, but I digress....) Similarly, a `join` node is more or less equivalent to Java's _Thread.join()_ method, which is based on the underlying POSIX thread library's _pthread_join()_ function. And so on.

If you really want to become adept at [Node-RED][] flow creation, you should learn to program in a conventional, non-graphical programming language first, with an emphasis on "advanced" concepts like multitasking and asynchronous event handling. This advice runs counter to the claims of fans of the "graphical programming paradigm" as a pedagogical tool or those who think that graphical programming platforms empower "non-programmers" to create effective automation. It is true none the less. Here is a screen shot of one of my actual flows used for my real home automation system:

![](../design/z-wave-metadata-flow.png)

Good luck figuring out what that does simply by looking at its graphical representation!

[JSONata]: https://jsonata.org/
[mqtt]: http://mqtt.org/
[node-red]: https://nodered.org
[node-red-contrib-huemagic]: https://flows.nodered.org/node/node-red-contrib-huemagic
[node-red-contrib-suncalc]: https://flows.nodered.org/node/node-red-contrib-suncalc
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-node-mysql]: https://flows.nodered.org/node/node-red-node-mysql
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-contrib-semaphore]: https://flows.nodered.org/node/node-red-contrib-semaphore
[node-red-node-ui-table]: https://flows.nodered.org/node/node-red-node-ui-table
[owntracks]: https://owntracks.org
[hue]: https://www2.meethue.com/
[pi]: https://www.raspberrypi.org/
[mosquitto]: https://mosquitto.org/
[duckdns]: https://www.duckdns.org/
[certbot]: https://letsencrypt.org/
[zigbee]: https://zigbeealliance.org/
[javascript]: https://en.wikipedia.org/wiki/JavaScript
[openzwave]: http://openzwave.com/
[Z-Wave]: https://z-wavealliance.org
[z-stick]: https://aeotec.com/z-wave-usb-stick/
[hubitat]: https://hubitat.com/
[homie]: https://homieiot.github.io/
[Home Assistant]: https://www.home-assistant.io/
[OpenHAB]: https://www.openhab.org/
[Aeotec MultiSensor 6]: https://aeotec.com/z-wave-sensor/
[Sensative Strips Comfort]: https://sensative.com/sensors/strips-zwave/comfort/
[JASCO Outdoor Smart Switch]: https://byjasco.com/ge-z-wave-plus-plug-outdoor-smart-switch-500s
[Zooz S2 Power Strip]: https://www.getzooz.com/zooz-zen20-power-strip.html
[Rhasspy]: https://rhasspy.readthedocs.io/en/latest/
[mic-array]: https://www.seeedstudio.com/ReSpeaker-Mic-Array-v2-0.html
[mic-hat]: https://www.seeedstudio.com/ReSpeaker-2-Mics-Pi-HAT.html
[touchscreen]: https://www.raspberrypi.org/products/raspberry-pi-touch-display/
[speaker]: https://www.logitech.com/en-us/product/multimedia-speaker-z50
