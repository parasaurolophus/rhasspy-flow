# Relational Databases and Structured Query Language

_Structured Query Language_ (SQL, pronounced "seequal") is a formal language used to create, modify and retrieve data stored in a _Relational Database Management`System_ (RDBMS).

One way to understand a RDBMS is by comparison to spreadsheets in a program like Microsoft Excel. A relational database consists of one or more _tables_, where each table can be visualized as functioning similarly to one _sheet_ in an Excel _workbook_. Each table consists of a sequence of _rows_, each of which represents a single data record. A row consists of a collection of named _columns_. A given column contains a particular datum of some specified _type_ such as a text string, an integer, a floating point number and so on.

## Flat Contact List

For example, a "flat" or non-relational database representing a contact list would correspond to an Excel workbook with a single sheet. That sheet might have columns with labels like "First Name," "Last Name", "Mailing Address," "Home Phone" etc.

| First Name | Last Name | Mailing Address | Home Phone   |
|------------|-----------|-----------------|--------------|
| Jane       | Doe       | 123 Main St     | 555-555-1234 |
| John       | Doe       | 123 Main St     | 555-555-1234 |
| Jill       | Roe       | 456 East Ave    | 555-555-4321 |
| Jack       | Low       | 789 West Ave    | 666-666-5678 |

In this data model, there is only a single type of entity. That entity represents the full contact information for an individual person.

(See the section on _Class Diagrams_ in [Understanding UML](./uml.md) for information on how to interpret diagrams like this.)

![](flat_contact_class.svg)


Here are SQL statements that create such a database:

```sql
-- all the examples in this document assume that you have
-- logged in using an account with administrative permissions
--
--     > mysql -u root -p
--
-- delete an existing database named 'examples', if present,
-- so as to start fresh
drop database if exists examples;

-- create a new, empty database named 'examples'
create database examples;

-- tell mysql to direct all subsequent commands to the newly
-- created 'examples' database
use examples;

-- add a single table representing a flat contact list
create table flat_contact (
  first_name varchar(255),
  last_name varchar(255),
  mailing_address varchar(255),
  home_phone char(12)
);

-- insert some contacts into the flat table
insert into flat_contact (
  first_name,
  last_name,
  mailing_address,
  home_phone
) values (
  'Jane',
  'Doe',
  '123 Main St',
  '555-555-1234'
);
insert into flat_contact (
  first_name,
  last_name,
  mailing_address,
  home_phone
) values (
  'John',
  'Doe',
  '123 Main St',
  '555-555-1234'
);
insert into flat_contact (
  first_name,
  last_name,
  mailing_address,
  home_phone
) values (
  'Jill',
  'Roe',
  '456 East Ave',
  '555-555-4321'
);
insert into flat_contact (
  first_name,
  last_name,
  mailing_address,
  home_phone
) values (
  'Jack',
  'Low',
  '789 West Ave',
  '666-666-5678'
);
```

Having executed the preceding SQL statements you will have a database named `examples` with a table named `flat_contact`. That table will have four columns, `first_name`, `last_name`, `mailing_address` and `home_phone`. It will contain as many rows as there are `insert` statements. Each row will contain the information for one person's contact information.

In particular:

- `drop database if exists examples` deletes any existing database named "examples" without signaling an error if none exists; this allows us to start fresh

- `create database examples` creates a new, empty database named "examples"

- `create table flat_contact ...` adds a table to the database with the specified columns

- Each `insert into flat_contact ...` statement adds a row to the `flat_contact` table with the specified values for the new record's columns

The collection of information defining the structure of a database and imposing constraints on how that data can be used is called its _schema_. In the present example, our database, so far, has a schema consisting of just a single table of four columns with no rules governing what can be stored in each column other than that it be of the data _type_ specified for it in the `create table ...` command.

In this case, the type `varchar(255)` indicates that the `first_name`, `last_name` and `mailing_address` columns can each hold a variable length string of up to a maximum of 255 characters. The type `char(12)` indicates that the `home_phone` column holds a string of exactly 12 characters, i.e. the number of characters in the conventional representation of a US-style telephone number.

Use `select` statements to retrieve data:

```sql
-- display the contact information in the flat table
use examples;

select
  first_name,
  last_name,
  mailing_address,
  home_phone
from flat_contact;
```

This will produce the following _result set_:

```
+------------+-----------+-----------------+--------------+
| first_name | last_name | mailing_address | home_phone   |
+------------+-----------+-----------------+--------------+
| Jane       | Doe       | 123 Main St     | 555-555-1234 |
| John       | Doe       | 123 Main St     | 555-555-1234 |
| Jill       | Roe       | 456 East Ave    | 555-555-4321 |
| Jack       | Low       | 789 West Ave    | 666-666-5678 |
+------------+-----------+-----------------+--------------+
```

Note the redundant information in a flat database where more than one record contains the same data. In our contrived contact list, Jane and John Doe live at the same address and share the same home phone number. Unless they separate as a couple, if one's address or phone number changes then the other's does as well. This makes storing their contact information both wasteful of space and fragile to maintain. When updating the contact information for either John or Jane you must remember to also update their spouse's information, as well.

SQL supports some shortcuts to make typing commands easier. For example, use `*` for the list of column names if you want to retrieve the data from all the columns in a table without having to type each name explicitly. I.e. the following `select` statement produces the same result set as the previous one:

```sql
select * from flat_contact;
```

You can also filter the output of `select` to contain only records that match some set of criteria using a `where` clause. For example, to see the contact information for only the Doe family:

```sql
select *
from flat_contact
where last_name = 'Doe';
```

This will produce the following result set:

```
+------------+-----------+-----------------+--------------+
| first_name | last_name | mailing_address | home_phone   |
+------------+-----------+-----------------+--------------+
| Jane       | Doe       | 123 Main St     | 555-555-1234 |
| John       | Doe       | 123 Main St     | 555-555-1234 |
+------------+-----------+-----------------+--------------+
```

## Relational Contact List

To avoid the redundancy described above for flat data, consider people, places and phone numbers as distinct entities with relationships between them.

![](contact_classes_many_to_one.svg)

This way of representing a contact list would be implemented in a RDBMS using three tables, one for each type of entity:

**Address table:**

| Key | Street       |
|-----|--------------|
| 1   | 123 Main St  |
| 2   | 456 East Ave |
| 3   | 789 West Ave |

**Phone table:**

| Key | Number       |
|-----|--------------|
| 1   | 555-555-1234 |
| 2   | 555-555-4321 |
| 3   | 666-666-5678 |

**Person table:**

| First Name | Last Name | Mailing Address | Home Phone |
|------------|-----------|-----------------|------------|
| Jane       | Doe       | 1               | 1          |
| John       | Doe       | 1               | 1          |
| Jill       | Roe       | 2               | 2          |
| Jack       | Low       | 3               | 3          |

Note that the _Person_ table here is like the flat contact list example, above, except that address and phone number columns contain references to records in other tables rather than directly containing the corresponding data. These references are created by populating a column in one table with some unique identifier, called a _key_, for a record in another table. Now, if the value in the _Street_ column of the _Address_ table changes, no change is required in any record of the _Person_ table but the overall contact data remains consistent and correct. For example, if _Street_ in record 1 of the _Address_ table becomes "111 Maple Ln," both Jane and John's contact records in the _Person_ table will remain consistent and correct without having to modify either of them.

Here are the SQL statements for our first attempt at creating a relational contact list schema:

```sql
-- delete an existing database named 'examples', if present
--, so as to start fresh
drop database if exists examples;

-- create a new, empty database named 'examples'
create database examples;

-- tell mysql to direct all subsequent commands to the newly
-- created 'examples' database
use examples;

create table address (
  address_id int auto_increment primary key,
  street varchar(255) unique
);

create table phone (
  phone_id int auto_increment primary key,
  phone_number char(12) unique
);

-- add three tables for our relational contact list schema
create table person (
  first_name varchar(255),
  last_name varchar(255),
  address_id int,
  phone_id int,
  foreign key (address_id) references address(address_id),
  foreign key (phone_id) references phone(phone_id)
);

-- populate the relational version of the contact list tables
insert into address (street) values ('123 Main St');
insert into address (street) values ('456 East Ave');
insert into address (street) values ('789 West Ave');

insert into phone (phone_number) values ('555-555-1234');
insert into phone (phone_number) values ('555-555-4321');
insert into phone (phone_number) values ('666-666-5678');

insert into person (
  first_name,
  last_name,
  address_id,
  phone_id
) values (
  'Jane',
  'Doe',
  1,
  1
);

insert into person (
  first_name,
  last_name,
  address_id,
  phone_id
) values (
  'John',
  'Doe',
  1,
  1
);

insert into person (
  first_name,
  last_name,
  address_id,
  phone_id
) values (
  'Jill',
  'Roe',
  2,
  2
);

insert into person (
  first_name,
  last_name,
  address_id,
  phone_id
) values (
  'Jack',
  'Low',
  3,
  3
);
```

Note the use of the `unique` keyword when defining the `street` column in the `address` table and the `phone_number` column in the `phone` table. This instructs the database to reject modification that would result in redundant data. After having run the preceding commands, you will get an error message and no modification will have been performed if you try to add a redundant record to the `phone` or `address` tables:

```sql
> insert into phone (phone_number) values ('666-666-5678');
ERROR 1062 (23000): Duplicate entry '666-666-5678' for key 'phone_number'
```

Instead of strings, as in the `flat_contact` table, the `person` table declares its `phone_id` and `address_id` columns to be of type `int` meaning they contain integers, i.e. whole numbers with no fractional part. These correspond to the `phone_id` and `address_id` columns in the `phone` and `address` tables, which is why I deliberately used the same names for the corresponding columns in each of the tables. This use is made explicit by way of the `foreign key ...` constraint clauses in `create table`. Declaring this relationship explicitly allows the RDBMS to enforce some kinds of _referential integrity_ rules automatically, as discussed below.

Note also the keywords, `auto_increment primary key`, used when defining the `address_id` and `phone_id` columns in the `address` and `phone` tables, respectively. Like `unique`, each of the `primary key` phrase imposes constraints on the values that can be placed in those columns that make them suitable for referencing as foreign keys in other tables. The `auto_increment` keyword instructs the database to automatically supply a default value if none is supplied explicitly when inserting a new row into the corresponding table. In particular, `auto_increment` inserts 1 for the id value for the first record, 2 for the second record and so on. These are all examples of functionality that a RDBMS provides to support data integrity and consistency in an automated fashion. While they are among the features that make relational databases an extremely robust form of data storage, they add computational overhead. Every time a record is added or modified, the RDBMS must expend effort ensuring that all default values are calculated and no constraints are violated.

## Joining Data

To access the contents of a relational database in a meaningful way requires combining data from multiple tables. For example, if you have been following along with the examples given so far, using `select * from person` produces the following rather unhelpful results:

```
+------------+-----------+------------+----------+
| first_name | last_name | address_id | phone_id |
+------------+-----------+------------+----------+
| Jane       | Doe       |          1 |        1 |
| John       | Doe       |          1 |        1 |
| Jill       | Roe       |          2 |        2 |
| Jack       | Low       |          3 |        3 |
+------------+-----------+------------+----------+
```

This is because the schema for the `person` table uses _foreign keys_ as references to the actual data stored in the separate `address` and `phone` tables. The numbers in the `address_id` and `phone_id` columns of the `person` tables are the corresponding _primary keys_ in the `address` and `phone` tables, respectively.

To display the full contact list, we need to add `join` clauses to our `select` statement. This joins together data from all three tables, using the values in the various key columns to match records:

```sql
-- display all contact info from the relational contact list
select
  person.first_name,
  person.last_name,
  address.street,
  phone.phone_number
from person
join address on person.address_id = address.address_id
join phone on person.phone_id = phone.phone_id;
```

Now we get results that look identical to the `flat_contact` examples:

```
+------------+-----------+--------------+--------------+
| first_name | last_name | street       | phone_number |
+------------+-----------+--------------+--------------+
| Jane       | Doe       | 123 Main St  | 555-555-1234 |
| John       | Doe       | 123 Main St  | 555-555-1234 |
| Jill       | Roe       | 456 East Ave | 555-555-4321 |
| Jack       | Low       | 789 West Ave | 666-666-5678 |
+------------+-----------+--------------+--------------+
```

Even though the result set looks identical to that in the corresponding flat contact list example, under the covers the redundancy has been eliminated from how the data is stored. There is only one record in the `address` table for "123 Main St" that is shared by the `person` records for Jane and John. Ditto for the "555-555-1234" record in the `phone` table.

The syntax of `select ... join ...` statements can be a bit challenging. To simplify things, consider just the address information:

```sql
select
  person.first_name,
  person.last_name,
  address.street
from person
join address on person.address_id = address.address_id;
```

The "_table_._column_" syntax tells the database to extract the `first_name` and `last_name` columns from the `person` table and the `street` column from the `address` table. The `from` keyword introduces the "left side" of the join, as in the flat examples. (The distinction between the "left" and "right" sides of a join will become significant in the discussion of different [Join Types](#join-types), below.) It is extracting data from the `person` table in this case. The `join` clause introduces the "right side" of the join. It is extracting data from the `address` table. The `on` expression tells the database how to decide whether or not two records, one from the left and one from the right, match and so should be included in the result set. In this case, the rule is that two records match if they have the same value in their respective `address_id` columns. It produces:

```
+------------+-----------+--------------+
| first_name | last_name | street       |
+------------+-----------+--------------+
| Jane       | Doe       | 123 Main St  |
| John       | Doe       | 123 Main St  |
| Jill       | Roe       | 456 East Ave |
| Jack       | Low       | 789 West Ave |
+------------+-----------+--------------+
```

This is because, for example, the `person.address_id` value for Jane is the same as the `address.address_id` value for "123 Main St."

The full contact list example shown previously adds a fourth column to the output and another `join` clause that pulls data in from the `phone` table in the same way.

## Normalization and Performance

The kind of transformation shown above from the "flat" contact list schema to a collection of three tables with relationships between them is called "normalization." Normalization is not an all-or-nothing process. For example, so far we have been treating US-style phone numbers as monolithic strings of digits and hyphens. This makes sense for an individual user's private contact list.

Suppose that instead of a private contact list, we were creating a database for some application used by a North American telephone company. A telephone company has a much more complicated set of requirements regarding telephone numbers than individual people who just want to make calls. For example, as the population of a given geographic region grows, it sometimes becomes necessary to add more area codes to that region, dividing what was a single region into multiple new regions from the telephone company's point of view. A consequence is that at certain times the phone company must force large numbers of existing land-line subscribers to change their phone numbers _en masse_ to use a new set of area codes.

Similarly, the prefix part of a US-style phone number (the fourth through sixth digits) can have meaning related to groups of subscribes by region, institutional affiliation ec. A phone company application will probably need the ability to search, sort, filter and group phone numbers based on their prefixes as well as their area codes.

To support these kinds of scenarios, a phone company's database would probably normalize phone numbers into (at least) three tables: one each for area code, prefix, and suffix. Just as our three-table schema is more normalized than the original, flat, one-table schema, if we adopted the normalized version of a telephone number for our contact list application that version of the schema would become even more normalized than the versions we have discussed previously. It would have six tables instead of three and everywhere a phone number was referenced it would be done by way of yet more cross-references between tables, implying even more complicated joins, more constraint processing overhead and so on.

Generally speaking, the more normalized a database becomes the less redundancy and therefore the smaller the storage capacity is required. (This is not a perfectly general rule, but is true for practical purposes for real-world tables that are likely to be normalized in the first place. The exception to "more normalized is smaller" comes when the storage required to represent the keys that link records between tables consume more storage than the redundant, non-normalized data would consume. If this ever becomes a real issue for an actual database, consider it a warning sign that something has probably gone wrong in your overall software design or at least that you should consider an aggressive optimization to your schema like carefully choosing the data types and maximum ranges for primary keys.) More normalized data is also easier to maintain, at least for some types of maintenance tasks, as already described using our contact list example. The shared data in normalized tables are to database schema design what modularity and encapsulation are to software design.

As is always the case, these kinds of advantages do not come for free. The more normalized a schema, the harder it is for human beings to understand the data simply by reading the contents of individual tables. This is due to the use of foreign keys in place of redundant data, as already discussed. Normalizing a schema also increases the computational complexity of standard operations like querying and modifying the data, as discussed above in regards to constraints like `unique`, `primary key` and below in the description of various kinds of joins. For these reasons, flat (so-called "noSQL") databases made a comeback starting early in the 21st after having become nearly extinct by the 1990's. "Big data" style applications often have performance and latency requirements when processing astronomically large volumes of data such that the processing overhead associated with normalized data becomes too high. As a result, "noSQL" data stores have become "fashionable" to the point that some development teams default to using them even in contexts where a relational database would probably be a better choice. You need to consider carefully the trade-offs and analyze your application's actual throughput requirements and transaction volumes before deciding on a persistence model.

In addition, many kinds of data are naturally hierarchical and relational. Attempting to represent such data using a flat database will result in application code having to perform work that a RDBMS would have provided more efficiently. in a more scalable fashion than an application development team is likely to produce as part of _ad hoc_ software design. I.e. you also need to understand your overall software requirements and have some idea of your ultimate software design before settling on a persistence model.

Examples of such naturally relational data include the kind of contact information already discussed but extends to any kind of "directory" style of data such as used in user account management protocols like LDAP. The metadata that drives movie and television program guide listings in TV and streaming media services is another example. In fact, it is fair to say that most data that is worth storing in a database to start with can benefit from the features of a relational database. For this reason, in the end most real-world applications end up either just using relational databases or a hybrid approach where "noSQL" databases are used during initial ingestion and / or ultimate publishing of data, as part of processing pipelines that feed data in and out of relational databases as their "systems of record."

## Maintaining Data Integrity

Relational databases simplify the task of maintaining data consistency in some scenarios, as described above. They also can make it more difficult in other respects. For example, suppose Jill and Jack decide to start living together at what was formerly Jill's home on her own. In the flat example this would require modifying Jack's record with a redundant copy of the mailing address and home phone information from Jill's, but there would be nothing else to consider.

Things are more complex in the relational example. Here, Jack's move to Jill's address would be accomplished by overwriting the key in the `person.address_id` column of Jack's record with the same key as that in Jill's record. Ditto for `person.phone_id`. But then we are left with a question: what is to be done with the records in the `address` and `phone` tables for what had been Jack's home? The answer is, as in so many cases, "it depends." For an individual user's private contact list, it probably makes sense to delete such "orphaned" records. For a multi-user database to support, say, an online telephone directory it probably makes sense to maintain such records on the assumption that someone else will move into that address before very long and the abandoned phone number will be assigned to a new subscriber. A RDBMS has features to support either of these scenarios depending on the requirements of a particular application. That is the main reason that it is important to add appropriate constraints like `not null`, `unique`, `foreign key` and so on in your database schemas. Given the distributed nature of the data in a relational model, it is very easy to accidentally break an application by making what seems like a trivial change to the contents of one table. This is quite analogous to the common experience of software engineers who make a small change to fix one bug and end up introducing more because of unanticipated consequences. RDBMS services exist to help prevent such accidents in data stores just as static code analyzers and unit test frameworks exist to prevent them in application code.

## Multiplicity of Relationships

The preceding model of a contact list, whether flat or relational, is obviously extremely over simplified. In the real world, for one thing, a given person can have multiple addresses (home, work, vacation cabin etc.) or no known address at all.

In database design terms, the relationship between the `person` and `address` entities shown previously was "many to one," i.e. a given address could be shared by more than one person but each person had exactly one address. In reality, this relationship is "many to many," i.e. a given person can have zero or more known addresses _and_ an address can be shared by zero or more people. The same is true of phone numbers. E.g. from a telephone company's point of view, when an account holder relinquishes a phone number it becomes available for re-assignment to a different customer.

To represent such many-to-many associations in a relational database we add yet more tables and use more keys as references between them. Here is a high level, conceptual model of our more sophisticated contact list:

![](contact_classes_many_to_many.svg)

At first glance this looks the almost exactly the same as the previous many-to-one contact list model. Note, however, that unlike the earlier representation, the references from `person` to `address` and `phone` are now shown explicitly as being many-to-many and non-directional instead of many-to-one and directional from `person` to the other entities.

The physical model in a relational database requires additional entities to explicitly represent such many-to-many, non-directional relationships. In particular, the `address_id` and `phone_id` columns are removed from the `person` table. Their purpose is replaced by additional tables. We can introduce the `resident` table to link records in the `person` table to records in `address`. The `subscriber` table in our new schema links records in `person` to `phone`. Here is the UML representation of the physical model of our contact list with many-to-many relationships, displaying `resident` and `subscriber` tables explicitly as association classes:

![](associations.svg)

This requires adding a primary key column to the `person` table, `person_id`, so that it can be referenced by the new tables. We can enhance our data to demonstrate the new many-to-many features of our new data model. Let us now suppose that Jill has two phone number and Jack two addresses.

**Address table:**

| Key | Street           |
|-----|------------------|
| 1   | 123 Main St      |
| 2   | 456 East Ave     |
| 3   | 789 West Ave     |
| 4   | 99 Lake Front Dr |

**Phone table:**

| Key | Number       |
|-----|--------------|
| 1   | 555-555-1234 |
| 2   | 555-555-4321 |
| 3   | 555-555-5555 |
| 4   | 666-666-5678 |

**Person table:**

| Key | First Name | Last Name |
|-----|------------|-----------|
| 1   | Jane       | Doe       |
| 2   | John       | Doe       |
| 3   | Jill       | Roe       |
| 4   | Jack       | Low       |

**Resident table:**

| Person | Address |
|--------|---------|
| 1      | 1       |
| 2      | 1       |
| 3      | 2       |
| 4      | 3       |
| 4      | 4       |

**Subscriber table:**

| Person | Phone |
|--------|-------|
| 1      | 1     |
| 2      | 1     |
| 3      | 2     |
| 3      | 4     |
| 4      | 3     |

The pairs of keys in `resident` define the relationship between people and addresses. The pairs of keys in the `subscriber` tables define the relationships between people and phones numbers. In this new example, note that now Jill is known to have two phone numbers and Jack to have two addresses. Here is the actual SQL defining and populating this new schema:

```sql
-- (re-) create examples database
drop database if exists examples;
create database examples;
use examples;

-- create tables

create table person (
  person_id int auto_increment primary key,
  first_name varchar(255),
  last_name varchar(255)
);

create table address (
  address_id int auto_increment primary key,
  street varchar(255) unique not null
);

create table phone (
  phone_id int auto_increment primary key,
  phone_number char(12) unique not null
);

create table resident (
  person_id int not null,
  address_id int not null,
  foreign key (person_id) references person(person_id),
  foreign key (address_id) references address(address_id)
);

create table subscriber (
  person_id int not null,
  phone_id int not null,
  foreign key (person_id) references person(person_id),
  foreign key (phone_id) references phone(phone_id)
);

-- populate tables

insert into address (street) values ('123 Main St');
insert into address (street) values ('456 East Ave');
insert into address (street) values ('789 West Ave');
insert into address (street) values ('99 Lake View Dr');

insert into phone (phone_number) values ('555-555-1234');
insert into phone (phone_number) values ('555-555-4321');
insert into phone (phone_number) values ('555-555-5555');
insert into phone (phone_number) values ('666-666-4678');

insert into person (
  first_name,
  last_name
) values (
  'Jane',
  'Doe'
);
insert into person (
  first_name,
  last_name
) values (
  'John','Doe'
);
insert into person (
  first_name,
  last_name
) values (
  'Jill',
  'Roe'
);
insert into person (
  first_name,
  last_name
) values (
  'Jack',
  'Low'
);

insert into resident (person_id,address_id) values (1,1);
insert into resident (person_id,address_id) values (2,1);
insert into resident (person_id,address_id) values (3,2);
insert into resident (person_id,address_id) values (4,3);
insert into resident (person_id,address_id) values (4,4);

insert into subscriber (person_id,phone_id) values (1,1);
insert into subscriber (person_id,phone_id) values (2,1);
insert into subscriber (person_id,phone_id) values (3,2);
insert into subscriber (person_id,phone_id) values (3,3);
insert into subscriber (person_id,phone_id) values (4,4);

-- display all date in a single view
select
  person.first_name,
  person.last_name,
  address.street,
  phone.phone_number
from person
join resident on person.person_id = resident.person_id
join subscriber on person.person_id = subscriber.person_id
join address on  address.address_id = resident.address_id
join phone on  phone.phone_id = subscriber.phone_id;
```

The output of the final `select` statement, above, should look like:

```
+------------+-----------+-----------------+--------------+
| first_name | last_name | street          | phone_number |
+------------+-----------+-----------------+--------------+
| Jane       | Doe       | 123 Main St     | 555-555-1234 |
| John       | Doe       | 123 Main St     | 555-555-1234 |
| Jill       | Roe       | 456 East Ave    | 555-555-4321 |
| Jill       | Roe       | 456 East Ave    | 555-555-5555 |
| Jack       | Low       | 789 West Ave    | 666-666-4678 |
| Jack       | Low       | 99 Lake View Dr | 666-666-4678 |
+------------+-----------+-----------------+--------------+
```

Note that this version of the `select` statement pulls together data from five tables, as indicated by the single `from` and four `join` clauses, rather than only 3. In other respects it is nearly identical to the previous `select ... from ... join ...` statement used in the many-to-one version of the data model. Note also that the data does not look exactly as one might have naively hoped. Where there is more than one match between a single record of the `person` table with distinct values of records in the `address` and `phone` tables there are multiple rows in the result set returned by `select`. In this case, note the two rows each for Jill and Jack due to Jill's two phone numbers and Jack's two addresses. It would be possible to adjust either the `select` statement or the data model to group such input records together into single records in the result set, but that is beyond the scope of this tutorial.

Let us return to the scenario in which Jack moves in with Jill some time after the preceding `insert` statements were executed. We might update the tables shown above to show that while Jill and Jack now share two addresses (Jill's home and Jack's vacation cabin), she still maintains her separate work phone number. To do so we update the `resident` and `subscriber` tables. Also, on the assumption this is a private contact list application, we delete the orphaned records left behind in the `address` and `phone` tables by Jack's move. Here are the additional SQL statements to execute after the preceding set:

```sql
-- move Jack to Jill's home
delete from resident where person_id = 4 and address_id = 3;
delete from address where address_id = 3;
insert into resident (person_id,address_id) values (4, 2);
delete from  subscriber where person_id = 4 and phone_id = 4;
insert into resident (person_id,address_id) values(3,4);
delete from phone where phone_id = 4;
insert into subscriber (person_id,phone_id) values (4,2);

-- display all date in a single view
select
  person.first_name,
  person.last_name,
  address.street,
  phone.phone_number
from person
join resident on person.person_id = resident.person_id
join subscriber on person.person_id = subscriber.person_id
join address on  address.address_id = resident.address_id
join phone on  phone.phone_id = subscriber.phone_id;
```

The output from `select` after these insertions and deletions should be:

```
+------------+-----------+-----------------+--------------+
| first_name | last_name | street          | phone_number |
+------------+-----------+-----------------+--------------+
| Jane       | Doe       | 123 Main St     | 555-555-1234 |
| John       | Doe       | 123 Main St     | 555-555-1234 |
| Jill       | Roe       | 456 East Ave    | 555-555-4321 |
| Jill       | Roe       | 456 East Ave    | 555-555-5555 |
| Jack       | Low       | 99 Lake View Dr | 555-555-4321 |
| Jack       | Low       | 456 East Ave    | 555-555-4321 |
| Jill       | Roe       | 99 Lake View Dr | 555-555-4321 |
| Jill       | Roe       | 99 Lake View Dr | 555-555-5555 |
+------------+-----------+-----------------+--------------+
```

Note that it is important to keep the same values in the primary key columns in the various tables across any such updates in order to keep the references between tables consistent and correct. Depending on the requirements of a given application, other columns' values may or may not be treated as immutable. For example, if either of the couples Jane and John or Jack and Jill were to move to a new address that does not yet appear in the `address` table, such a change could be accomplished by adding a new `address` record and updating the two `resident` records accordingly. But it would actually be more efficient simply to alter the value of the `street` column in the existing `address` record. Which is actually better, as usual, depends on application requirements. Much more on that later in the section on [Join Types](#join-types).

As a concrete example, let's suppose that the Does decide to move from "123 Main St" to "111 Maple Ln." The most efficient way to accomplish this is with yet another type of SQL statement, `update`:

```sql
update address
set street = "111 Maple Ln"
where street = "123 Main St";

select
  person.first_name,
  person.last_name,
  address.street,
  phone.phone_number
from person
join resident on person.person_id = resident.person_id
join subscriber on person.person_id = subscriber.person_id
join address on  address.address_id = resident.address_id
join phone on  phone.phone_id = subscriber.phone_id
where person.last_name = "Doe";
```

As promised near the beginning of this tutorial, the output of the select now shows Jane and John's new address after updating a single record, neither of which mentions John or Jane explicitly:

```
+------------+-----------+-----------------+--------------+
| first_name | last_name | street          | phone_number |
+------------+-----------+-----------------+--------------+
| Jane       | Doe       | 111 Maple Ln    | 555-555-1234 |
| John       | Doe       | 111 Maple Ln    | 555-555-1234 |
+------------+-----------+-----------------+--------------+
```

However, this is actually a bit of a cheat that works because of a number of implicit assumptions that are true in our contrived example but cannot be relied on for a real application. Even in our private contact list scenario, note that modifying the `phone.street` column in the existing record with `phone.phone_id` value of 1 as a means of moving the Does assumes:

1. No-one but the Does live at "123 Main St" before the move
2. No-one will be living at "123 Main St" after the move
3. No-one lives at "111 Maple Ln" either before the move, i.e. no record exists in `phone` with "111 Maple Ln" as its value for `phone.street`
4. Only the Does will live at "111 Maple Ln" after the move

If any of these assumptions could in principle be violated by your application, then you will probably need to use combinations of `insert` and `delete` as already shown, in order to accomplish all moves, not just moves like Jack's move to Jill's existing residence.

That does not mean that `update` is useless. Suppose that Jack and Jill decided to get married and that Jack decided to start using Jill's last name. It would then be necessary to update the `person` record for Jack accordingly:

```sql
update person
set last_name = 'Roe'
where person_id = 4;
```

An important hint regarding which columns it is generally safe to update rather than perform modifications via insertions and deletions can be had by looking at the various columns' constraints. Primary keys should never be modified. Keys that are `unique`, even if not primary keys, should be modified only with caution. Columns that are not used as the destination of cross-reference between tables and which are not required to be `unique` are more likely to be safe to update. But that assumes that great care was put into the design of the database schema, such that the lack of an explicit constraint may be relied upon as a deliberate design choice. All bets are off if a column which really ought to have such a constraint because of how it is used in the overall schema fails to have it due to ignorance, carelessness or an attempt at optimization to avoid constraint processing overhead.

Similarly, it is important to never delete a record that is being referenced via a key column by any other record in any table. For example, how would one go about creating a coherent view of the overall contact list if the `resisdent` table included a record still showing that Jack resides at the place denoted by the `address` record with the `address_id` value of 3 after the latter had been deleted from the database?

## Join Types

With examples like the many-to-many relationships discussed above, you can see that joining data from multiple tables into coherent views can be a complicated process fraught with potential ambiguity. It is no simple matter to go from the physical representation of a contact list that involves a number of tables like `address`, `phone`, `person`, `resident` and `subscriber` to something resembling the flat contact list representation with which we started. Specifically, the discussion describing updates like Jack's move to Jill's home contained a small lie of omission. There I said that the reason to delete "orphaned" records from tables like `address` and `phone` was simply a matter of application requirements. While true, that is far from the whole story.

One might have elected to leave addresses and phone numbers in their respective tables behind even after no entries in the `resident` and `subscriber` tables referenced them. Such records would have consumed a small amount of storage to no obviously good purpose for our contact list application but possibly as an essential requirement for some other. Perhaps surprisingly to the novice SQL user, their presence would not have affected the output from any of the `select ... from ... join ...` statements shown above. In other words, there can easily be "garbage" data lurking in a database, consuming storage, causing constraint-processing overhead and so on but with little visibility using standard database user operations.

In this case, the reason the "orphan" records would not have affected the output of the `select` statements given as examples is that the default "join type" is an "inner join." I.e. the last `select` statement shown above could have been written and means exactly the same as:

```sql
select
  person.first_name,
  person.last_name,
  address.street,
  phone.phone_number
from person
inner join resident on person.person_id = resident.person_id
inner join subscriber on person.person_id = subscriber.person_id
inner join address on  address.address_id = resident.address_id
inner join phone on  phone.phone_id = subscriber.phone_id;
```

This begs the questions, of course, "what makes a join 'inner' and what other kinds of joins are there?" Different RDBMS implementations provide different repertoires of join types. For MySQL, here are the join types:

| Join Type          | Short Hand   | Meaning                                                                                     |
|--------------------|--------------|---------------------------------------------------------------------------------------------|
| `inner join`       | `join`       | Intersection of the two tables as matched by the `on` expression                            |
| `left outer join`  | `left join`  | Union of the table on the left (`from`) and the intersection of the two tables              |
| `right outer join` | `right join` | Union of the table on the right ('right join') and the intersection of the two tables       |
| `cross join`       | `cross join` | The cross product (also known as the Cartesian product) of all the columns from both tables |

The terms _intersection_, _union_ and _cross / Cartesian product_ are used here in the senses defined by the branch of mathematics known as [Set Theory](https://en.wikipedia.org/wiki/Set_theory). Some other RDBMS software, notably Microsoft SQL Server, define an additional `full outer join` type not supported directly by MySQL.

Visually, we can depict the commonly used join types as _Venn diagrams_. The shaded part of each diagrams depicts the records that will appear in the corresponding type of join's result set:

**Inner Join**

![](inner-join-venn.png)

**Left Outer Join**

![](left-outer-join-venn.png)

**Right Outer Join**

![](right-outer-join-venn.png)

**Full Outer Join**

![](full-outer-join-venn.png)

**Cross Join**

![](cross-join-venn.png)

In the preceding diagrams, the circles represent two tables being joined. The _Left_ circle is the table specified by the `from` clause in a `select` statement. The _Right_ circle represents the table specified by the `join` clause. The shaded region indicates the records that will appear in the result set returned by `select`. The lens-shaped region where the circles overlap is the intersection of the records from the two tables.[<sup>&star;</sup>](#not-really-sets)

> <a id="not-really-sets"><sup>&star;</sup></a> **Note:** While discussions of relational databases often refer to tables and the output of `select` as if they were "sets" in the sense used in set theory, actually they are not. For one thing, the result sets returned by `select ... from ... join ...` contain records that do not actually appear as-is directly in any of the input tables. For another, tables and results can contain duplicate records while by definition sets cannot contain duplicate elements. But the mathematical rules governing set-theoretic operations like intersection, union and cross-product are very useful analogies for how relational databases function. You can use the axioms and theorems of set theory to prove that a given database schema has certain properties, as long as one is not too rigorous as to what sets are or what intersecting them really means.

Inner and outer joins require `on` expressions to drive the matching to determine the intersection. Cross joins are not allowed to have `on` expressions because that would be meaningless. I.e. a cross join combines everything unconditionally while the inner and outer join types require a rule to determine the relevant intersection. Note that in the preceding diagrams, every join type except cross joins returns at least the intersection of the two tables. For inner joins, that is all they return. Left and right outer joins return the union of the intersection and the corresponding left or right table. Intersections are irrelevant for cross joins. They return every possible combination of every record in both sets.

While both full outer joins and cross joins are "exhaustive" in the sense of returning all the data from both tables, a cross join returns records that would not be included in a full outer join because they correspond to "relationships" for which no sensible `primary key / foreign key` association exists.

Another way of understanding the various join types is:

- Inner joins only show "active" or "meaningful" relationships based on the supplied rule driving the intersection

- Outer joins show those same results but also include "dead" or "irrelevant" records from one, the other, or both tables depending on whether it is a left, right, or full outer join

- Cross joins provide very little in the way of meaning since they act as if every possible pairing of columns is a valid relationship

_"I'll take your word for it,"_ I hear the long-suffering Reader mutter, _"but what does any of that actually mean?"_

The effects of the different join types are best illustrated by examples. Suppose we add an entry to the `person` table for someone without adding any entries to `resident` or `subscriber` that refer to the new person.

```sql
insert into person (first_name,Last_name) values ('Joe','Blow');
```

As claimed previously, doing so will have no effect on the output of the `select` statements shown so far, because they used inner joins. I.e. since nothing in `phone` or `address` references the `person` record for Joe, there can be no record in common between them in the inner join's result set. That is what is meant by "intersection." However, if `select` is modified to use left outer joins, a new entry will appear in its output:

```sql
select
  person.first_name,
  person.last_name,
  address.street,
  phone.phone_number
from person
left join resident on person.person_id = resident.person_id
left join subscriber on person.person_id = subscriber.person_id
left join address on  address.address_id = resident.address_id
left join phone on  phone.phone_id = subscriber.phone_id;
```

Note the additional row for Joe, with `NULL` as the values for `street` and `phone_number`:

```
+------------+-----------+-----------------+--------------+
| first_name | last_name | street          | phone_number |
+------------+-----------+-----------------+--------------+
| Jane       | Doe       | 123 Main St     | 555-555-1234 |
| John       | Doe       | 123 Main St     | 555-555-1234 |
| Jill       | Roe       | 456 East Ave    | 555-555-4321 |
| Jill       | Roe       | 99 Lake View Dr | 555-555-4321 |
| Jill       | Roe       | 456 East Ave    | 555-555-5555 |
| Jill       | Roe       | 99 Lake View Dr | 555-555-5555 |
| Jack       | Low       | 99 Lake View Dr | 555-555-4321 |
| Jack       | Low       | 456 East Ave    | 555-555-4321 |
| Joe        | Blow      | NULL            | NULL         |
+------------+-----------+-----------------+--------------+
```

Joe does not appear in the output for the inner join case because none of the `on` expressions cause a match. He does appear in the output of left join because in that case every record on the `from` side will appear whether or not it is matched by any of the `on` expressions. His entry shows `NULL` in those columns that would contain values from the right side if their had been a match. `NULL` is a special value in SQL that means "no valid data." `NULL` appears for records in outer joins because if there were valid data to put in those columns, they would have appeared in the output of an inner join to start with.

In the current state, the output of a right join would again be identical to that of an inner join. This is because

1. A right join does not include unmatched records coming from the left
2. Currently, all of the records coming from the right have matching records coming from the left

If we add "dangling" records to the `address` and / or `phone` tables and use corresponding right joins, you can see the same effect as seen above for Joe.

```sql
insert into address (street) values ('66 6th Blvd');

select
person.first_name,
person.last_name,
address.street
from person
right join resident on person.person_id = resident.person_id
right join address on  address.address_id = resident.address_id;
```

Produces as output:

```
+------------+-----------+-----------------+
| first_name | last_name | street          |
+------------+-----------+-----------------+
| Jane       | Doe       | 123 Main St     |
| John       | Doe       | 123 Main St     |
| Jill       | Roe       | 456 East Ave    |
| Jack       | Low       | 456 East Ave    |
| Jack       | Low       | 99 Lake View Dr |
| Jill       | Roe       | 99 Lake View Dr |
| NULL       | NULL      | 66 6th Blvd     |
+------------+-----------+-----------------+
```

Note that as with inner joins, Joe does not appear in this right outer join because he is coming from the left. An entry for _66 6th Blvd_ does appear because it is gathered in by the right join. As with all outer joins, unmatched columns are populated with `NULL`.

Some other RDBMS programs, for example Microsoft's SQL Server, support full outer joins. This means that the result set could include unmatched records coming from both the left and the right. MySQL does not directly support full outer joins but their effect can be simulated using clever combinations of left and right joins. The easiest to visualize, and that will work for our database in its current state, is simply the union of left and right joins that mirror one another:

```sql
select
  person.first_name,
  person.last_name,
  address.street
from person
left join resident on person.person_id = resident.person_id
left join address on  address.address_id = resident.address_id
union
select
person.first_name,
person.last_name,
address.street
from person
right join resident on person.person_id = resident.person_id
right join address on  address.address_id = resident.address_id;
```

This works to produce the following output because it performs otherwise identical left and right joins and concatenates their output using the `union` connective:

```
+------------+-----------+-----------------+
| first_name | last_name | street          |
+------------+-----------+-----------------+
| Jane       | Doe       | 123 Main St     |
| John       | Doe       | 123 Main St     |
| Jill       | Roe       | 456 East Ave    |
| Jack       | Low       | 99 Lake View Dr |
| Jack       | Low       | 456 East Ave    |
| Jill       | Roe       | 99 Lake View Dr |
| Joe        | Blow      | NULL            |
| NULL       | NULL      | 66 6th Blvd     |
+------------+-----------+-----------------+
```

Note in particular that in this last example, both the unmatched person and the unmatched address appear. For reasons too involved to go into here, this simple approach does not generalize well. To really simulate a full outer join as implemented by SQL Server requires crafting the correct sequence of left and right joins and concatenating them with `union all` and the correct `where` filters. That approach would fully simulate a SQL Server full outer join in cases where the latter would include duplicate records in its result set. This simpler approach is quite satisfactory, however, for many real-world scenarios.

For completeness, here is what a cross join between the `person` and `phone` tables looks like:

```sql
select
  person.first_name,
  person.last_name,
  phone.phone_number
from person
cross join phone;
```

Note that this mechanically associates every record from the left and right even though that makes no sense for our data set:

```
+------------+-----------+--------------+
| first_name | last_name | phone_number |
+------------+-----------+--------------+
| Jane       | Doe       | 555-555-1234 |
| John       | Doe       | 555-555-1234 |
| Jill       | Roe       | 555-555-1234 |
| Jack       | Low       | 555-555-1234 |
| Jane       | Doe       | 555-555-4321 |
| John       | Doe       | 555-555-4321 |
| Jill       | Roe       | 555-555-4321 |
| Jack       | Low       | 555-555-4321 |
| Jane       | Doe       | 555-555-5555 |
| John       | Doe       | 555-555-5555 |
| Jill       | Roe       | 555-555-5555 |
| Jack       | Low       | 555-555-5555 |
| Jane       | Doe       | 666-666-4678 |
| John       | Doe       | 666-666-4678 |
| Jill       | Roe       | 666-666-4678 |
| Jack       | Low       | 666-666-4678 |
+------------+-----------+--------------+
```

For an online ordering or inventory application, a cross join could be used to produce every possible combination of, say, color and size in which a given product can be stocked (assuming, of course, that every possible such combination is actually manufactured which in the real world it rarely is).

```sql
drop database if exists examples;
create database examples;
use examples;

create table sizes (
  size varchar(7)
);

create table colors (
  color varchar(5)
);

insert into sizes (size) values ('small');
insert into sizes (size) values ('medium');
insert into sizes (size) values ('large');
insert into sizes (size) values ('x-large');

insert into colors (color) values ('red');
insert into colors (color) values ('blue');
insert into colors (color) values ('green');

select
  colors.color,
  sizes.size
from colors
cross join sizes;
```

```
+-------+---------+
| color | size    |
+-------+---------+
| red   | small   |
| blue  | small   |
| green | small   |
| red   | medium  |
| blue  | medium  |
| green | medium  |
| red   | large   |
| blue  | large   |
| green | large   |
| red   | x-large |
| blue  | x-large |
| green | x-large |
+-------+---------+
```

The real-world use cases for cross joins tend to be few and far between and are almost never involved in simple queries of the sort we have considered so far. The same is true, to a lesser extent, for left and right outer joins. There is a reason that `join` on its own means `inner join` since that is the kind of relationship most likely to be useful.

As a final example, one can use an outer join with a `where` clause to find "orphan" records like Joe's:

```sql
select
  person.first_name,
  person.last_name,
  address.street,
  phone.phone_number
from person
left join resident on person.person_id = resident.person_id
left join subscriber on person.person_id = subscriber.person_id
left join address on  address.address_id = resident.address_id
left join phone on  phone.phone_id = subscriber.phone_id
where address.street is null and phone.phone_number is null;
```

```
+------------+-----------+--------+--------------+
| first_name | last_name | street | phone_number |
+------------+-----------+--------+--------------+
| Joe        | Blow      | NULL   | NULL         |
+------------+-----------+--------+--------------+
```

By replacing `select` with `delete`, the preceding style of query could be transformed into a command to "garbage collect" dangling entries in the `person` table:

```sql
delete person
from person
left join resident on person.person_id = resident.person_id
left join subscriber on person.person_id = subscriber.person_id
where resident.person_id is null and subscriber.person_id is null;
```

Executing the preceding removes the record for Joe from the `person` table but leaves all the other entries since they valid data coming from the right side in all their columns. The same approach can be used with right outer joins to manage "orphan" address and phone records.

## More On Referential Integrity

Note the risk of getting this kind of query slightly wrong with the consequence, other things being equal, of deleting masses amount of valid data unintentionally. That is why you should always fully annotate your schemas with constraints like `foreign key`. In such cases, the database will protect you from yourself:

```
> delete person from person where person_id = 1;
ERROR 1451 (23000): Cannot delete or update a parent row: a
foreign key constraint fails (`examples`.`resident`,
CONSTRAINT `resident_ibfk_1` FOREIGN KEY (`person_id`)
REFERENCES `person` (`person_id`))
```

Conversely, the more normalized your data structure the higher the risk of accidentally leaving behind "orphan" records that may do nothing for your application but waste space and consume CPU resources during constraint processing. MySQL supports adding `on delete cascade` to `foreign key` to help address this. When you add `on delete cascade`, deleting a record referenced by way of the given foreign key no longer causes an error, instead _it deletes the records that reference the current one, as well._ The italics should be taken as a warning. You should think carefully about whether `on delete cascade` is really what you want in any given circumstance. The `on delete cascade` functionality makes most sense when the foreign key denotes a "part / whole" relationship, i.e. what [UML](#uml.md) class diagrams display using a composition symbol. That is different from the kind of relationship denoted as aggregation, which is the kind of association we have dealt with so far.

For an example where `on delete cascade` might make sense, consider a system for managing reservations at a big hotel chain or conference rooms in a big company. A reservation aggregates a person and a room in much the way that our `subscriber` and `resident` tables aggregate people, phone numbers and addresses.

![](reservation-classes.svg)

The "aggregation" symbol on this and the preceding class diagrams denote the fact that the associated entities can exist independent of one another even where the relationship between them implies some kind of containment or ownership. In this case, rooms continue to exist when no-one is using them and people continue to exist whether or not they are in a given room.

If the hotel chain or company is large enough to have multiple sites and / or multiple buildings in a single site, then it might make sense to add an entity representing buildings to the data model. The relationship between a building and its rooms denotes composition rather than simple aggregation.

![](building-classes.svg)

Unlike the relationship between people and rooms, there is an existential link between buildings and rooms denoted by use of the "composition" symbol in UML. One might remodel a building, adding and removing rooms over time. But if a building looses its last room it ceases to exist. Similarly, at least for the purposes of our application, there is no such thing as a room without a building. We can implement the `building` and `room` entities in a relational database using something like:

```sql
drop database if exists examples;
create database examples;
use examples;

create table building (
  building_id int primary key auto_increment,
  name varchar(255) not null unique
);

create table room (
  room_id int primary key auto_increment,
  name varchar(255) not null,
  building_id int,
  foreign key (building_id)
    references building(building_id)
    on delete cascade
);

create table person (
  person_id int primary key auto_increment,
  name varchar(255) not null
);

create table reservation (
  reserved_room int not null,
  reserved_from datetime not null,
  reserved_for int not null,
  reserved_by int not null,
  foreign key (reserved_room) references room(room_id),
  foreign key (reserved_by) references person(person_id)
);

insert into building (name) values ('One');
insert into building (name) values ('Two');

insert into room (
  name,
  building_id
) values (
  'A',
  1
);

insert into room (
  name,
  building_id
) values (
  'B',
  1
);

insert into room (
  name,
  building_id
) values (
  'C',
  2
);

insert into room (
  name,
  building_id
) values (
  'D',
  2
);

insert into person (name) values ('Fred');
insert into person (name) values ('Barney');

insert into reservation (
  reserved_room,
  reserved_by,
  reserved_from,
  reserved_for
) values (
  1,
  1,
  '2020-04-01T08:00:00',
  30
);

select building.name, room.name
from building
join room on building.building_id = room.building_id;

select
  building.name,
  room.name,
  person.name,
  reservation.reserved_from,
  reservation.reserved_for
from building
join room on building.building_id = room.building_id
join reservation on reservation.reserved_room = room.room_id
join person on person.person_id = reservation.reserved_by;
```

```
+------+------+------+---------------------+--------------+
| name | name | name | reserved_from       | reserved_for |
+------+------+------+---------------------+--------------+
| One  | A    | Fred | 2020-04-01 08:00:00 |           30 |
+------+------+------+---------------------+--------------+
```

Given all of the above, if we delete building Two, the rooms C and D are cascade deleted.

```sql
delete building from building where building_id = 2;

select * from room;
```

```
+---------+------+-------------+
| room_id | name | building_id |
+---------+------+-------------+
|       1 | A    |           1 |
|       2 | B    |           1 |
+---------+------+-------------+
```

This is because of `on delete cascade` in the `foreign key` reference from `room` by way of `building_id`. It makes sense to enable this behavior because, as said above, a room is part of a building and cannot exist without it. But what about reservations and people?

As things stand currently in our database, attempting to delete room A or building One will fail due to the `foreign key` without `on delete cascade` linking reservations to rooms.

```
> delete building from building where building_id = 1;
ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key
constraint fails (`examples`.`reservation`, CONSTRAINT `reservation_ibfk_1`
 FOREIGN KEY (`reserved_room`) REFERENCES `room` (`room_id`))

 delete building from building where building_id = 1;
ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key
constraint fails (`examples`.`reservation`, CONSTRAINT `reservation_ibfk_1`
FOREIGN KEY (`reserved_room`) REFERENCES `room` (`room_id`))

```

It might seem convenient and consistent to the naive SQL user to add `on delete cascade` to every `foreign key` constraint to ease data maintenance tasks like adding and removing records for buildings, in this case. In reality, you would be better off not using `foreign key` constraints at all rather than abusing `on delete cascade` in cases of aggregation rather than composition.

In this case, the error deleting buildings and rooms when there are reservations that would be affected is a reminder that you need to cancel reservations before deleting rooms. Similarly, the corresponding error deleting a person who has a reservation is a reminder that the reservation must be re-assigned or canceled first. Fewer or different constraints risk either leaving your data in an inconsistent state or deleting more data than you intended as a side-effect of a seemingly simple operation on a single table.
