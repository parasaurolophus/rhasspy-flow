# Understanding UML

[Unified Modeling Language](https://www.uml.org/) is a standard for depicting the design and implementation of software and data processing systems as a collection of various types of diagrams. Its origins lie in early efforts at creating formal processes and documentation formats to be used in "object-oriented analysis" (OOA) and "object-oriented design" (OOD). The word "unified" in its name refers to the fact that its inspiration was an attempt to bring together two of the most popular such methodologies and associated formalisms of their day, those of Rumbaugh and Booch.

UML today consists of a fairly extensive set of diagrams types, each depicting in a highly stylized fashion a specific aspect of a data processing system at various levels of abstraction, as it is designed and implemented. While there are many types of diagrams, it is fair to say that you are most likely to encounter only a few of them in the real world. A few of the most popular types are described below.

To understand anything about UML you must first understand a bit of OOA/OOD terminology.

- A **class** is the description of an application-specific data type. Classes have _attributes_ (often called _data members_, _instance variables_ or _fields_) that contain specific pieces of information releant to the purpose of the class. For example, a class representing a person could have attributes representing the person's name, address and the like. Classes can also have _methods_ (sometimes called _member functions_) that define operations that can be carried out by objects of that class. A class representing a vehicle might have a method named _StartEngine_. Methods are visually distinguished from attributes by following their names with parentheses that can optionally contain a list of arguments (parameters) to the method

- An **instance** is an actual object that stores a particular set of values for the attributes defined by its class. Instances also provide the execution context when invoking the class' methods. I.e. a method will have direct access to the storage for a given instance's attributes at run time. Instances may be regions of computer memory allocated by a running program, records in a database etc.

- A set of classes may have **associations** between them. Such associations indicate any of a number of specific kinds of relationships that can exist between classes and between instances. For OOA/OOD the single most significant kind of relationship is **inheritance**. When one class is denoted as inheriting from another, the former is said to be a **sub-class** of the latter. Inheritance defines "is a" relationships between classes that allow instances of a sub-class to be treated in every way as if it were an instance of any class from which it inherits directly or indirectly. The ability to treat one instance as being of more than one type (any ancestor class in the case of inheritance) is called **polymorphism** and is an essential aspect of object-oriented programming. Other kinds of associations can be indicated in OOA/OOD, as well. Relationships like "uses," "has" (or "contains") and "is composed of" are among the relationship types most commonly referenced in OOA/OOD documents.

UML is intended to convey intended functionality and design rather than dictate implementation. For example, UML is officially agnostic regarding things like choice of programming language or how a particular kind of association is implemented by specific data structures, database schema features or the like. That said, the more fully "fleshed out" a set of UML and similar documentation is for a given project the less room for ambiguity but also the less freedom of choice as to how to implement.

# Class Diagrams

A _class diagram_ describes the "static structure" of the data stored and manipulated by a system. Each node in such a diagram represents a data type. Data types may be classes or interfaces. They may be "concrete" or be parameterized (i.e. correspond to what some programming languages refer to as "generic" classes).

Each class or interface is defined in terms of the attributes (data members or fields) and operations (function members or methods) it possesses. The nodes in a class diagram are connected with lines denoting relationships between them. Each relationship line is decorated in various ways denoting the kind and attributes of the relationship.

## Inheritance

```mermaid
classDiagram

  class Person {
    +String FirstName
    +String LastName
  }

  class Employee {
    +String SocialSecurityNumber
  }

  Person <|-- Employee
```

In the terminology used in object-oriented analysis, design and programming, the "is a" relationship denotes inheritance. In this case, an _Employee_ has all the attributes, methods and associations of a _Person_ as well as the additional _SocialSecurityNumber_ attribute. I.e. every _Employee_ has a _SocialSecurityNumber_ but not every _Person_ does. But every _Employee_ is a _Person_ so has a _FirstName_ and _LastName_.

## Associations

By contrast to the "is a" relationship, "has a" relationships are represented either as attributes (for simple data types like numbers, strings and so on) or associations between classes.

```mermaid
classDiagram

  class Person {
    +String FirstName
    +String LastName
  }

  class ContactInfo {
    +String Phone
    +String Address
  }

  Person o-- ContactInfo: +Contact
```

In this case, each _Person_ has a _FirstName_, _LastName_ and associated _ContactInfo_.

## Visibility and Multiplicity

Attributes and associations can have various levels of _visibility_. _Public_ members are decorated by preceding them with `+` signs. Any type of function or component can access any public member of any other type. _Protected_ members are decorated by preceding them with `#` symbols. Protected members are visible to the class to which they belong as well as any classes that inherit from it, but no other classes including those associated with it. _Private_ members are are decorated by preceding them with `-` signs. Only the class to which a private member directly belongs can access, not even classes that inherit from it.

There is a fourth, less frequently used visibility level that formally is called _internal_. It is denoted by preceding a member with a `~` symbol. Internal members have come to be used in some contexts for specific features of particular object-oriented programming languages. For example Java defines "package level" visibility and '~' is sometimes used to denote that when the implementation language is expected to be Java. But note that UML is meant to be a design tool that is independent of any particular implementation choices including which programming language will be used. If you see internal members in a UML diagram be sure to ask the author what they mean in that particular context.

| Visibility Indicator | Meaning   | Restriction                                              |
|----------------------|-----------|----------------------------------------------------------|
| `+`                  | Public    | Visible everywhere                                       |
| `#`                  | Protected | Visible only to the class itself and its sub-classes     |
| `-`                  | Private   | Visible only the the class itself                        |
| `~`                  | Internal  | Context-dependent, e.g. package-level visibility in Java |

```mermaid
classDiagram

  class VisibilityExamples {
    +String PublicAttribute
    #Integer ProtectedAttribute
    -Boolean PrivateAttribute
  }

  class AssociatedClass

  VisibilityExamples o-- AssociatedClass: #ProtectedAssociation
```

By default, associations between classes are assumed to be 1:1, i.e. an association between exactly one instance of each of the associated classes. Association lines can be decorated with "multiplicity" indicators at either or both ends to indicate relationships between different numbers of instances of each of the associated classes. For example, here is what data structure and database schema designers call a "1:many" (one to many) relationship:

```mermaid
classDiagram

  A "1" o-- "*" B
```

In the preceding example, the multiplicity indicator `1` at one of the association line is optional but it is considered good practice to indicate multiplicity at both ends if it is indicated at either.

| Multiplicity Indicator | Description                                                             |
|------------------------|-------------------------------------------------------------------------|
| _n_                    | A single number indicates exactly that number of instances (1, 2, 3...) |
| _m_.._n_               | A range, e.g. `2..4` means 2, 3 or 4 instances                          |
| *                      | Zero or more                                                            |

A `*` can be used on its own to indicate "zero or more." It can also be used as the right side of a range. For example, `2..*` means "two or more."

## Complete Class Diagram Example

```mermaid
classDiagram

  class Person {
    +String FirstName
    +String LastName
  }

  class Employee {
    +String SocialSecurityNumber
  }

  class Contractor

  class DirectHire

  class Manager

  class Company {
    +String Name
  }

  class ContactInfo {
    +String Phone
    +String Address
  }

  Person <|-- Employee
  Employee <|-- Contractor
  Employee <|-- DirectHire
  DirectHire <|-- Manager

  Person o-- ContactInfo: +Contact
  Company o-- ContactInfo: +Contact
  Contractor o-- Company: +Represents
  Manager "1" o-- "1..*" Employee: +DirectReports
```

It denotes:

- A _Person_ is a data type in the system that has _FirstName_ and _LastName_ attributes

- A _Person_ has associated _ContactInfo_

- A _ContactInfo_ has _Phone_ and _Address_ attributes

- A _Company_ has a _Name_ attribute and associated _ContactInfo_

- An _Employee_ is a _Person_ and has a _SocialSecurityNumber_ attribute

- A _Contractor_ is an _Employee_ and has an associated _Company_

- A _DirectHire_ is an _Employee_ with no additional attributes or associations

- A _Manager_ is a _DirectHire_ associated with 1 or more _Employee_ direct reports

### Object Diagrams

Less commonly used than class diagrams, object diagrams use almost exactly the same visual style. The difference is that the nodes in a class diagram represent classes while the nodes in an object diagram indicate instances.

Both describe the static structure of a program's data types, the a database schema and so on. Class diagrams represent the abstract design of that static structure. Object diagrams describe how that design is actually populated at run time in a particular configuration of a program, the actual contents of a database instance etc.

## Sequence Diagrams

_Sequence diagrams_ are used as one among several ways to specify the behavior of a data processing system. This is as opposed to class and object diagrams that describe the static structure of how the data being processed is represented. Sequence diagrams concentrate on the nitty-gritty interactions between components of a system as they work together on some coordinated activity. The concept of "component of a system" is very broad and can include actions taken by users through a user interface, scheduling "batch" jobs or the like.

Visually, a sequence diagram consists of a set of boxes layed out horizontally in a row. The same row of boxes appears at the top and bottom of the diagram. Each box represents one "participant" (sometimes called "actor," especially where the participant is a person). There are vertical "life lines" connecting each participant's box at the top and bottom of the diagram. The life lines represent the total extent of the sequence of operations depicted by the diagram. Interactions between participants are indicated by horizontal arrows drawn between life lines. The order of the interaction arrows from top to bottom represents the order in which the operations are carried out.

```mermaid
sequenceDiagram

  participant user as User
  participant browser as Web Browser
  participant server as Web Server

  user ->> browser: click link on web page
  browser ->> server: sent GET request
  server ->> server: process request
  server ->> browser: send HTML response
  browser ->> browser: render HTML on screen
```

When authored as part of a suite of design documentation including class diagrams, the participants would generally be instances of classes and the interaction lines would be labeled as an invocation of one of the receiving participant's methods.

```mermaid
classDiagram

  class Student {
    -dropAll()
  }

  class Class {
    +enroll(Student): Boolean
    +drop(Student)
  }

  class Database {
    ~checkPrequisitesAndEnroll(Student, Class): Boolean
    -drop(Student, Class): Boolean
  }

  Student "1" o-- "*" Class: +enrolled
```

```mermaid
sequenceDiagram

  participant Student
  participant Class

  Student ->> Class: enroll()
```

Depending on the level of detail provided by a given diagram, what is a single "atomic" operation from the point of view one participant may be a series of operations from the point of view of another. To represent such situations, sections of life lines can be overlaid with rectangles called "pipes." All the operations leading into or out of a given pipe constitute the implementation by the receiver of the operation indicated by the incoming operation at the top of the pipe.

```mermaid
sequenceDiagram

  participant Student
  participant Class
  participant Database

  Student ->> Class: enroll()
  activate Class
  Class ->> Database: checkPrequisitesAndEnroll()
  alt successful
    Class ->> Student: true
  else unsuccessful
    Class ->> Student: false
  end
  deactivate Class
```

The last example also introduce the use of the `alt` flow-of-control construct. It supports branching logic where different sub-sequences of operations might ensue under difference circumstances. It is the UML sequence diagram version of an `if... then... else...` statement in a typical programming language. Other such constructs include `opt` and `loop`. The `opt` construct is really the same as `alt` but with no `else`. The `loop` construct allows for repeating a sub-sequence multiple times until some condition is satisfied.

```mermaid
sequenceDiagram


  participant Student
  participant Class
  participant Database

  Student ->> Student: dropAll()
  activate Student
  loop for each Class in enrolled list
    Student ->> Class: drop()
    activate Class
    Class ->> Database: drop()
    deactivate Class
  end
  deactivate Student
```

## State Diagrams

Sequence diagrams are one way to depict the dynamic behavior of a system. They present a view of behavior that is closely aligned with the essentially procedural nature of most conventional programming languages, whether or not they are considered "object oriented." Even if a Java, C# or C++ program makes full use of polymorphism, encapsulation and any other of the hallmark  attributes of the object-oriented paradigm, the code in each method of each class remains procedural.

The trouble with the procedural point of view is that it is very narrowly focused on a particular scenario -- what Software Engineers call a "use case." Even with only a few classes and with a few methods each it can require a large number of sequence diagrams to completely document the intended behavior of every possible use case. That, of course, is difficult to create and maintain.

In addition, realistically complex systems embody many design principles that do not lend themselves well to representation in a strictly procedural fashion. Home automation systems (the focus of this while site) in particular are best understand as "event driven." An event driven system is one that spends a lot of time doing nothing other than waiting for something to happen. The system wakes up and takes some specific, predefined action when a given triggering event occurs. Such "triggering events" could be a user clicks on a control on a dashboard, a sensor sends a message over the network that motion was detected, a timer signals that some scheduled automation should run and so on. Usually, the system or component that triggers the event is operating asynchronously from the home automation system that responds to it. "Asynchronous" operations are ones that can be invoked at any time and from which the device, component or completely separate system that triggers the event does not expect a direct response.

Getting a holistic view of how more than one system behaves when communicating asynchronously is a task for which sequence diagrams are not well suited. _State diagrams_ were invented for just that purpose. A state diagram represents behavior when regarding the components or participants are regarded as being _state machines_. In this model, the participants are regarding as having a finite number of distinct internal states. Laying aside the concept of "composite" states for a moment, a given participant can be in exactly one state at a time. A state machine consists of a set of such states and set of rules by which a given participant transitions from one state to another. The nodes in a state diagram represent the set of states. The arrows connecting the state nodes indicate transitions and are labeled with the triggering event that causes the transition.

Consider a plain old, "dumb" light switch. It has two states, `on` and `off`. When a person flips the switch it toggles between those two states:

```mermaid
stateDiagram-v2

  device_off: off
  device_on: on

  device_off --> device_on: flip
  device_on --> device_off: flip
```

In the preceding diagram there are two states and one type of triggering event. Whichever state the system is in when the trigger event occurs it transitions to the other state.

A "smart" switch has the same states and reacts to being manually flipped in the same way as a "dumb" one. It also supports sending status notifications and receiving commands as messages over a network such as Wi-Fi, Bluetooth, Z-Wave or Zigbee. The state diagram for such device might look like:

```mermaid
stateDiagram-v2

  device_off: off
  device_on: on

  device_off --> device_on: flip,cmd_on / notify_on
  device_off --> device_off: cmd_off/notify_off
  device_on --> device_off: flip,cmd_off / notify_off
  device_on --> device_on: cmd_on / notify_on
```

Note that in the preceding example there are not only more types of triggering events (`cmd_on` and `cmd_off` in addition to `flip`) but each transition is labeled with both its triggering event and its output message in the form _input_ / _output_. The output events generated by one component's state transition are then consumed as triggering events by other components. If a given transition could be triggered by more than one input (e.g. both manually flipping a switch and sending it a command over a network) than all such inputs should be listed separated by commas, as in `flip,cmd_on` and `flip,cmd_off`. Such transitions can still generate output events, as in `flip,cmd_on / notify_on`. This only applies when the alternative would be to have otherwise identical transition arrows between exactly the same two states but with different input events. Note that in this case `flip,cmd_on` and `flip,cmd_off` are used as inputs on a single transition each to denote that there is no functional difference between manually flipping such a switch and sending it a command over a network to turn it on or off. There are separate transitions that use only `cmd_on` and `cmd_off` as their inputs to denote that a device needs to behave sensibly when it receives a command to transition into the state it is already in even though there is no equivalent possibility in terms of manual operations.

For simple diagrams it is sufficient to use the same syntax for multiple outputs as multiple inputs. For example, a "smart" light that can not only be toggled on and off but also dimmed often supports commands that make it act both like a normal on/off device and like a dimmer switch with a range of brightness levels. Such a light might have a transition like `cmd_on,cmd_level / notify_on,notify_level`. That would be appropriate if there were no states on the given diagram which used `notify_on` or `notify_level` as distinct triggering events.

For more complex situations where one transition's multiple output events are used as triggers by more than one state on the same diagram, use "fork." Where two nodes send the same output event that leads to a single state as input use  "join:"

```mermaid
stateDiagram

  state1
  state state2 <<fork>>
  state3
  state4
  state state5 <<join>>
  state6

  state1 --> state2: event1 / event2
  state2 --> state3: event2 / event3
  state2 --> state4: event2 / event4
  note left of state2: fork
  state3 --> state5: event5 / event7
  note right of state4: The component is in both<br/>state3 and state4 at this point,<br/>but on different threads
  state4 --> state5: event6 / event7
  state5 --> state6: event7
  note right of state5: join
```

Note that anywhere in any context related to computer programming you see the words "fork" and "join" you should understand them to refer to the concept of parallel threads of execution. That subject is worth at least one [tutorial of its own](parallel-execution.md). For example, Node-RED includes built-in _fork_ and _join_ node types which may be involved when implementing behavior like that shown above involving forking and joining state transitions.

As real world example, suppose that the home automation system presents a dashboard of some kind so that users can control devices using a computer or mobile device. Further, suppose that there is a toggle switch graphical user interface (GUI) control that is used to send commands to a particular physical "smart" switch:

```mermaid
stateDiagram-v2

  state Physical&nbsp;Switch {
    state send_notify_off <<fork>>
    state send_notify_on <<fork>>
    device_off: off
    device_on: on
  }

  state Dashboard&nbsp;Control {
    control_off: off
    control_on: on
    state send_on_command <<fork>>
    state send_cmd_on <<fork>>
  }

  device_off --> send_notify_on: flip,cmd_on
  send_notify_on --> control_on: notify_on
  send_notify_on --> device_on: sent
  device_on --> send_notify_off: flip,cmd_off
  send_notify_off --> control_off: notify_off
  send_notify_off --> device_off: sent

  control_off --> send_on_command: clicked
  send_cmd_on --> control_on: sent
  send_cmd_on --> device_on: cmd_off
  control_on --> send_cmd_on: clicked
  send_on_command --> control_off: sent
  send_on_command --> device_off: cmd_off
```

> **Note:** Exactly the preceding scenario is discussed at length in the context of the ["closed loop feedback" pattern](../design/closed-loop-feedback-pattern.md).

> **Note also:** I am slightly abusing UML state machine diagram syntax here. I am using "composite states" and forking transitions to document event-driven interactions between two different components or sub-systems, which is not really what the creators of UML intended.

It is worth emphasizing that the state machine model depicted in state diagrams is essentially non-procedural. It is better to think of them as collections of rules of the form

- "if a system or component is in state A and receives event X it transitions to state B"

    and

- "when a transition to state B occurs, trigger event Y"

<a id="activity-diagrams"></a>

## Activity Diagrams (a.k.a. Flow Charts)

Flow charts have been a staple of data processing documentation for as long as their have been data processing systems. UML defines the _activity diagram_ which is really just a slightly modernized flow chart.

As with sequence and state diagrams, activity diagrams describe the dynamic behavior of a system. Loosely speaking, an activity diagram is just a state diagram turned inside-out, and vice versa. In other words, while the nodes of a state diagram represent states and the arrows between states represent operations that cause the system to transition from one state to another, the nodes of a flow chart / activity diagram represent operations and the arrows represent the stateful context being operated upon.

For example, the same interactions between a "smart" device and a GUI control on a dashboard described above using a state diagram could be represented as the following activity diagram:

```mermaid
graph TD

  subgraph Physical Device
    device_wait[wait for event]
    device_event{flip or cmd}
    device_cmd_on{cmd = 'on'}
    device_on[turn device on]
    device_off[turn device off]
    device_flip[toggle state]
    device_notify[send notification]
  end

  subgraph Dashboard Control
    control_wait[wait for event]
    control_event{click or notify}
    control_clicked{control on}
    control_send_off[send cmd='off']
    control_send_on[send cmd='on']
    control_toggle[toggle state]
  end

  device_wait --> device_event
  device_event -- cmd --> device_cmd_on
  device_event -- flip --> device_flip
  device_cmd_on -- yes --> device_on
  device_cmd_on -- no --> device_off
  device_on --> device_notify
  device_off --> device_notify
  device_flip --> device_notify
  device_notify --> device_wait

  control_wait --> control_event
  control_event -- click --> control_clicked
  control_clicked -- no --> control_send_off
  control_clicked -- yes --> control_send_on
  control_send_on --> control_wait
  control_send_off --> control_wait
  control_event -- notify --> control_toggle
  control_toggle --> control_wait

  control_send_off .-> device_wait
  control_send_on .-> device_wait
  device_notify -. current state .-> control_wait
```

In activity diagrams, the shape of a node denotes the kind of operation. The two most common shapes are rectangles denoting procedures with side-effects and diamonds denoting "if... then... else..." branches in the flow of control. The labels on the arrows connecting the nodes are used to annotate what contextual information is being used on that logic path. That contextual information can include stored state that would be embodied in the nodes of a state diagram.
